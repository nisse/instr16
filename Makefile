
VERSION = 0.6

DISTDIR = instr16-$(VERSION)
DIST_FILES = instr16.pdf assembler/parse.c assembler/parse.h assembler/keyword.c

SUBDIRS = simulator assembler examples hw

all: instr16.pdf all-recursive
all-recursive:
	set -e; for d in $(SUBDIRS) ; do $(MAKE) -C $$d all ; done

clean:
	set -e; for d in $(SUBDIRS) ; do $(MAKE) -C $$d $@ ; done

check:
	$(MAKE) -C testsuite check

dist:
	rm -rf $(DISTDIR)
	git archive --prefix $(DISTDIR)/ --format tar master | tar xf -
	for f in $(DIST_FILES) ; do cp "$$f" $(DISTDIR)/"$$f" ; done
	tar -czf $(DISTDIR).tar.gz $(DISTDIR)

.PHONY: all all-recursive clean check dist

# TeX-related rules                                                                                                                      
.PRECIOUS: %.ps %.dvi

%.pdf: %.dvi
	dvipdf $< $@

%.ps: %.dvi
	dvips $(DVIPSFLAGS) -o $@ $<

%.dvi: %.tex
	latex --src-specials "\nonstopmode\input{$<}"
