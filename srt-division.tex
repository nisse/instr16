\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{xspace}

\author{Niels Möller}
\title{Hardware reciprocal using \textsc{srt}}
\date{2017}

\frenchspacing

\newcommand{\SRT}{\textsc{srt}\xspace}

\begin{document}

\maketitle

\section{Introduction}

To compute the reciprocal using Newton iteration results in a very big
circuit. To get a smaller circuit with reasonable performance for a
given size, \SRT division is a nice alternative. These notes
cover the simplest radix-2 and radix-4 variants.

\subsection{Notation}

Our application is computing the reciprocal. We have a word size
$\ell$ (we will use $\ell = 64$), and implied base $\beta = 2^\ell$. The
divisor $D$ is assumed normalized, $\beta/2 \leq D < \beta$. We want to
compute the quotient
\begin{equation*}
  Q = \lfloor(\beta^2 - 1)/D\rfloor
\end{equation*}
$Q$ is a 65-bit number, with most significant bit always one. We
represent the partial remainder as a fractional number, with the
binary point located so that $D$ is added or subtracted to the integer
part of $P$. Initially, we set
\begin{equation*}
  P_0 = \beta - 1/\beta = \beta-1 + (\beta-1)/\beta
\end{equation*}

\section{SRT division, radix 2}

The first quotient bit is then $q_0 = 1$, and we form the next partial
remainder as
\begin{equation*}
  P_1 = 2 (P_0 - D)
\end{equation*}
which lies in the range $0 < P_1 < \beta$.

The \SRT division algorithm (for radix 2) works by computing a partial
remainder sequence $P_k$ as
\begin{equation*}
  P_{k+1} = 2 (P_k - q_k D)
\end{equation*}
where the quotients $q_k$ are selected so that the sequence $P_k$
stays within a bounded interval. For basic radix 2 \SRT,
quotients are selected from the set $\{-1, 0, 1\}$, and $P_k$ is
bounded by $|P_k| < 2D$.

Since we will be using two's complement arithmetic, it's more
convenient to use a slightly asymmetric interval, $-M \leq P_k < M$.
For the moment, drop the $k$ subscript, and first examine the case $M
= 2D$. The quotient is not uniquely determined; instead we get the
following constraints:
\begin{description}
\item[$q = 0$:] Possible when $-D \leq P < D$.
\item[$q = 1$:] Possible when $P \geq 0$.
\item[$q = -1$:] Possible when $P < 0$.
\end{description}
The overlapping intervals is what enables efficient implementation: We
can select a working $q$ based only on examining the top few bits of
$P$.

\subsection{Representation of $P$}

We will only represent the integer part explicitly; since the fraction
is initially $\ell$ ones, we can handle it by just shifting in a one
bit in each iteration.

Since $|P_k| \leq 2D < 2\beta = 2^{\ell+1}$, we can represent $P_k$ as an
$\ell+2$-bit two's complement integer. To select a working $q_k$, it is
sufficient to examine the top three bits of $P_k$: If $P_k = 000\ldots$,
then $0 \leq P < \beta/2 \leq D$, and we can choose $q_k = 0$. And in all
other cases, $P \neq 0$ and with known sign, so we can choose $q_k$
from the sign bit of $P$.

But to limit latency when adding or subtracting $D$, we will represent
all but the top few bits of $P_k$ using a redundant ``carry save''
representation. So we set
\begin{equation*}
  P_k = S_k + C_k
\end{equation*}
where $S_k$ is a $\ell+2$ bits, and $C_k$ is a few bits smaller. The
value of the bits of $P_k$ are then the corresponding top bits of
$S_k$ plus any carry from adding in the smaller $C_k$. To accommodate
the unknown carry when going from $S_k$ to $P_k$, we need one more bit
when selecting $q_k$. I.e., $C_k$ can be $\ell - 2$ bits, 4 bits
smaller than $S_k$.

This adds a complication: If $P_k = 0111... \approx 2\beta$, we must
select $q_k = 1$, but if $P_k = 1000... \approx -2\beta$, we must select
$q = -1$. And if the top bits of $S_k$ are $0111$, which of these
cases we get depends on the carry, which we don't want to compute.

Since we have $|P_k| \leq 2D$, the ambiguity is possible only for $D$
close to $\beta$. One solution is to use a smaller $M$ in this case. If we
can ensure that $P < 7\beta/4$, then $P = 0111\ldots$ is no longer
possible.

\subsection{Narrowing the range}

So let us set $M = \min(2D, 7\beta/4)$. Then we rule out the border line
values of the top four bits of $P$, since $P = 0111\ldots$ implies $P
\geq M$ and $P = 1000\ldots$ implies $P < -M$.

To stay within this narrower range, the quotient selection constraints
get a little stricter,
\begin{description}
\item[$q = 0$:] Possible when $-M/2 \leq P < M/2$.
\item[$q = 1$:] Possible when $P \geq \max(0, D - 7\beta/8)$.
\item[$q = -1$:] Possible when $P < -\max(0, D - 7\beta/8)$.
\end{description}

If we tighten this a little bit more, we get the following constraints
which are sufficient for all values of $D$:

\begin{description}
\item[$q = 0$:] Possible when $-\beta/2 \leq P < \beta/2$.
\item[$q = 1$:] Possible when $P \geq \beta/8$.
\item[$q = -1$:] Possible when $P \leq -\beta/8$.
\end{description}

This lets us define quotient selection based on the top $S_k$ bits
only. Let $h$ denote the value of the four most significant bits of
$S_k$, interpreted as a two's complement number.

We have already ruled out the problematic case $h = 7$. So we can
assume that $-8 \leq h \leq 6$, and each value corresponds the the
following ranges for $S_k$ and $P_k$:
\begin{align*}
  h\beta/4 \leq S_k &< (h+1) \beta/4 \\
  h\beta/4 \leq P_k &< (h+2) \beta/4
\end{align*}
We can therefore use the following rules:
\begin{description}
\item[$-8 \leq h \leq -3$:] Then $-7\beta/4 \leq P_k < -\beta/4$, use $q_k = -1$. Note
  that $h = -8$ can happen only if we do get a carry from the addition
  of $C_k$.
\item[$-2 \leq h \leq 0$:] Then $-\beta/2 \leq P_k < \beta/2$. Use $q_k = 0$.
\item[$1 \leq h \leq 6$:] Then $\beta/4 \leq P_k < 7\beta/4$. Use $q_k = 1$.
\item[$h = 7$:] Can't happen.
\end{description}

\subsection{Final processing}

The iteration $P_{k+1} = 2 (P_k - q_k D)$ can be turned around to
\begin{equation*}
  P_k = q_k D + P_{k+1} / 2
\end{equation*}
After $\ell + 1$ iterations, we have
\begin{equation*}
  P_0 = \sum_{k=0}^\ell q_k 2^{-k} D + P_{\ell+1} / 2^{\ell+1}
\end{equation*}
Recall that $P_0 = \beta - 1/\beta$ and multiply by $\beta$, to get
\begin{equation*}
  \beta^2 - 1 = \sum_{k=0}^\ell q_k 2^{\ell-k} D + P_{\ell+1} / 2
\end{equation*}
Define
\begin{align*}
  Q' &= \sum_{k=0}^\ell q_k 2^{\ell-k} & R' = P_{\ell+1} / 2
\end{align*}
Then $\beta^2 - 1 = Q'D + R'$, and we have $-D \leq R < D$.
Hence $Q = Q' + [R < 0]$.

\section{Radix 4}

For radix-4 \SRT, we use quotients in the set $\{-2, -1, 0, 1, 2\}$.
Unlike radix 2, we have to take more bits of $P$ and $D$ into account
in quotient selection. The partial remainder update is
\begin{equation*}
  P_{k+1} = 4(P_k - q_k D)
\end{equation*}
We then need $P$ in the range $-8D/3 \leq P < 8 D / 3$ (sligthtly
assymmetrical). Integer part needs $\ell + 3$ bits including a two's
complement sign bit. For reciprocal, we must start with
$q_0 \in \{1, 2\}$. It should work to select
\begin{equation*}
  q_0 =
  \begin{cases}
    1 & 3\beta / 4 \leq D \leq \beta \\
    2 & \beta / 2 \leq D < 3 \beta / 4
  \end{cases}
\end{equation*}
Then with $q_0 = 1$ we get $0 < P_1 < 8D/3$, and with $q_0 = 2$ we get
$-8D/3 < P_1 < 0$. For following iterations, constraints are:
\begin{description}
\item[$q = 2$:] Possible when $P \geq 4D/3$.
\item[$q = 1$:] Possible when $D/3 \leq P < 5D/3$.
\item[$q = 0$:] Possible when $-2D/3 \leq P < 2D/3$
\item[$q = -1$:] Possible when $-5D/3 \leq P < -D/3$.
\item[$q = -2$:] Possible when $P < -4D/3$.
\end{description}
Again, the overlap between these intervals is the key.

So if we start with considering thresholds for $3P$, we roughly need
to distinguish multiples $\{1, 2, 4, 5\} D$. It should be cheap to
compute these thresholds up front with as many bits we need; all but
$5D$ is a plain shift. So how many bits of $P$ do we need? If we have
an error $e$, we need $3e < D$, or $e < D / 3$. Taking smallest $D$,
it's sufficient with $e < B / 6$, and even more sufficient with
$e < B/8$. This implies that we need at most 6 bits of $P$ (including
sign).

\subsection{Selecting $q = 0$}


Let is start with quotient selection rules for selecting $q = 0$.
Assume we have $P$ in the interval
$p \beta 2^{-k} \leq P < (p+1) \beta 2^{-k}$. Again, consider two
cases, starting with small $D$, $\beta / 2 \leq D < 3 \beta / 4$. We
then have
\begin{align*}
  0.001010\ldots < \beta / 6 < &D/3 < \beta / 4 = 0.010000\dots \\
  0.010101\ldots < \beta / 3 < &2D/3 < \beta / 3 = 0.100000\dots
\end{align*}
Here, it's sufficient with $k = 2$. If $p \in \{-1, 0 \}$, then
\begin{equation*}
  |P| \leq \beta / 4 < \beta/3 = 2D/3.
\end{equation*}
so we can select $q = 0$. And if $p \geq 1$, or $p \leq -2$, then
$|P| \geq \beta / 4 > D/3$, and we can select $q \in \{-2, -1, 1, 2\}$.

For the other case, assume $3\beta / 4 \leq D < \beta$. Then
\begin{align*}
  0.010000\beta / 4 \leq &D/3 < \beta / 3 < 0.0101 \ldots 0110 \\
  0.100000\beta / 2 \leq &2D/3 < 2 \beta / 3 0.1010 \ldots 1011
\end{align*}
Still, $k = 2$ is sufficient precision. If $-2 \leq p \leq 1$, then
$\beta / 2 \leq P < \beta / 2$.
\begin{equation*}
  -2D/3 \leq -\beta /2 \leq P < \beta / 2 \leq 2D/3
\end{equation*}
And if $p \geq 2$ or $p \leq -3$, then
\begin{equation*}
  |P| \geq \beta / 2 > \beta / 3 > D/3
\end{equation*}
so we can select $q \in \{-2, -1, 1, 2\}$.

To sum up select, $q = 0$ if $p \in \{-1, 0\}$ or ($p \in \{-2, 1\}$
and $D \geq 3\beta /4$.

\subsection{Selecting $|q| = 1$ or $|q = 2|$}

Here, we have a tighter overlap, thresholds $4D/3$ and $5D/3$. If we
start again with the case of small $D$, $\beta / 2 \leq D < 3\beta /
4$, we get
\begin{align*}
  0.10101\ldots < 2 \beta / 3 & \leq 4D/3 < \beta = 1.0000 \\
  0.110101\ldots < 5 \beta / 6 &\leq 5D/3 < 5 \beta/4 = 1.0100
\end{align*}
The intervals overlap. So we need more precision for $D$. Halve the
interval, and assume $\beta / 2 \leq D < 5 \beta / 8$. We then get
$4D/3 < 5 \beta / 6$, still too tight.

Halve again, assume $\beta / 2 \leq D < 9 \beta / 16$. Then
\begin{align*}
  0.10101\ldots < 2 \beta / 3 & \leq 4D/3 < 3\beta/4 = 0.11000 \\
  0.110101\ldots < 5 \beta / 6 &\leq 5D/3 < 15 \beta/16 = 0.1111000
\end{align*}
This gives disjoint ranges, but would need $k = 4$. If we halve the
$D$ interval again, assuming $\beta / 2 \leq D < 17 \beta / 32$, then
\begin{equation*}
  4D/3 < 17 \beta / 24 = 0.101101
\end{equation*}
This would work with $k = 3$.

Let us also examine what happens for the largest $D$. With $D < \beta$
we get
\begin{equation*}
4D/3 < 4 \beta / 3 = 1.010101\ldots
\end{equation*}
For a lower bound of $D \geq 15/16$, we get
\begin{equation*}
  5D/3 \geq 25/16 \beta = 1.100100
\end{equation*}
Which would work with $k = 3$. Halving again, $D >31/32$, gives
\begin{equation*}
  5D/3 \geq 155/96 \beta = 1.100111
\end{equation*}

All over, this gets tight and needs lots of bits.

\subsection{Using larger $q$ range}

We could use a larger wider selection of quotients, $-3 \leq q < 3$.
This provides a wider range for $P$,
\begin{equation*}
  -4D \leq P < 4D
\end{equation*}
Each $q$ can then be selected in the following intervals:
\begin{description}
\item[$q = 3$:] Possible when $P \geq 2D$.
\item[$q = 2$:] Possible when $D \leq P < 3D$.
\item[$q = 1$:] Possible when $0 \leq P < 2D$.
\item[$q = 0$:] Possible when $-D \leq P < D$
\item[$q = -1$:] Possible when $-2D \leq P < 0$.
\item[$q = -2$:] Possible when $-3D \leq P < -D$.
\item[$q = -3$:] Possible when $P < -2D$.
\end{description}

We still need $\ell + 3$ bits to represent $P$, but quotient selection
should get much easier. For each adjustment (with carrry-save
representation) we get more bits to add, $S + C + D + 2D$, which needs
two layers of full adders.

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 

%  LocalWords: radix srt
