/* xalloc.c */

/* Copyright (C) 2006, 2013, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xalloc.h"

void *
xalloc (size_t size)
{
  void *p = malloc (size);
  if (!p && size)
    {
      fprintf(stderr, "Virtual memory exhausted.\n");
      abort();
    }
  return p;
}

void *
xrealloc (void *p, size_t size)
{
  void *n = realloc (p, size);
  if (!n && size)
    {
      fprintf (stderr, "Virtual memory exhausted.\n");
      abort ();
    }
  return n;
}

void *
xmemdup (const void *p, size_t size)
{
  void *n = xalloc(size);
  memcpy (n, p, size);
  return n;
}

char *
xstrndup (const char *s, size_t size)
{
  char *n = xalloc(size + 1);
  memcpy (n, s, size);
  n[size] = '\0';
  return n;
}

