/* keyword.h */

/* Copyright (C) 2006, 2013, 2014, 2015, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_KEYWORD_H
#define INSTR16_KEYWORD_H

/* Opcodes for various special cases */
#define OPCODE_LDST_INDEXED	0x4000
#define OPCODE_IMM_TST		0xc000
#define OPCODE_IMM_CMPEQ	0xc200
#define OPCODE_IMM_CMPUGEQ	0xc400
#define OPCODE_IMM_CMPSGEQ	0xc600
#define OPCODE_IMM_CMPUGT	0xc800
#define OPCODE_IMM_CMPSGT	0xc900
#define OPCODE_IMM_MOV		0xb000
#define OPCODE_XOR		0xe300
#define OPCODE_MOV		0xe000
#define OPCODE_IMM_SIGN		0x0100
#define OPCODE_NOT		0xf910

enum keyword {
  KEYWORD_INT8,
  KEYWORD_INT16,
  KEYWORD_INT32,
  KEYWORD_INT64,
  KEYWORD_ASCII,
  KEYWORD_ASCIZ,
  KEYWORD_SINGLE,
  KEYWORD_DOUBLE,
  KEYWORD_NOP,
  KEYWORD_LD,
  KEYWORD_ST,
  KEYWORD_MOV,
  KEYWORD_MOVT,
  KEYWORD_MOVF,
  KEYWORD_ADD,
  KEYWORD_ADDC,
  KEYWORD_ADDS,
  KEYWORD_ADDV,
  KEYWORD_ADD8,
  KEYWORD_ADD16,
  KEYWORD_ADD32,
  KEYWORD_SUB,
  KEYWORD_SUBC,
  KEYWORD_SUBS,
  KEYWORD_SUBV,
  KEYWORD_SUB8,
  KEYWORD_SUB16,
  KEYWORD_SUB32,
  KEYWORD_NEG,
  KEYWORD_SHIFTL,
  KEYWORD_LSHIFT,
  KEYWORD_LSHIFT8,
  KEYWORD_LSHIFT16,
  KEYWORD_LSHIFT32,
  KEYWORD_RSHIFT,
  KEYWORD_RSHIFT8,
  KEYWORD_RSHIFT16,
  KEYWORD_RSHIFT32,
  KEYWORD_ASHIFT,
  KEYWORD_ASHIFT8,
  KEYWORD_ASHIFT16,
  KEYWORD_ASHIFT32,
  KEYWORD_ROT,
  KEYWORD_ROT8,
  KEYWORD_ROT16,
  KEYWORD_ROT32,
  KEYWORD_AND,
  KEYWORD_OR,
  KEYWORD_XOR,
  KEYWORD_NOT,
  KEYWORD_UMULHI,
  KEYWORD_MULLO,
  KEYWORD_SHIFT,
  KEYWORD_INJT8,
  KEYWORD_INJT16,
  KEYWORD_INJT32,
  KEYWORD_TST,
  KEYWORD_CMPEQ,
  KEYWORD_CMPUGEQ,
  KEYWORD_CMPSGEQ,
  KEYWORD_CMPULT,
  KEYWORD_CMPSLT,
  KEYWORD_CMPUGT,
  KEYWORD_CMPSGT,
  KEYWORD_CLZ,
  KEYWORD_CLT,
  KEYWORD_CLS,
  KEYWORD_POPC,
  KEYWORD_XSHIFT,
  KEYWORD_XSHIFTC,
  KEYWORD_RSHIFTC,
  KEYWORD_ASHIFTC,
  KEYWORD_BSWAP,
  KEYWORD_RECPR,
  KEYWORD_FNEG,
  KEYWORD_FABS,
  KEYWORD_JMP,
  KEYWORD_JSR,
  KEYWORD_BT,
  KEYWORD_BF,
  KEYWORD_BNZ,
  KEYWORD_BKPT,
  KEYWORD_HALT,
  KEYWORD_CC,
  KEYWORD_R0,
  KEYWORD_R1,
  KEYWORD_R2,
  KEYWORD_R3,
  KEYWORD_R4,
  KEYWORD_R5,
  KEYWORD_R6,
  KEYWORD_R7,
  KEYWORD_R8,
  KEYWORD_R9,
  KEYWORD_R10,
  KEYWORD_R11,
  KEYWORD_R12,
  KEYWORD_R13,
  KEYWORD_R14,
  KEYWORD_R15,
};

struct keyword_assoc
{
  const char *name;
  enum keyword id;
  unsigned type;
  /* Opcode or other flags used for code generation */
  uint16_t code;
  /* Alternative code, e.g., for a different addressing mode. */
  uint16_t alt;
};

const struct keyword_assoc *
keyword_lookup (const char *str, size_t len);

#endif /* INSTR16_KEYWORD_H */
