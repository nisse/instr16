/* program.c */

/* Copyright (C) 2006, 2013, 2014  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include "program.h"

#include "instruction.h"
#include "symbol.h"
#include "xalloc.h"

struct program *
program_create (void)
{
  NEW (struct program, self);

  self->symbols = symbol_table_create();
  self->instr = NULL;

  return self;
};

void
program_add_instruction (struct program *self,
			 struct instruction *instruction)
{
  /* Cons up list, reverse later. */
  instruction->next = self->instr;
  self->instr = instruction;
}

void
program_define_label (struct program *self, struct symbol *label,
		      struct instruction *instruction, unsigned lineno)
{
  if (!symbol_bind_label (label, instruction, lineno))
    {
      fprintf (stderr,
	       "Symbol %s redefined on line %d. Previous definition on line %d.\n",
	       label->name, lineno, label->definition_lineno);
      exit (EXIT_FAILURE);
    }
}

int
program_allocate (struct program *self)
{
  /* Start by destructively reversing the instruction list */
  struct instruction *head;
  struct instruction *p;
  uint64_t addr;
  for (head = NULL, p = self->instr; p;)
    {
      struct instruction *next = p->next;
      p->next = head;
      head = p;
      p = next;
    }
  self->instr = head;

  for (addr = 0, p = self->instr; p; p = p->next)
    {
      addr = (addr + (p->align - 1)) & - (uint64_t) p->align;
      p->address = addr;

      if (!p->size)
	{
	  fprintf (stderr, "%d: Unknown size of instruction.\n", p->lineno);
	  return 0;
	}
      addr += p->size;
    }
  return 1;
}

int
program_output (struct program *self, FILE *f)
{
  struct instruction *p;
  uint64_t addr;
  for (p = self->instr, addr = 0; p; addr += p->size, p = p->next)
    {
      /* Alignment padding */
      for (; addr < p->address; addr++)
	if (fputc (0, f) < 0)
	  return 0;

      if (!instruction_resolve (p))
	return 0;
      if (!instruction_output (p, f))
	return 0;
    }
  return fflush (f) == 0;
}
