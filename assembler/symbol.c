/* symbol.c */

/* Copyright (C) 2006, 2013, 2014  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <limits.h>
#include <stdio.h>
#include <string.h>

#include "symbol.h"

#include "instruction.h"
#include "xalloc.h"


/* For simplicity, use a fixed size for the hash table.*/
#define SYMBOL_TABLE_SIZE 211

/* All symbols are global, so keep just a single table, with values
   directly attached to each symbol. */
struct symbol_table
{
  struct symbol *table[SYMBOL_TABLE_SIZE];
};


struct symbol_table*
symbol_table_create (void)
{
  NEW (struct symbol_table, self);
  unsigned i;

  for (i = 0; i < SYMBOL_TABLE_SIZE; i++)
    self->table[i] = NULL;

  return self;
}

static unsigned
symbol_hash (const char *name, unsigned length)
{
  unsigned x = length * 1048583;
  unsigned i;

  for (i = 0; i < length; i++)
    x = ((x << 5 ) | (x >> (CHAR_BIT * sizeof(x) - 5)))
      ^ (unsigned char) name[i];

  return x;
}

static struct symbol *
symbol_create (unsigned hash,
	       const char *name, unsigned name_length,
	       unsigned lineno,
	       struct symbol *next)
{
  NEW (struct symbol, self);
  self->name = xstrndup (name, name_length);
  self->name_length = name_length;

  self->first_seen_lineno = lineno;
  self->type = SYMBOL_UNDEFINED;
  self->hash_value = hash;
  self->next = next;

  return self;
}

struct symbol *
symbol_intern (struct symbol_table *self,
	       const char *name, unsigned name_length,
	       unsigned lineno)
{
  unsigned hash = symbol_hash (name, name_length);
  unsigned index = hash % SYMBOL_TABLE_SIZE;

  struct symbol *p;

  for (p = self->table[index]; p; p = p->next)
    if (p->hash_value == hash
	&& p->name_length == name_length
	&& memcmp (name, p->name, name_length) == 0)
      return p;

  p = symbol_create (hash, name, name_length, lineno,
		     self->table[index]);
  self->table[index] = p;
  return p;
}

int
symbol_bind_label (struct symbol *symbol,
		   struct instruction *definition, unsigned lineno)
{
  if (symbol->type != SYMBOL_UNDEFINED)
    return 0;

  symbol->type = SYMBOL_LABEL;
  symbol->definition.instruction = definition;
  symbol->definition_lineno = lineno;

  return 1;
}

int
symbol_bind_constant (struct symbol *symbol,
		      uint64_t definition, unsigned lineno)
{
  if (symbol->type != SYMBOL_UNDEFINED)
    return 0;

  symbol->type = SYMBOL_CONSTANT;
  symbol->definition.value = definition;
  symbol->definition_lineno = lineno;

  return 1;
}

int
symbol_value (const struct symbol *symbol, int64_t *value)
{
  switch (symbol->type)
    {
    case SYMBOL_UNDEFINED:
      return 0;
    case SYMBOL_CONSTANT:
      *value = symbol->definition.value;
      return 1;
    case SYMBOL_LABEL:
      *value = symbol->definition.instruction->address;
      return 1;
    default:
      abort ();
    }
}
	
int
symbol_output_map(struct symbol_table *self,
		  FILE *f)
{
  unsigned b;
  for (b = 0; b < SYMBOL_TABLE_SIZE; b++)
    {
      const struct symbol *s;
      for (s = self->table[b]; s; s = s->next)
	if (s->type == SYMBOL_LABEL)
	  if (fprintf (f, "%08jx %s\n",
		       s->definition.instruction->address, s->name) < 0)
	    return 0;
    }
  return fflush (f) == 0;
}
