/* instr16 assembler
  
   Copyright (C) 2006, 2013, 2014, 2015   Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Needs a forward declaration, since bison-2.7 puts the yyparse
   prototype in parse.h. */
%code requires {
  struct parse_state;
}

%code top {
  #if HAVE_CONFIG_H
  # include "config.h"
  #endif

  #include <stdint.h>
}

%code {

  #include <ctype.h>
  #include <stdarg.h>
  #include <stdlib.h>

  #include "keyword.h"
  #include "program.h"
  #include "instruction.h"
  #include "symbol.h"
  #include "expr.h"
  #include "xalloc.h"

  struct parse_state
  {
    FILE *file;
    const char *filename;
    int free_filename;
    int start_of_line;
    unsigned lineno;

    unsigned nerror;
    struct program *program;
  };

  static int
  yylex (struct parse_state *state);

  static void
  yyerror (struct parse_state *state, const char *msg);
}

%union {
  uint64_t number;
  const struct expr *expr;
  const char *str;
  const struct keyword_assoc *keyword;
  struct symbol *symbol;
  struct instruction *instruction;
}

%debug
%expect 0
%parse-param {struct parse_state *state}
%lex-param {struct parse_state *state}

%token TOK_NEWLINE
%token TOK_ERROR
%token <symbol> TOK_SYMBOL

%token <number> TOK_NUMBER

%token <str> TOK_STRING

%token <keyword> TOK_SCALAR_TYPE
%token <keyword> TOK_STRING_TYPE
%token <keyword> TOK_INSTR_NULLARY
%token <keyword> TOK_INSTR_UNARY
%token <keyword> TOK_INSTR_BINARY
%token <keyword> TOK_INSTR_TERNARY
%token <keyword> TOK_INSTR_SHIFT
%token <keyword> TOK_INSTR_XSHIFT
%token <keyword> TOK_INSTR_ADD
%token <keyword> TOK_INSTR_MEMORY
%token <keyword> TOK_INSTR_CMPGT
%token <keyword> TOK_INSTR_BRANCH
%token <keyword> TOK_REGISTER
%token <keyword> TOK_CC

%type <instruction> instruction
%type <instruction> instr_ld_st
%type <instruction> instr_add
%type <instruction> instr_branch
%type <symbol> label

%type <expr> expr

%left '-' '+'

%%

program: /* Empty */
	| lines
	;

lines: line
	| lines line
	;

line: TOK_NEWLINE
	| instruction TOK_NEWLINE
	  {
	    if (!$1)
		YYERROR;
	    program_add_instruction (state->program, $1); }
	| label opt_newlines instruction TOK_NEWLINE
	  {
	    if (!$1)
		YYERROR;
	    program_add_instruction (state->program, $3);
	    program_define_label (state->program, $1, $3, state->lineno);
	  }
	;

/* FIXME: Attach line number and check for multiple definition here? */
label: TOK_SYMBOL ':' ; 

opt_newlines: /* Empty */
	| opt_newlines TOK_NEWLINE
	;

instruction: 	instr_ld_st
	|	instr_branch
	|	instr_add
	| 	TOK_SCALAR_TYPE expr
		{ $$ = instruction_int ($1->code, $2, state->lineno); }
        |	TOK_STRING_TYPE TOK_STRING
		{ $$ = instruction_string ($1->code, $2, state->lineno); }
	| 	TOK_INSTR_NULLARY
		{ $$ = instruction_nullary ($1->code, state->lineno); }
	| 	TOK_INSTR_UNARY TOK_REGISTER
		{ $$ = instruction_unary ($1->code, $2->code, state->lineno); }
	| 	TOK_INSTR_UNARY TOK_CC
		{
		  if (!$1->alt)
		    {
		      fprintf (stderr,
			       "%s:%d: %s instruction can't be applied to cc.\n",
			       state->filename, state->lineno, $1->name);
		      YYERROR;
		    }
		  $$ = instruction_nullary ($1->alt, state->lineno);
		}
	| 	TOK_INSTR_BINARY TOK_REGISTER ',' TOK_REGISTER
		{ $$ = instruction_binary ($1->code, $2->code, $4->code,
		state->lineno); }
	|	TOK_INSTR_SHIFT TOK_REGISTER ',' '#' expr
		{ $$ = instruction_imm_shift ($1->code, $2->code, $5, state->lineno); }
	|	TOK_INSTR_SHIFT TOK_REGISTER ',' TOK_REGISTER
		{
		  if (!$1->alt)
		    {
		      fprintf (stderr,
			       "%s:%d: Shift instruction %s requires immediate shift count.\n",
			       state->filename, state->lineno, $1->name);
		      YYERROR;
		    }
		  $$ = instruction_binary ($1->alt, $2->code,
					   $4->code, state->lineno);
		}
	|	TOK_INSTR_XSHIFT TOK_REGISTER
	{ $$ = instruction_unary ($1->code, $2->code, state->lineno); }
	|	TOK_INSTR_XSHIFT TOK_REGISTER ',' TOK_REGISTER
	{ $$ = instruction_binary ($1->alt, $2->code, $4->code,
		state->lineno); }
	|	TOK_INSTR_BINARY TOK_REGISTER ',' '#' expr
		{
		  if (!$1->alt)
		    {
		      fprintf (stderr,
			       "%s:%d: Instruction %s requires register arguments.\n",
			       state->filename, state->lineno, $1->name);
		      YYERROR;
		    }
		  $$ = instruction_imm ($1->id, $1->alt, $2->code, $5, state->lineno);
		}
	|	TOK_INSTR_TERNARY TOK_REGISTER ',' TOK_REGISTER ',' TOK_REGISTER
	{ $$ = instruction_ternary ($1->id, $1->code, $2->code, $4->code, $6->code, state->lineno); }
	|	TOK_INSTR_CMPGT TOK_REGISTER ',' '#' expr
		{
		  const struct expr *expr = expr_sum(expr_number(1), $5);
		  $$ = instruction_imm ($1->id, $1->alt, $2->code, expr, state->lineno); }
		;

instr_ld_st:	TOK_INSTR_MEMORY TOK_REGISTER ',' '[' TOK_REGISTER ']'
		{
		  $$ = instruction_memory ($1->code, $1->alt,
					   $2->code, $5->code, NULL, state->lineno);
		}
	|	TOK_INSTR_MEMORY TOK_REGISTER ',' '[' TOK_REGISTER ',' '#' expr ']'
		{
		  $$ = instruction_memory ($1->code, $1->alt,
					   $2->code, $5->code, $8, state->lineno);
		}
	| 	TOK_INSTR_MEMORY TOK_REGISTER ',' '[' TOK_REGISTER ',' TOK_REGISTER ']'
		{ $$ = instruction_ternary ($1->id, $1->code, $2->code, $5->code,
		  $7->code, state->lineno);}
		;

instr_add:	TOK_INSTR_ADD TOK_REGISTER ',' TOK_REGISTER
		{ $$ = instruction_add ($1->code, $2->code, $4->code, 0,
		state->lineno); }
	| 	TOK_INSTR_ADD TOK_REGISTER ',' TOK_CC ',' TOK_REGISTER
		{ $$ = instruction_add ($1->code, $2->code, $6->code, 1,
		state->lineno); }
	| 	TOK_INSTR_ADD TOK_REGISTER ',' '#' expr
		{ $$ = instruction_imm_add ($1->id, $1->alt, $2->code, $5, 0, state->lineno); }
	| 	TOK_INSTR_ADD TOK_REGISTER ',' TOK_CC ',' '#' expr
		{ $$ = instruction_imm_add ($1->id, $1->alt, $2->code, $7, 1, state->lineno); }

	;

instr_branch:	TOK_INSTR_BRANCH expr
		{ $$ = instruction_branch ($1->code, $2, state->lineno); }
	| 	TOK_INSTR_BRANCH '[' TOK_REGISTER ']'
		{ $$ = instruction_reg_branch ($1->alt, $3->code, state->lineno); }
		;

/* Expressions (quite limited) */
expr:		TOK_NUMBER { $$ = expr_number($1); }
	|	TOK_SYMBOL { $$ = expr_symbol($1); }
	|	expr '+' expr { $$ = expr_sum ($1, $3); }
	|	expr '-' expr { $$ = expr_diff ($1, $3); }
	|	'-' expr { $$ = expr_neg ($2); }
	|	'(' expr ')' { $$ = $2; }
	;

%%

static void
yyerror (struct parse_state *state, const char *msg)
{
  if (msg)
    fprintf (stderr, "%s:%d: %s.\n", state->filename, state->lineno, msg);
  state->nerror++;
}

static __attribute__ ((__format__ (__printf__, 2, 3))) int
lexical_error (struct parse_state *state, const char *msg, ...)
{
  va_list args;

  va_start (args, msg);

  fprintf (stderr, "%s:%d: ", state->filename, state->lineno);
  vfprintf (stderr, msg, args);
  fprintf (stderr, ".\n");

  va_end (args);
  return TOK_ERROR;
}

static int
skip_ws (FILE *f, int c)
{
  while (c != '\n' && isspace (c))
    c = getc(f);

  return c;
}

static void
skip_comment (FILE *f)
{
  int c;

  do
    c = getc(f);
  while (c != EOF && c != '\n');
}

static int
parse_number(FILE *f, uint64_t *value, int c, unsigned base)
{
  uint64_t x;
  if (!isdigit(c))
    return TOK_ERROR;

  x = c - '0';

  c = getc(f);
  if (base == 0)
    {
      base = 10;
      if (x == 0)
	switch (c)
	  {
	  case 'x': case 'X':
	    base = 16;
	    c = getc(f);
	    break;
	  case 'b': case 'B':
	    base = 2;
	    c = getc(f);
	    break;
	  }
    }
  for (;; c = getc(f))
    {
      unsigned digit;
      if (c >= '0' && c <= '9')
	digit = c - '0';
      else if (c >= 'A' && c <= 'Z')
	digit = c - 'A' + 10;
      else if (c >= 'a' && c <= 'z')
	digit = c - 'a' + 10;
      else break;

      if (digit >= base)
	break;
      x = base*x + digit;
    }
  ungetc(c, f);
  *value = x;

  return TOK_NUMBER;
}

static int
parse_char(FILE *f, uint64_t *value)
{
  int c = getc (f);
  if (c == '\\')
    {
      c = getc (f);
      switch (c)
	{
	case '0': c = '\0'; break;
	case 'a': c = '\a'; break;
	case 'b': c = '\b'; break;
	case 'f': c = '\f'; break;
	case 'n': c = '\n'; break;
	case 'r': c = '\r'; break;
	case 't': c = '\t'; break;
	case 'v': c = '\v'; break;
	case '\'': break;
	case '\\' : break;
	default:
	  fprintf (stderr, "Bad character constant.\n");
	  return TOK_ERROR;
	}
      }
  *value = (unsigned char) c;

  if (getc (f) != '\'')
    {
      fprintf (stderr, "Bad character constant.\n");
      return TOK_ERROR;
    }
  

  return TOK_NUMBER;
}

#define MAX_ID_LENGTH 200

static int
parse_identifier(struct parse_state *state, YYSTYPE *value, int c)
{
  unsigned l;
  char buf[MAX_ID_LENGTH];
  const struct keyword_assoc *keyword;

  buf[0] = c;
  l = 1;

  for (;;)
    {
      c = getc (state->file);
      if (! (c == '_' || c == '.' ||
	     (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
	     || (c >= '0' && c <= '9')))
	break;

      if (l >= MAX_ID_LENGTH)
	return lexical_error(state, "Too long identifier");

      buf[l++] = c;
    }
  ungetc(c, state->file);

  keyword = keyword_lookup (buf, l);
  if (keyword)
    {
      value->keyword = keyword;
      return keyword->type;
    }
  else
    {
      value->symbol = symbol_intern (state->program->symbols,
				     buf, l, state->lineno);
      return TOK_SYMBOL;
    }
}

static const char *
parse_quoted_string (FILE *f, int c)
{
  size_t alloc = 10;
  size_t size = 0;
  char *s;
  if (c != '"')
    return NULL;

  s = xalloc (alloc + 1);

  for (;;)
    {
      int c = fgetc (f);
      switch (c)
	{
	case '"':
	  s[size] = '\0';
	  return s;
	case EOF:
	  free (s);
	  return NULL;
	  /* FIXME: Handle \-escapes? */
	}
      if (size == alloc)
	{
	  alloc *= 2;
	  s = xrealloc (s, alloc);
	}
      s[size++] = c;
    }
}

static int
parse_directive (struct parse_state *state)
{
  unsigned i;
  int c;
  const char *filename;
  uint64_t lineno;
  c = skip_ws (state->file, fgetc (state->file));
  for (i = 0; i < 5; i++)
    {
      if (c != "line "[i])
	{
	  /* Skip line as a comment */
	  while (c != EOF && c != '\n')
	    c = getc (state->file);
	  state->lineno++;
	  return TOK_NEWLINE;
	}
      c = fgetc (state->file);
    }
  c = skip_ws (state->file, c);
  if (!parse_number (state->file, &lineno, c, 10))
    {
      fprintf (stderr, "Bad #line directive.\n");
      return TOK_ERROR;
    }
  c = skip_ws (state->file, fgetc (state->file));
  if (c == EOF || c == '\n')
    {
      state->lineno = lineno;
      return TOK_NEWLINE;
    }
  if (! (filename = parse_quoted_string (state->file, c)))
    {
      fprintf (stderr, "Bad #line directive.\n");
      return TOK_ERROR;
    }
  skip_comment (state->file);
  if (state->free_filename)
    free ((void *) state->filename);
  state->filename = filename;
  state->free_filename = 1;

  state->lineno = lineno;
  return TOK_NEWLINE;
}

static int
yylex (struct parse_state *state)
{
  int c;

  c = skip_ws(state->file, fgetc (state->file));

  switch (c)
    {
    case EOF:
      return -1;
    case ';':
      skip_comment(state->file);
      state->lineno++;
      state->start_of_line = 1;
      return TOK_NEWLINE;
    case '\n':
      state->lineno++;
      state->start_of_line = 1;
      return TOK_NEWLINE;
    case '#':
      if (!state->start_of_line)
	return c;
      else
	parse_directive (state);
      return TOK_NEWLINE;
    case ':':
    case ',':
    case '=':
    case '[':
    case ']':
    case '-':
    case '!':
    case '(':
    case ')':
      state->start_of_line = 0;
      return c;
    case '\'':
      state->start_of_line = 0;
      return parse_char (state->file, &yylval.number);
    case '"':
      state->start_of_line = 0;
      return (yylval.str = parse_quoted_string(state->file, c)) ? TOK_STRING : TOK_ERROR;
    default:
      state->start_of_line = 0;
      if (isdigit(c))
	return parse_number(state->file, &yylval.number, c, 0);
      else if (c == '_' || c == '.'
	       || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
	return parse_identifier(state, &yylval, c);
      else
	return TOK_ERROR;
    }
}

struct program *
program_parse_file (const char *filename, FILE *f)
{
  struct parse_state state;
  int res;

  state.file = f;
  state.filename = filename;
  state.free_filename = 0;
  state.start_of_line = 1;
  state.lineno = 1;
  state.nerror = 0;

  state.program = program_create();

  if (getenv ("YYDEBUG"))
    yydebug = 1;

  res = yyparse (&state);

  if (res == 0 && state.nerror == 0)
    return state.program;
  else
    return NULL;
}
