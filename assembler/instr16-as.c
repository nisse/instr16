/* instr16-as.c */

/* Copyright (C) 2013, 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <unistd.h>

#include <fcntl.h>

#include "program.h"
#include "symbol.h"
#include "xalloc.h"

#define M4_PROGRAM "/usr/bin/m4"

const char *
make_output_fname (const char *s)
{
  size_t len;
  char *name;
  const char *dot = strrchr (s, '.');
  if (dot)
    len = dot - s;
  else
    len = strlen(s);

  name = xalloc (len + 3);
  memcpy (name, s, len);
  memcpy (name + len, ".o", 3);
  return name;
}

static void
usage (FILE *f)
{
  fprintf (f,
	   "Usage: instr16-as [OPTIONS] [INPUT]\n"
	   "Options:\n"
	   "       -o OUTPUT-FILE  Default is input file with .o suffix.\n"
	   "                       Mandatory if no input file is given.\n"
	   "       -m              Use m4 for preprocessing.\n"
	   "       -M              Print symbol map.\n");
}

static FILE *
pexec (int keep_stdin, const char *program, const char **argv)
{
  int fds[2];
  int null;

  if (pipe (fds) < 0)
    {
      fprintf (stderr, "pipe failed: %s\n", strerror (errno));
      _exit (EXIT_FAILURE);
    }

  if (keep_stdin)
    null = -1;
  else
    {
      null = open ("/dev/null", O_RDONLY);
      if (null < 0)
	{
	  fprintf (stderr, "open of /dev/null failed: %s\n", strerror (errno));
	  _exit (EXIT_FAILURE);
	}
    }
  switch (fork ())
    {
    case -1:
	{
	  fprintf (stderr, "fork failed: %s\n", strerror (errno));
	  _exit (EXIT_FAILURE);
	}
    case 0:
      /* Child */
      close (fds[0]);
      if (dup2 (fds[1], STDOUT_FILENO) < 0
	  || (null >=0 && dup2 (null, STDIN_FILENO)))
	{
	  fprintf (stderr, "dup2 failed: %s\n", strerror (errno));
	  _exit (EXIT_FAILURE);
	}
      execv (program, (char * const *) argv);
      fprintf (stderr, "exec of %s failed: %s\n", program, strerror (errno));
      _exit (EXIT_FAILURE);
    default:
      /* Parent */
      close (fds[1]);
      if (null >= 0)
	close (null);
      return fdopen (fds[0], "r");
    }
}

int
main (int argc, char **argv)
{
  const char *input_fname = NULL;
  const char *output_fname = NULL;
  FILE *input;
  FILE *output;
  struct program *program;
  int use_m4 = 0;
  int output_map = 0;
  int c;

  while ( (c = getopt (argc, argv, "o:hmM")) != -1)
    switch (c)
      {
      case 'o':
	output_fname = optarg;
	break;
      case 'h':
	usage (stdout);
	return EXIT_SUCCESS;
      case 'm':
	use_m4 = 1;
	break;
      case 'M':
	output_map = 1;
	break;
      default:
	usage (stderr);
	return EXIT_FAILURE;
      }
  argv += optind; argc -= optind;

  if (argc > 0)
    input_fname = argv[0];

  if (!output_fname)
    {
      if (!input_fname)
	{
	  usage (stderr);
	  return EXIT_FAILURE;
	}
      output_fname = make_output_fname (input_fname);
    }
  if (use_m4)
    {
      /* FIXME: Change m4 comment character to ';' */
      const char *m4_argv[4];
      m4_argv[0] = "m4";
      m4_argv[1] = "-s";
      /* Possibly NULL, to read stdin */
      m4_argv[2] = input_fname;
      m4_argv[3] = NULL;
      input = pexec (!input_fname, M4_PROGRAM, m4_argv);
      if (!input )
	return EXIT_FAILURE;
    }
  else
    {
      if (input_fname)
	{
	  input = fopen (input_fname, "r");
	  if (!input)
	    {
	      fprintf (stderr, "Failed to open input file %s: %s\n",
		       input_fname, strerror (errno));
	      return EXIT_FAILURE;
	    }
	}
      else
	input = stdin;
    }
  program = program_parse_file (input_fname, input);

  if (!program)
    return EXIT_FAILURE;
  if (!program_allocate (program))
    return EXIT_FAILURE;
  output = fopen (output_fname, "wb");
  if (!output)
    {
      fprintf (stderr, "Failed to open output file %s: %s\n",
	       input_fname, strerror (errno));
      return EXIT_FAILURE;
    }
  if (!program_output (program, output))
    return EXIT_FAILURE;

  if (output_map)
    if (!symbol_output_map (program->symbols, stdout))
      return EXIT_FAILURE;

  return EXIT_SUCCESS;
}
