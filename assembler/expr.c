/* expr.c */

/* Copyright (C) 2013, 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "expr.h"

#include "symbol.h"
#include "xalloc.h"

static struct expr *
expr_create (enum expr_type type)
{
  NEW (struct expr, expr);
  expr->type = type;
  return expr;
}

const struct expr *
expr_number (uint64_t number)
{
  struct expr *expr = expr_create (EXPR_NUMBER);
  expr->u.number = number;
  return expr;
}

const struct expr *
expr_symbol (struct symbol *symbol)
{
  struct expr *expr = expr_create (EXPR_SYMBOL);
  expr->u.symbol = symbol;
  return expr;
}

static const struct expr *
expr_binary (enum expr_type type,
	     const struct expr *a, const struct expr *b)
{
  struct expr *expr = expr_create (type);
  expr->u.children[0] = a;
  expr->u.children[1] = b;
  return expr;
}

const struct expr *
expr_sum (const struct expr *a, const struct expr *b)
{
  return expr_binary (EXPR_SUM, a, b);
}

const struct expr *
expr_diff (const struct expr *a, const struct expr *b)
{
  return expr_binary (EXPR_DIFF, a, b);
}

const struct expr *
expr_neg (const struct expr *a)
{
  return expr_binary (EXPR_NEG, a, NULL);
}
  
static int
expr_eval_children (const struct expr *expr,
		    unsigned n, int64_t *value, int verbose)
{
  unsigned i;
  for (i = 0; i < n; i++)
    if (!expr_eval (expr->u.children[i], &value[i], verbose))
      return 0;

  return 1;      
}

int
expr_eval (const struct expr *expr, int64_t *value,
	   int verbose)
{
  switch (expr->type)
    {
    case EXPR_NUMBER:
      *value = expr->u.number;
      return 1;
    case EXPR_SYMBOL:
      if (symbol_value (expr->u.symbol, value))
	return 1;
      if (verbose)
	fprintf (stderr, "Undefined symbol: %s\n",
		 expr->u.symbol->name);
      return 0;

    case EXPR_SUM:
    case EXPR_DIFF:
      {
	int64_t c[2];
	if (!expr_eval_children (expr, 2, c, verbose))
	  return 0;
	switch (expr->type)
	  {
	  case EXPR_SUM:
	    *value = c[0] + c[1];
	    break;
	  case EXPR_DIFF:
	    *value = c[0] - c[1];
	    break;
	  default:
	    abort ();
	  }
	return 1;
      }
    case EXPR_NEG:
      {
	int64_t c;
	if (!expr_eval_children (expr, 1, &c, verbose))
	  return 0;

	*value = -c;
	return 1;
      }
    }
  abort ();
}
