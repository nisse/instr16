/* instruction.h */

/* Copyright (C) 2006, 2013, 2014  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_INSTRUCTION_H
#define INSTR16_INSTRUCTION_H

#include <stdio.h>

#include "symbol.h"
#include "keyword.h"

struct program;

enum instruction_type
  {
    INSTRUCTION_INT,
    INSTRUCTION_STRING,
    INSTRUCTION_NULLARY,
    INSTRUCTION_UNARY,
    INSTRUCTION_BINARY,
    INSTRUCTION_TERNARY,
    INSTRUCTION_MEMORY,
    INSTRUCTION_IMM,
    INSTRUCTION_SHIFT,
    INSTRUCTION_BRANCH,
  };

struct instruction
{
  struct instruction *next;

  int64_t address;
  const struct expr *expr;
  const char *str;
  int64_t imm;
  unsigned lineno;

  enum instruction_type type;
  unsigned code;
  unsigned char align;
  unsigned char size;
  
  /* Involved registers, reg[0] is typically the destination */
  unsigned char reg[3];
};

struct instruction *
instruction_int (unsigned code, const struct expr *value, unsigned lineno);

struct instruction *
instruction_string (unsigned code, const char *str, unsigned lineno);

struct instruction *
instruction_nullary (unsigned code, unsigned lineno);

struct instruction *
instruction_unary (unsigned code, unsigned dreg, unsigned lineno);

struct instruction *
instruction_binary (unsigned code, unsigned dreg, unsigned sreg,
		    unsigned lineno);

struct instruction *
instruction_ternary (enum keyword op, unsigned code,
		     unsigned dreg,
		     unsigned a0, unsigned a1, unsigned lineno);

struct instruction *
instruction_add (unsigned code, unsigned dreg, unsigned sreg, int cin,
		 unsigned lineno);

struct instruction *
instruction_imm (enum keyword op, unsigned code,
		 unsigned dreg,
		 const struct expr *imm, unsigned lineno);

struct instruction *
instruction_imm_cmpgt (enum keyword op, unsigned code,
		       unsigned dreg,
		       const struct expr *imm, unsigned lineno);

struct instruction *
instruction_imm_add (enum keyword op, unsigned code,
		     unsigned dreg, const struct expr *imm, int cin,
		     unsigned lineno);

struct instruction *
instruction_imm_shift (unsigned code, unsigned dreg,
		       const struct expr *imm, unsigned lineno);

struct instruction *
instruction_memory (unsigned code, unsigned alt_code,
		    unsigned dreg, unsigned sreg,		    
		    const struct expr *offset, unsigned lineno);

struct instruction *
instruction_branch (unsigned code, const struct expr *expr, unsigned lineno);

struct instruction *
instruction_reg_branch (unsigned code, unsigned sreg, unsigned lineno);

int
instruction_resolve (struct instruction *instr);

int
instruction_output (const struct instruction *self,
		    FILE *f);

#endif /* INSTR16_INSTRUCTION_H */
