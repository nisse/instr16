/* expr.h */

/* Copyright (C) 2013, 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_EXPR_H
#define INSTR16_EXPR_H

#include <stdint.h>

struct symbol;

enum expr_type {
  EXPR_NUMBER,
  EXPR_SYMBOL,
  EXPR_SUM,
  EXPR_DIFF,
  EXPR_NEG,
};

struct expr
{
  enum expr_type type;
  union {
    uint64_t number;
    struct symbol *symbol;
    const struct expr *children[2];
  } u;
};

const struct expr *
expr_number (uint64_t number);

const struct expr *
expr_symbol (struct symbol *symbol);

const struct expr *
expr_sum (const struct expr *a, const struct expr *b);

const struct expr *
expr_diff (const struct expr *a, const struct expr *b);

const struct expr *
expr_neg (const struct expr *a);

int
expr_eval (const struct expr *expr, int64_t *value, int verbose);

#endif /* INSTR16_EXPR_H */
