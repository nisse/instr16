/* instruction.c */

/* Copyright (C) 2013, 2014, 2015, 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>
#include <string.h>

#include "instruction.h"

#include "expr.h"
#include "xalloc.h"

static struct instruction *
instruction_create (enum instruction_type type, unsigned code,
		    unsigned align, unsigned size, unsigned lineno)
{
  NEW (struct instruction, instr);
  instr->lineno = lineno;
  instr->type = type;
  instr->code = code;
  instr->align = align;
  instr->size = size;
  instr->expr = NULL;
  instr->str = NULL;
  return instr;
}

struct instruction *
instruction_int (unsigned code, const struct expr *value, unsigned lineno)
{
  struct instruction *instr
    = instruction_create (INSTRUCTION_INT, code, code, code, lineno);

  instr->expr = value;
  return instr;
}

struct instruction *
instruction_string (unsigned code, const char *str, unsigned lineno)
{
  size_t len = strlen(str);

  struct instruction *instr
    = instruction_create (INSTRUCTION_STRING, code, 1, len + code, lineno);
  instr->str = str;
  return instr;
}

struct instruction *
instruction_nullary (unsigned code, unsigned lineno)
{
  return instruction_create (INSTRUCTION_NULLARY, code, 2, 2, lineno);
}

struct instruction *
instruction_unary (unsigned code, unsigned dreg, unsigned lineno)
{
  struct instruction *instr
    = instruction_create (INSTRUCTION_UNARY, code, 2, 2, lineno);
  instr->reg[0] = dreg;
  return instr;
}

struct instruction *
instruction_binary (unsigned code, unsigned dreg, unsigned sreg,
		    unsigned lineno)
{
  unsigned reversed_args = code & 1;
  unsigned simd_mode = (code >> 1) & 3;
  struct instruction *instr
    = instruction_create (INSTRUCTION_BINARY, code & ~7,
			  2, simd_mode > 0 ? 4 : 2, lineno);
  if (reversed_args)
    {
      instr->reg[0] = sreg;
      instr->reg[1] = dreg;
    }
  else
    {
      instr->reg[0] = dreg;
      instr->reg[1] = sreg;
    }
  /* Simd mode represented as prefix instruction. */
  instr->imm = simd_mode;
  return instr;
}

#define SWAP(a,b) do {				\
    unsigned swap_t = (a);			\
    (a) = (b);					\
    (b) = swap_t;				\
  }  while (0);

struct instruction *
instruction_ternary (enum keyword op, unsigned code,
		     unsigned dreg,
		     unsigned a0, unsigned a1, unsigned lineno)
{
  struct instruction *instr;

  /* Special case for indexed load and store */
  if (op == KEYWORD_LD)
    {
      code = OPCODE_LDST_INDEXED;
      if (a0 == a1)
	{
	  fprintf (stderr, "%d: Indexed load must use distinct base and index registers.\n",
		   lineno);
	  return NULL;
	}
      else if (a1 < a0)
	SWAP (a1, a0);
    }
  else if (op == KEYWORD_ST)
    {
      code = OPCODE_LDST_INDEXED;
      if (a0 == a1)
	{
	  fprintf (stderr, "%d: Indexed store must use distinct base and index registers.\n",
		   lineno);
	  return NULL;
	}
      else if (a1 > a0)
	SWAP (a1, a0);
    }
  else if (op == KEYWORD_SHIFTL)
    {
      if (a0 == 15)
	{
	  fprintf (stderr, "%d: Invalid shiftl, shifting in pc bits.\n",
		   lineno);
	  return NULL;
	}
      /* FIXME: Switch order elsewhere, to eliminate this swap. */
      SWAP (a1, a0);
    }
    
  instr = instruction_create (INSTRUCTION_TERNARY, code, 2, 2, lineno);
  instr->reg[0] = dreg;
  instr->reg[1] = a0;
  instr->reg[2] = a1;
  return instr;
}

struct instruction *
instruction_add (unsigned code, unsigned dreg, unsigned sreg, int cin,
		 unsigned lineno)
{
  if (cin)
    code |= 0x400;
  return instruction_binary (code, dreg, sreg, lineno);
}

/* Check if x fits in an n-bit unsigned representation */
#define IMM_FIT(x, n) \
  ((uint64_t) (x) < ((uint64_t) 1 << (n)))

/* Return 4-bit code, or -1 if not available */
static int
imm4_code (int64_t x)
{
  static const signed char tab[32] =    
    { /*  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 */
          1, 2, 3, 4, 5, 6, 7, 8,-1, 9,-1,10,-1,11,-1,12,
      /* 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 */
	 -1,-1,-1,13,-1,-1,-1,14,-1,-1,-1,15,-1,-1,-1, 0,
    };
  return (x > 0 && x <= 32) ? tab[x-1] : -1;
}

static int
imm_cmpgt_code (int64_t x)
{
  static const signed char tab[17] =
    { /*  10 12 14 16 18 20 22 24 26 28 30 32 */
           1, 2, 3, 4,-1, 5,-1, 6,-1, 7,-1, 0
    };
  if ( (x & 1) || x < 10 || (x > 32))
    return -1;

  else
    return tab[(x-10)/2];  
}

static unsigned imm4_size(uint64_t imm)
{
  if (imm4_code(imm) >= 0)
    return 2;
  else if (IMM_FIT (imm, 16))
    return 4;
  else if (IMM_FIT (imm, 28))
    return 6;
  else if (IMM_FIT (imm, 40))
    return 8;
  else if (IMM_FIT (imm, 52))
    return 10;
  else
    return 12;
}

static unsigned
opcode_imm4 (unsigned code, unsigned dreg, unsigned imm)
{
  assert ((code & 0xff) == 0);
  assert (dreg < 0x10);
  assert (imm < 0x10);
  return code | (imm << 4) | dreg;
}

static unsigned
opcode_cmpgt (unsigned code, unsigned dreg, unsigned imm)
{
  assert ((code & 0xff) == 0);
  assert (dreg < 0x10);
  assert (imm < 0x10);
  return code | (imm << 4) | dreg;
}

static unsigned
opcode_unary (unsigned code, unsigned reg)
{
  assert ((code & 0xf) == 0);
  assert (reg < 0x10);
  return code | reg;
}

static unsigned
opcode_binary (unsigned code, unsigned dreg, unsigned sreg)
{
  assert ((code & 0xff) == 0);
  assert (dreg < 0x10);
  assert (sreg < 0x10);
  return code | (sreg << 4) | dreg;
}

/* If the instruction uses a special encoding, return the opcode.
   Otherwise, return -1. If the instruction is invalid, return 0
   (which isn't a valid opcode for immediate instructions). The imm
   argument is signed. */
static int
opcode_imm_special (enum keyword op, unsigned code,
		    unsigned dreg,
		    int64_t imm, unsigned lineno)
{
  int imm_code;

  if (imm == 0)
    switch (op)
      {
      case KEYWORD_AND:
      case KEYWORD_MOV:
	/* and #0, mov #0 */
	return opcode_binary (OPCODE_XOR, dreg, dreg);

      case KEYWORD_CMPEQ:
	/* cmpeq #0, using a stolen encoding */
	return opcode_imm4 (OPCODE_IMM_CMPUGEQ, dreg, 2);
      case KEYWORD_CMPSGEQ:
      case KEYWORD_CMPSGT:
	/* cmpsgeq #0, using a stolen encoding */
	return opcode_imm4 (OPCODE_IMM_CMPUGEQ | OPCODE_IMM_SIGN,
			    dreg, 1);
      case KEYWORD_OR:
      case KEYWORD_XOR:
	goto nop;

      case KEYWORD_ADD:
      case KEYWORD_ADDC:
      case KEYWORD_ADDS:
      case KEYWORD_ADDV:
      case KEYWORD_SUB:
      case KEYWORD_SUBC:
      case KEYWORD_SUBS:
      case KEYWORD_SUBV:
      /* Only check add with carry. */
	if ((code >> 11) == 0x15)
	  {
	    unsigned cc_bits = (code >> 9) & 3;
	    return opcode_unary (0xf800 | (cc_bits << 4), dreg);
	  }
	break;
      case KEYWORD_CMPUGEQ:
      case KEYWORD_CMPUGT:
	fprintf (stderr, "%d: Always true cmpugeq.\n",
		 lineno);
	return 0;
      case KEYWORD_TST:
	fprintf (stderr, "%d: Always false tst.\n",
		 lineno);
	return 0;
      default:
	fprintf (stderr, "%d: Immediate #0 not implemented.\n",
		 lineno);
	return 0;
      }
  else if (imm == -1)
    switch (op) {
    case KEYWORD_AND:
    nop:
      /* and #-1 is a nop. */
      return opcode_binary (OPCODE_MOV, dreg, dreg);
    case KEYWORD_OR:
      /* or #-1, replace by mov #-1 */
      return opcode_imm4 (OPCODE_IMM_MOV | OPCODE_IMM_SIGN,
			  dreg, 1);
    case KEYWORD_XOR:
      return opcode_unary (OPCODE_NOT, dreg);
    case KEYWORD_ADD:
    case KEYWORD_ADDC:
    case KEYWORD_ADDS:
    case KEYWORD_ADDV:
    case KEYWORD_SUB:
    case KEYWORD_SUBC:
    case KEYWORD_SUBS:
    case KEYWORD_SUBV:
      /* Only check add with carry. */
      if ((code >> 11) == 0x15)
	{
	  unsigned cc_bits = (code >> 9) & 3;
	  return opcode_unary (0xf840 | (cc_bits << 4), dreg);
	}
      break;
    case KEYWORD_CMPUGEQ:
    case KEYWORD_CMPUGT:
      /* Stolen encoding. */
      return opcode_imm4 (OPCODE_IMM_CMPEQ | OPCODE_IMM_SIGN,
			  dreg, 1);

    case KEYWORD_TST:
      /* tst #-1 is opposite of cmpeq #0. */
    default:
      if (!(code & 1))
	{
	  fprintf (stderr, "%d: Immediate #-1 not implemented.\n",
		   lineno);
	  return 0;
	}
    }

  switch (op)
    {
    case KEYWORD_CMPUGEQ:
    case KEYWORD_CMPUGT:
      switch (imm) {
      case 2:
      case 4:
      case 8:
	/* Replaced by tst. */
	return opcode_imm4 (OPCODE_IMM_TST | OPCODE_IMM_SIGN,
			    dreg, imm-1);
      case 9:
	/* Corresponds to cmpugt #8, with a stolen
	   encoding. */
	return opcode_imm4 (OPCODE_IMM_CMPUGEQ, dreg, 8);
      default:
	if (imm < 0)
	  {
	    if ((imm_code = imm_cmpgt_code(1 - imm)) >= 0)
	      return opcode_cmpgt (OPCODE_IMM_CMPUGT, dreg, imm_code | 0x8);
	  }
	else
	  {
	    if ((imm_code = imm_cmpgt_code(imm - 1)) >= 0)
	      return opcode_cmpgt (OPCODE_IMM_CMPUGT, dreg, imm_code);
	  }
      }
      break;

    case KEYWORD_CMPSGEQ:
    case KEYWORD_CMPSGT:
      if (imm == (uint64_t) 1 << 63)
	{
	  fprintf (stderr, "%d: Always true cmpsgeq.\n",
		   lineno);
	  return 0;
	}
      else if (imm == 9)
	/* Corresponds to cmpsgt #8, with a stolen
	   encoding. */
	return opcode_imm4 (OPCODE_IMM_CMPUGEQ, dreg, 4);
      else if (imm < 0)
	{
	  if ((imm_code = imm_cmpgt_code(1 - imm)) >= 0)
	    return opcode_cmpgt (OPCODE_IMM_CMPSGT,
				 dreg, imm_code | 0x8);
	}
      else
	{
	  if ((imm_code = imm_cmpgt_code(imm - 1)) >= 0)
	    return opcode_cmpgt (OPCODE_IMM_CMPSGT,
				 dreg, imm_code);
	}
      break;

    default:
      break;
    }
  return -1;
}

static struct instruction *
instruction_imm_early (enum keyword op, unsigned code,
		       unsigned dreg,
		       int64_t imm, unsigned lineno)
{
  struct instruction *instr;
  unsigned size;
  int special_code;

  special_code = opcode_imm_special (op, code, dreg, imm, lineno);
  if (!special_code)
    return NULL;
  if (special_code > 0)
    return instruction_nullary (special_code, lineno);

  /* Absolute value or complement */
  if (imm < 0)
    {
      imm = ~imm + (code & 1);
      code |= OPCODE_IMM_SIGN;
    }
  code &= ~1;

  size = imm4_size (imm);

  instr = instruction_create (INSTRUCTION_IMM, code, 2, size, lineno);
  instr->reg[0] = dreg;
  instr->imm = imm;
  return instr;
}

static struct instruction *
instruction_imm_expr (unsigned code,
		      unsigned dreg,
		      const struct expr *expr, unsigned size,
		      unsigned lineno)
{
  struct instruction *instr
    = instruction_create (INSTRUCTION_IMM, code, 2, size, lineno);
  instr->reg[0] = dreg;
  instr->expr = expr;
  return instr;
}

struct instruction *
instruction_imm (enum keyword op, unsigned code,
		 unsigned dreg,
		 const struct expr *expr, unsigned lineno)
{
  int64_t imm;
  if (expr_eval (expr, &imm, 0))
    {
      free ((void *) expr);
      
      return instruction_imm_early(op, code, dreg, imm, lineno);
    }
  else    
    /* Hardcode size 4. FIXME: Should have a .size suffix */
    return instruction_imm_expr(code, dreg, expr, 4, lineno);
}

struct instruction *
instruction_imm_add (enum keyword op, unsigned code,
		     unsigned dreg, const struct expr *expr, int cin,
		     unsigned lineno)
{
  if (cin)
    {
      code |= 0x800;
      /* Use one's complement */
      code &= ~1;
    }
  if (code & 2)
    {
      if (cin)
	expr = expr_sum (expr, expr_number (1));
      expr = expr_neg(expr);
      code &= ~2;
    }
  return instruction_imm (op, code, dreg, expr, lineno);
}

struct instruction *
instruction_imm_shift (unsigned code, unsigned dreg, const struct expr *expr,
		       unsigned lineno)
{
  int64_t imm;
  unsigned simd_mode = code & 3;

  if (expr_eval (expr, &imm, 0))
    {
      free ((void *) expr);
      expr = NULL;

      if (imm <= 0 || imm >= 64)
	{
	  fprintf (stderr, "%d: Shift count out of range.\n", lineno);
	  return 0;
	}
      code |= (imm << 4);
    }
  struct instruction *instr
    = instruction_create (INSTRUCTION_SHIFT, code &= ~3, 2, simd_mode > 0 ? 4 : 2, lineno);

  instr->reg[0] = dreg;
  instr->expr = expr;
  instr->imm = simd_mode;
  return instr;
}

struct instruction *
instruction_memory (unsigned code, unsigned alt_code,
		    unsigned dreg, unsigned sreg,
		    const struct expr *expr, unsigned lineno)
{
  struct instruction *instr;
  int64_t offset;
  
  if (!expr)
    return instruction_binary (alt_code, dreg, sreg, lineno);
  if (expr_eval (expr, &offset, 0))
    {
      free( (void *) expr);
      if (!offset)
	return instruction_binary (alt_code, dreg, sreg, lineno);
      else
	{
	  struct instruction *instr;
	  unsigned size;
	  if (offset < 0)
	    {
	      code |= 0x1000;
	      offset = -offset;
	    }
	  size = imm4_size(offset);
	  
	  instr = instruction_create (INSTRUCTION_MEMORY, code, 2, size, lineno);
	  instr->reg[0] = dreg;
	  instr->reg[1] = sreg;
	  instr->imm = offset;
	  return instr;
	}
    }
  else
    {
      instr = instruction_create (INSTRUCTION_MEMORY, code, 2, 4, lineno);
      instr->reg[0] = dreg;
      instr->reg[1] = sreg;
      instr->expr = expr;
      return instr;
    }
}

struct instruction *
instruction_branch (unsigned code, const struct expr *target, unsigned lineno)
{
  /* FIXME: Offset must fit without prefix. */
  struct instruction *instr
    = instruction_create (INSTRUCTION_BRANCH, code, 2, 2, lineno);
  instr->expr = target;
  return instr;
}

struct instruction *
instruction_reg_branch (unsigned code, unsigned sreg, unsigned lineno)
{
  if (code & 1)
    return instruction_binary (code & ~(uint64_t) 1, 15, sreg, lineno);
  else
    return instruction_unary (code, sreg, lineno);
}

int
instruction_resolve (struct instruction *self)
{
  if (self->expr)
    {
      int64_t imm;
      if (!expr_eval (self->expr, &imm, 1))
	return 0;
      free ((void *) self->expr);
      self->expr = NULL;
      
      switch (self->type)
	{
	default:
	  abort ();
	case INSTRUCTION_IMM:
	  if (self->size == 2)
	    {
	      fprintf(stderr,
		      "%d: Short encoding here not yet supported.\n",
		      self->lineno);
	      return 0;
	    }
	  if (imm < 0)
	    {
	      imm = ~imm + (self->code & 1);
	      self->code |= OPCODE_IMM_SIGN;
	    }
	  self->code &= ~1;
	  self->imm = imm;
	  break;
	case INSTRUCTION_MEMORY:
	  if (imm < 0)
	    {
	      imm = -imm;
	      self->code |= 0x1000;
	    }
	  self->imm = imm;
	  break;
	case INSTRUCTION_INT:
	  self->imm = imm;
	  break;
	case INSTRUCTION_SHIFT:
	  if (imm <= 0 || imm >=64)
	    {
		fprintf (stderr, "%d: Shift count %jd out of range.\n",
			 self->lineno,  (intmax_t) imm);
		return 0;
	    }
	  self->code |= (imm << 4);
	  break;

	case INSTRUCTION_BRANCH:
	  if (imm & 1)
	    {
	      fprintf (stderr, "%d: Branch target refers to odd address %jx\n",
		       self->lineno,  (uintmax_t) imm);
	      return 0;
	    }
	    imm = (imm >> 1) - (self->address >> 1) - (self->size >> 1);
	    if (imm == 0)
	      {
		/* Make it a nop */
		fprintf (stderr, "%d: Branch converted to a NOP.\n",
			 self->lineno);
		self->type = INSTRUCTION_NULLARY;
		self->code = OPCODE_MOV;
		if (self->size > 2)
		  {
		    fprintf (stderr, "%d: Long branch but zero offset. Not implemented.\n",
			     self->lineno);
		    return 0;
		  }
	      }
	    else if (imm < 0)
	      {
		imm = -imm;
		self->code |= 0x200;
	      }
	    self->imm = imm - 1;
	    break;
	}
    }
  return 1;	
}

static int
output_int (FILE *f, unsigned size, int64_t x)
{
  while (size-- > 0)
    if (fputc ((x >> (8*size)) & 0xff, f) < 0)
      return 0;

  return 1;
}

static int
output_string (FILE *f, unsigned size, const char *str)
{
  return fwrite(str, 1, size, f) == size;
}

static int
output_uint16 (FILE *f, unsigned x)
{
  return output_int (f, 2, x);
}

static int
output_ternary (FILE *f, unsigned code, 
		unsigned dreg, unsigned a0, unsigned a1)
{
  assert ((code & 0xff) == 0);
  assert (dreg < 0x10);
  assert (a0 < 0x10);
  assert (a1 < 0x10);
  return output_uint16 (f, code | (a1 << 8) | (a0 << 4) | dreg);
}

static int
output_branch (FILE *f, const struct instruction *self)
{
  if (!IMM_FIT (self->imm, 9))
    {
      fprintf (stderr, "%d: Branch target too far away, distance: %s%jx\n",
	       self->lineno, self->code & 0x200 ? "-" : "",
	       (uintmax_t) 2*(self->imm+1));
      return 0;
    }
  return output_uint16 (f, self->code | (self->imm & 0x1ff));  
}

static int
output_prefix (FILE *f, uint64_t prefix, unsigned n)
{
  assert (n <= 5);
  assert ((prefix >> (12*n)) == 0);
  while (n-- > 0)
    if (!output_uint16 (f, 0x7000 | (0xfff & (prefix >> (12*n)))))
      return 0;
  return 1;  
}

static int
output_imm4 (FILE *f, unsigned code, unsigned dreg, unsigned imm)
{
  return output_uint16 (f, opcode_imm4 (code, dreg, imm));
}

static int
output_imm_instr (FILE *f, const struct instruction *self)
{
  int imm_code;

  if (self->size > 2)
    return output_prefix (f, self->imm >> 4, self->size / 2 - 1)
      && output_imm4 (f, self->code, self->reg[0], self->imm & 15);

  imm_code = imm4_code (self->imm);
  if (imm_code >= 0)
    return output_imm4 (f, self->code, self->reg[0], imm_code);
  else
    {
      fprintf(stderr, "%d: Too large immediate.\n",
	      self->lineno);
      return 0;
    }  
}

int
instruction_output (const struct instruction *self,
		    FILE *f)
{
  assert (!self->expr);
  switch (self->type)
    {
    case INSTRUCTION_INT:
      return output_int (f, self->size, self->imm);
    case INSTRUCTION_STRING:
      return output_string (f, self->size, self->str);
    case INSTRUCTION_NULLARY:
      return output_uint16 (f, self->code);
    case INSTRUCTION_SHIFT:
      /* Output simd mode as prefix. */
      if (self->imm > 0 && !output_prefix(f, self->imm, 1))
	return 0;

      return output_uint16 (f, opcode_unary (self->code, self->reg[0]));
    case INSTRUCTION_UNARY:
      return output_uint16 (f, opcode_unary (self->code, self->reg[0]));
    case INSTRUCTION_BINARY:
      /* Output simd mode as prefix. */
      if (self->imm > 0 && !output_prefix(f, self->imm, 1))
	return 0;

      return output_uint16 (f, opcode_binary (self->code, 
					      self->reg[0],
					      self->reg[1]));
    case INSTRUCTION_TERNARY:
      return output_ternary (f, self->code, 
			     self->reg[0], self->reg[1], self->reg[2]);

    case INSTRUCTION_BRANCH:
      return output_branch (f, self);
    case INSTRUCTION_IMM:
      return output_imm_instr (f, self);
    case INSTRUCTION_MEMORY:
      if (self->size == 2)
	{
	  int imm = imm4_code (self->imm);
	  if (imm >= 0)
	    return output_ternary (f, self->code, self->reg[0],
				   imm, self->reg[1]);
	  else
	    {
	      fprintf(stderr, "%d: Too large immediate.\n",
		      self->lineno);
	      return 0;
	    }
	}
      else
	return output_prefix (f, self->imm >> 4, self->size / 2 - 1)
	  && output_ternary (f, self->code, self->reg[0],
			     self->imm & 15, self->reg[1]);
      
    default:
      fprintf (stderr, "instruction_output: type %d not implemented.\n",
	       self->type);
      return 0;
    }
}
