/* symbol.h */

/* Copyright (C) 2006, 2013, 2014  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_SYMBOL_H
#define INSTR16_SYMBOL_H

#include <stdint.h>
#include <stdio.h>

struct instruction;

struct symbol_table;

struct symbol_table *
symbol_table_create (void);

enum symbol_type { SYMBOL_UNDEFINED, SYMBOL_LABEL, SYMBOL_CONSTANT };

struct symbol
{
  unsigned name_length;
  const char *name;

  unsigned first_seen_lineno;
  unsigned definition_lineno;

  enum symbol_type type;
  union {
    struct instruction *instruction;
    int64_t value;
  } definition;

  /* For the symbol hash tables */
  unsigned hash_value;
  struct symbol *next;
};

struct symbol *
symbol_intern (struct symbol_table *self,
	       const char *name, unsigned name_length,
	       unsigned lineno);

int
symbol_bind_label (struct symbol *symbol,
		   struct instruction *definition, unsigned lineno);

int
symbol_bind_constant (struct symbol *symbol,
		      uint64_t definition, unsigned lineno);

int
symbol_value (const struct symbol *symbol, int64_t *value);

int
symbol_output_map(struct symbol_table *self,
		  FILE *f);

#endif /* INSTR16_SYMBOL_H */
