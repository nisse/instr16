/* xalloc.h */

/* Copyright (C) 2006, 2013, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_XALLOC_H
#define INSTR16_XALLOC_H

#include <stdlib.h>

void *
xalloc (size_t size);

void *
xrealloc (void *p, size_t size);

void *
xmemdup (const void *p, size_t size);

char *
xstrndup (const char *s, size_t size);

#define NEW(type, var) type *var = xalloc(sizeof(type))

#endif /* INSTR16_XALLOC_H */
