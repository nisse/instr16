/* program.h */

/* Copyright (C) 2006, 2013  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_PROGRAM_H
#define INSTR16_PROGRAM_H

#include <stdio.h>

struct instruction;
struct symbol_table;
struct symbol;

struct program
{
  struct symbol_table *symbols;

  struct instruction *instr;
};

struct program *
program_create (void);

void
program_add_instruction (struct program *self,
			 struct instruction *instruction);

void
program_define_label (struct program *self, struct symbol *label,
		      struct instruction *instruction, unsigned lineno);

/* Main passes */
struct program *
program_parse_file (const char *filename, FILE *f);

int
program_allocate (struct program *self);

int
program_output (struct program *self, FILE *f);

#endif /* INSTR16_PROGRAM_H */
