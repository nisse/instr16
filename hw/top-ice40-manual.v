/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "ice40-manual-clock.v"
`include "top-ice40.v"

module top(input clk_12MHz, input button_pin,
	   // Leds, labeled D2 - D9 on the board.
	   output [9:2] d);
   wire clk;
   wire txd;  // Ignored

   manual_clock clock(clk_12MHz, button_pin, clk);
   main ice40_main(clk, 1'b0 /* rxd */, txd,
		   d[9:2]);
endmodule
