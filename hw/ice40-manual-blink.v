`include "ice40-manual-clock.v"

module top(input clk_12MHz, input button_pin,
	   output reg [8:2] d, output clk_led);
   wire clk;

   manual_clock clock(clk_12MHz, button_pin, clk);

   initial
     d <= 0;

   always @(posedge clk)
     d[5:2] <= d[5:2] + 1;

   assign clk_led = clk;
endmodule
