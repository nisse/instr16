/* Copyright (C) 2015, 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`ifndef ADDRESS_WIDTH
`define ADDRESS_WIDTH 20
`endif

// `define ENABLE_LOGIC_ANALYZER
// `define ENABLE_UNDERCLOCK


/* Memory map:

 0x00000 - 0x1ffff  RAM (8 KB, 1024 64-bit words)
 0x80000            TTY read port
 0x80008            TTY write port
 0x80010            LED port (read/write)
*/

module storage (input clk, input rst,
		input 		  cyc, input stb,
		input 		  we, output reg ack,
		output reg 	  stall,
		output reg	  err,
		input [ADDRESS_WIDTH-1:3] adr,
		input [63:0] 	  dat_write,
		output reg [63:0] dat_read,

		input 		  rxd,
		output 		  txd,
		output reg [5:0]  leds);
   parameter ADDRESS_WIDTH = `ADDRESS_WIDTH;
   parameter RAM_SIZE = 10;
   parameter CLK_FREQUENCY = 12000000;

   wire ram_sel;
   wire tty_sel;
   wire led_sel;
   wire ram_ack;
   wire tty_ack;
   wire tty_stall;
   wire tty_err;

   wire [63:0] ram_dat_read;
   wire [63:0] tty_dat_read;

   assign ram_sel = cyc & (adr[ADDRESS_WIDTH-1:RAM_SIZE+3] == 0);
   assign tty_sel = cyc & (adr[ADDRESS_WIDTH-1:4] == 'h8000);
   assign led_sel = cyc & (adr[ADDRESS_WIDTH-1:3] == 'h10002);

   ram_unit #(RAM_SIZE)
   ram(clk, rst, cyc, ram_sel & stb, we, ram_ack,
       adr[RAM_SIZE+2:3], dat_write, ram_dat_read);

   tty_unit #(CLK_FREQUENCY)
   tty (clk, rst, cyc, tty_sel & stb, we, tty_ack, tty_stall, tty_err,
		  adr[3], dat_write, tty_dat_read, rxd, txd);

   always @(*) begin
      if (ram_sel) begin
	 dat_read = ram_dat_read;
	 stall = 1'b0;
	 ack = ram_ack;
	 err = 1'b0;
      end
      else if (tty_sel) begin
	 dat_read = tty_dat_read;
	 stall = tty_stall;
	 ack = tty_ack;
	 err = tty_err;
      end
      else if (led_sel) begin
	 dat_read = leds;
	 stall = 1'b0;	 
	 ack = 1'b1;
	 err = 1'b0;
      end
      else begin
         dat_read = {64{1'bx}};
	 ack = 1'b0;
	 stall = 1'b0;
	 err = cyc & stb;
      end
   end

   always @(posedge clk) begin
      if (rst) begin
	 leds <= 0;
      end
      else if (led_sel & stb & we) begin
	 leds <= dat_write[5:0];
      end
   end
endmodule // storage

module main(input clk_12MHz, input rxd, output txd,
	    output 	 dcd,
	    // Leds, labeled D2 - D9 on the board.
	    output [9:2] d);
   reg [5:0]   reset_cnt;

   wire        rst;
   wire        cpu_halted;
   wire        cpu_fail;

   wire        mem_cyc;
   wire        mem_stb;
   wire        mem_we;
   wire        mem_ack;
   wire        mem_stall;
   wire        mem_err;
   wire [ADDRESS_WIDTH-1:3] mem_adr;
   wire [63:0] mem_dat_write;
   wire [63:0] mem_dat_read;

   wire [63:0] diagnostics;
   wire        tty_txd;

   localparam ADDRESS_WIDTH = `ADDRESS_WIDTH;
   localparam RAM_SIZE = 10;

`ifdef ENABLE_UNDERCLOCK
   localparam CLK_FREQUENCY = 6000000;
   reg 	       clk;
   initial clk <= 0;
   always @(posedge clk_12MHz) clk <= !clk;
`else
   localparam CLK_FREQUENCY = 12000000;
   wire        clk;
   assign clk = clk_12MHz;
`endif

   storage #(.ADDRESS_WIDTH(ADDRESS_WIDTH),
	     .RAM_SIZE(RAM_SIZE),
	     .CLK_FREQUENCY(CLK_FREQUENCY))
   main_storage (clk, rst, mem_cyc, mem_stb,
		 mem_we, mem_ack, mem_stall, mem_err,
		 mem_adr, mem_dat_write, mem_dat_read,
		 rxd, tty_txd, d[7:2]);

   cpu #(.INITIAL_STACK(8 << RAM_SIZE),
	 .ADDRESS_WIDTH(ADDRESS_WIDTH))
   main_cpu (clk_12MHz, rst,
	     mem_cyc, mem_stb, mem_we,
	     mem_ack, mem_stall, mem_err,
	     mem_adr, mem_dat_write, mem_dat_read,
	     cpu_halted, cpu_fail,
	     diagnostics);

   // According to https://github.com/cliffordwolf/icestorm/issues/76,
   // reading BRAM cells gives always zero for the first 3 us (36
   // cycles) after configuration and/or power on. So count to 63
   // cycles before starting processor.
   assign rst = (reset_cnt != 6'b111111);
   assign d[9] = cpu_halted;
   assign d[8] = cpu_fail;

   initial begin
      reset_cnt <= 6'b000000;
   end

   always @(posedge clk_12MHz)
     if (rst)
       reset_cnt <= reset_cnt + 1;

`ifdef ENABLE_LOGIC_ANALYZER
   logic_analyzer #(.CLK_FREQUENCY(CLK_FREQUENCY))
     analyzer(clk, rst, diagnostics, txd);
`else
   assign txd = tty_txd;
`endif
   // This signal is negated, so set to zero when processor is active.
   assign dcd = (rst | cpu_halted);
   
endmodule
