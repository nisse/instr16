#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <termios.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <fcntl.h>

static void
die (const char *format, ...)
{
  va_list args;
  va_start (args, format);
  vfprintf (stderr, format, args);
  va_end (args);

  exit(EXIT_FAILURE);
}

static void
usage (void)
{
  fprintf (stderr, "Usage: recv-serial [OPTIONS]\n"
	   "Options:\n"
	   " -d DEVICE Serial device to use (default '/dev/ttyUSB1')\n"
	   " -m MODE   Mode, 'ascii' (default), 'line' or 'logic'\n"
	   " -i        Interactive; also send data from stdin.\n");

  exit (EXIT_FAILURE);
}

/* The DCD (data carrier detect) is high when processor is running,
   i.e., not reset and not halted. */
static int
read_dcd(int fd)
{
  int bits;
  if (ioctl(fd, TIOCMGET, &bits) != 0)
    die ("ioctl TIOCMGET failed: %s\n", strerror(errno));

  return (bits & TIOCM_CAR) != 0;
}

static int
open_serial (const char *device, int enable_write)
{
  struct termios termios;
  int fd = open (device, enable_write ?  O_RDWR : O_RDONLY);

  if (fd < 0)
    die ("failed to open %s: %s\n", device, strerror (errno));

  if (tcgetattr (fd, &termios) < 0)
    die ("failed to get terminal attributes: %s\n", strerror (errno));

  if (cfsetspeed (&termios, B19200) < 0)
    die ("failed to set baud rate: %s\n", strerror (errno));

  cfmakeraw (&termios);

  termios.c_cc[VTIME] = 1;
  termios.c_cc[VMIN] = 0;

  if (tcsetattr (fd, TCSANOW, &termios) < 0)
    die ("failed to set terminal attributes: %s\n", strerror (errno));

  return fd;
}

/* Returns 1 on success, -1 on EOF, 0 if no char is available. */
static int
try_read_char (int fd, unsigned char *c)
{
  fd_set read_fds;
  struct timeval tv = {0,0};

  FD_ZERO (&read_fds);
  FD_SET (fd, &read_fds);

  int res = select (fd +1, &read_fds, NULL, NULL, &tv);
  if (res < 0)
    {
      if (errno == EINTR)
	return 0;
      die ("select failed: %s\n", errno);
    }
  if (res == 0)
    return 0;

  res = read(fd, c, 1);
  if (res < 0)
    {
      if (errno == EINTR)
	return 0;
      die ("read input failed: %s\n", errno);
    }
  return res == 0 ? -1 : 1;
}

enum mode {
  MODE_ASCII,
  MODE_LINE,
  MODE_LOGIC,
};

int
main(int argc, char **argv)
{
  const char *device = "/dev/ttyUSB1";
  enum mode mode = MODE_ASCII;
  int interactive = 0;
  int c;

  int fd;
  size_t i;

  while ((c = getopt (argc, argv, "d:m:i")) != -1)
    switch (c)
      {
      case 'd':
	device = optarg;
	break;
      case 'm':
	if (strcmp(optarg, "ascii") == 0)
	  mode = MODE_ASCII;
	else if(strcmp(optarg, "logic") == 0)
	  mode = MODE_LOGIC;
	else if (strcmp (optarg, "line") == 0)
	  mode = MODE_LINE;
	else
	  usage();
	break;
      case 'i':
	interactive = 1;
	break;
      default:
	usage ();
      }

  fd = open_serial (device, interactive);

  /* Unbuffered output. */
  setbuf (stdout, NULL);

  for (i = 0; ;)
    {
      unsigned char c;
      ssize_t res;

      res = read (fd, &c, 1);
      if (res < 0)
	{
	  if (errno != EINTR)
	    die ("read failed: %s\n", strerror(errno));
	}
      else if (res == 0)
	{
	  if (read_dcd (fd))
	    {
	      if (interactive)
		switch (try_read_char(STDIN_FILENO, &c))
		  {
		  default:
		    /* Do nothing */
		    break;
		  case -1:
		    /* EOF */
		    if (mode == MODE_ASCII)
		      return EXIT_SUCCESS;
		    /* Otherwise, stop sending, but keep receiving. */
		    interactive = 0;
		    break;
		  case 1:
		    do
		      res = write(fd, &c, 1);
		    while (res < 0 && errno == EINTR);
		    if (res < 0)
		      die ("write failed: %s\n", strerror (errno));
		    else if (res == 0)
		      die ("unexpected result from write: %d\n", res);
		  }
	    }
	  else if (i > 0)
	    return EXIT_SUCCESS;
	}
      else
	{
	  if (mode == MODE_LOGIC)
	    {
	      unsigned bit;
	      for (bit = 8; bit--> 0;)
		printf ("%d", (c >> bit) & 1);

	      printf ("%c", (i&7) == 7 ? '\n' : ' ');
	    }
	  else
	    {
	      printf("%c", c);
	      if (mode == MODE_LINE && c == '\n')
		return EXIT_SUCCESS;
	    }
	  i++;
	}
    }
}
