/* Copyright (C) 2015, 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`ifndef ADDRESS_WIDTH
`define ADDRESS_WIDTH 20
`endif

module tty_unit (input clk, input rst,
		 input 		   cyc, input stb,
		 input 		   we, output reg ack,
		 input 		   adr,
		 input [63:0] 	   dat_write,
		 output reg [63:0] dat_read);
   reg [7:0] tty_in;

   always @(posedge clk)
     if (~rst) begin
	if (cyc & stb) begin
	   if (~adr) begin
	      if (we)
		$display("Invalid write to tty input register.");
	      else begin
		 // Iverilog specific, 2^31 means stdin.
		 if ($fscanf('h80000000, "%c", tty_in) == 1)
		   dat_read <= {56'b0, tty_in};
		 else // -1 on EOF or error
		   dat_read <= {64{1'b1}};
	      end
	   end
	   else begin
	      if (~we) begin
		 $display("Invalid read from tty input register.");
		 dat_read <= 0;
	      end
	      else
		$write("%s", dat_write[7:0]);
	   end
	end
	ack <= cyc & stb;
     end
endmodule

module storage (input clk, input rst,
		input 			  cyc, input stb,
		input 			  we, output reg ack,
		output reg 		  err,
		input [ADDRESS_WIDTH-1:3] adr,
		input [63:0] 		  dat_write,
		output reg [63:0] 	  dat_read);
   parameter ADDRESS_WIDTH = `ADDRESS_WIDTH;
   parameter RAM_SIZE = 10;

   wire ram_sel;
   wire tty_sel;
   wire ram_ack;
   wire tty_ack;

   wire [63:0] ram_dat_read;
   wire [63:0] tty_dat_read;

   assign ram_sel = cyc & (adr < (8 << RAM_SIZE));
   assign tty_sel = cyc & (adr[ADDRESS_WIDTH-1:4] == 'h8000);

   ram_unit #(RAM_SIZE)
   ram(clk, rst, cyc, ram_sel & stb, we, ram_ack,
       adr[RAM_SIZE+2:3], dat_write, ram_dat_read);

   tty_unit tty (clk, rst, cyc, tty_sel & stb, we, tty_ack,
		 adr[3], dat_write, tty_dat_read);

   always @(*) begin
      if (ram_sel) begin
	 dat_read = ram_dat_read;
	 ack = ram_ack;
      end
      else if (tty_sel) begin
	 dat_read = tty_dat_read;
	 ack = tty_ack;
      end
      else begin
         dat_read = {64{1'bx}};
	 ack = 0;
      end
   end

   always @(posedge clk) begin
      if (rst) begin
	 err <= 0;
      end
      else begin
	 if (ram_sel & tty_sel) begin
	    $display("Internal error in address decode.");
	    $finish_and_return(2);
	 end
	 if (cyc & stb & ~(ram_sel | tty_sel)) begin
	    $display("Invalid address %x, we %b.", adr, we);
	    err <= 1'b1;
	 end
	 else
	   err <= 1'b0;
      end
   end
endmodule // storage

module main;
   string      img;
   reg 	       clk;
   reg 	       rst;
   wire        cpu_halted;
   wire        cpu_error;

   wire        mem_cyc;
   wire        mem_stb;
   wire        mem_we;
   wire        mem_ack;
   wire        mem_err;
   wire [ADDRESS_WIDTH-1:3] mem_adr;
   wire [63:0] mem_dat_write;
   wire [63:0] mem_dat_read;

   integer     i;
   integer     quiet = 0;
   integer     decimal = 0;

   wire [63:0] diagnostics;

   localparam ADDRESS_WIDTH = `ADDRESS_WIDTH;
   localparam RAM_SIZE = 10;

   storage #(.ADDRESS_WIDTH(ADDRESS_WIDTH),
	     .RAM_SIZE(RAM_SIZE))
   main_storage (clk, rst, mem_cyc, mem_stb,
		 mem_we, mem_ack, mem_err, mem_adr,
		 mem_dat_write, mem_dat_read);

   cpu #(.INITIAL_STACK(8 << RAM_SIZE),
	 .ADDRESS_WIDTH(ADDRESS_WIDTH))
   main_cpu (clk, rst,
	     mem_cyc, mem_stb, mem_we,
	     mem_ack, 1'b0 /* stall */,
	     mem_err,
	     mem_adr, mem_dat_write, mem_dat_read,
	     cpu_halted, cpu_error,
	     diagnostics);

   function [63:0] read_reg(input [3:0] i);
      read_reg = main_cpu.rf.storage.rf[i];
   endfunction
   initial begin
      if ($value$plusargs("quiet=%b", quiet))
	quiet = 1;      
      if ($value$plusargs("decimal=%b", decimal))
	decimal = 1;      
      if (!$value$plusargs("img=%s", img)) begin
	 $display("Specify image file with +img=<image>.");
	 $finish_and_return(1);
      end
      $display("Using image: %s", img);
      $readmemh(img, main_storage.ram.mem);
      $display("Initial memory word: mem[0] = %x", main_storage.ram.mem[0]);
      clk <= 0;
      rst <= 1;
   end

   always
      #10 begin
	 clk <= !clk;
	 if (clk)
	   rst <= 0;
      end

   always @(posedge clk)
     if (cpu_halted) begin
	$display("cycle count: %d", main_cpu.cycle_counter);
	$display("instr count: %d", main_cpu.instr_counter);
	$display("pc: 0x%x", {main_cpu.fetch.pc, 1'b0});

	for (i = 0; i < 15; i = i + 1)
	  if (decimal)
	    $display("reg %d: %d %d", i, read_reg(i), $signed(read_reg(i)));
	  else
	    $display("reg %d: %x", i, read_reg(i));
	if (cpu_error) begin
	   $display("halt on failure");
	   $finish_and_return(2);
	end
	else
	  $finish_and_return (read_reg(0) != 0);
     end
   always @(posedge clk)
     if ($time > 3000000) begin
	/* Exit for run-away programs */
	$display("Terminating.");
	$finish_and_return(2);
     end
   always @(posedge clk)
     if (!quiet && !rst) begin
	$display("%b %b %b %b %b %b %b %b",
		 diagnostics[63:56], diagnostics[55:48],
		 diagnostics[47:40], diagnostics[39:32],
		 diagnostics[31:24], diagnostics[23:16],
		 diagnostics[15:8], diagnostics[7:0]);
	if (main_cpu.rf.write_enable) begin
	   $display("R%0d <-- %x", main_cpu.rf.write_idx, main_cpu.rf.write_data);
	end
	if (main_cpu.cc_write_enable & main_cpu.result_ready)
	  $display("cc <-- %d", main_cpu.cc_next);
	if (main_cpu.fetch.pc_write_enable)
	  $display("pc <-- %x", {main_cpu.fetch.pc_write, 1'b0});

	if (main_cpu.ldst_ready) begin
	   if (main_cpu.dst_reg_write_enable)
	     $display("load, address %x, value %x",
		      main_cpu.load_store_eu.effective_address,
		      main_cpu.load_store_eu.mem_data_read);
	   else
	     $display("store, address %x <-- %x",
		      main_cpu.load_store_eu.effective_address,
		      main_cpu.load_store_eu.mem_data_write);
	end
     end
endmodule // main
