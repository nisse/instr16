module manual_clock(input clk_12MHz, input button_pin, output reg clk
`ifdef MANUAL_CLOCK_DIAGNOSTICS
		    , output [1:0] diagnostics
`endif
);
   wire inv_button;
   wire button;
   // 12e6 / 2^15 \approx 366, period \appr 2.7 ms.
   // To get that, we should toggle the slower clock once
   // every 2^14 cycles.
   reg [13:0] ctr_12MHz;
   reg 	      clk_366Hz;
   // Keeps history for appr. 41 ms, for debouncing.
   reg [14:0] history;

   SB_IO #(.PIN_TYPE(6'b000001),
	   .PULLUP(1'b1))
   io_pin(.PACKAGE_PIN(button_pin),
	  .LATCH_INPUT_VALUE(),
	  .CLOCK_ENABLE(),
	  .INPUT_CLK(),
	  .OUTPUT_CLK(),
	  .OUTPUT_ENABLE(),
	  .D_OUT_0(),
	  .D_OUT_1(),
	  // When the button is pressed, signal is grounded to zero.
	  .D_IN_0(inv_button),
	  .D_IN_1());

   initial begin
      ctr_12MHz <= 0;
      clk_366Hz <= 0;
      clk <= 0;
      history <= 0;
   end

   always @(posedge clk_12MHz) begin
      if (&ctr_12MHz)
	clk_366Hz <= ~clk_366Hz;
      ctr_12MHz <= ctr_12MHz + 1;
   end

   assign button = ~inv_button;

   always @(posedge clk_366Hz) begin
      if (&history)
	clk <= 1'b1;
      else if (~|history)
	clk <= 1'b0;
      history = (history << 1) | button;
   end

`ifdef MANUAL_CLOCK_DIAGNOSTICS
   assign diagnostics[0] = button;
   assign diagnostics[1] = clk;
`endif
endmodule // manual_clock
