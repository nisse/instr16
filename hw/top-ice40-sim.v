`include "top-ice40.v"

module tb_top();

   reg clk;
   wire rxd;
   wire txd;
   wire [9:2] leds;

   initial
     clk <= 0;

   always
     #10 clk <= ~clk;

   main ice40_main (clk, rxd, txd, leds);

   always @(posedge clk) begin
      if (ice40_main.main_storage.led_sel
	  && ice40_main.main_storage.cyc
	  && ice40_main.main_storage.stb
	  && ice40_main.main_storage.we) begin
	 $display("leds: %b", ice40_main.main_storage.dat_write[5:0]);
      end
      if (ice40_main.cpu_halted) begin
	 if (ice40_main.cpu_fail) begin
	    $display("final leds: %b", leds);
	    $display("cpu failed");
	    $finish_and_return(1);
	 end
	 else if (!ice40_main.main_storage.tty.tty_tx.tx_active_q) begin
	    $display("final leds: %b", leds);
	    $finish_and_return(0);
	 end
      end
   end

   always @(posedge clk)
     if (ice40_main.rst)
       $display("reset");

   always @(posedge clk)
     if (ice40_main.main_cpu.decode_ready & !ice40_main.cpu_halted)
       $display("decoded: %x", ice40_main.main_cpu.instr);

   always @(posedge clk) begin
      if (ice40_main.main_cpu.rf.write_enable) begin
	 $display("R%0d <-- %x",
		  ice40_main.main_cpu.rf.write_idx,
		  ice40_main.main_cpu.rf.write_data);
      end
      if (ice40_main.main_cpu.cc_write_enable)
	$display("cc <-- %d", ice40_main.main_cpu.cc_next);
      if (ice40_main.main_cpu.fetch.pc_write_enable)
	$display("pc <-- %x", {ice40_main.main_cpu.fetch.pc_write, 1'b0});
   end

   always @(posedge clk)
     if (ice40_main.main_cpu.ldst_pending
	 & ice40_main.main_cpu.ldst_ack) begin
	if (ice40_main.main_cpu.dst_write_enable)
	  $display("load, address %x, value %x",
		   ice40_main.main_cpu.effective_address,
		   ice40_main.main_cpu.load_data_rot);
	else
	  $display("store, address %x <-- %x",
		   ice40_main.main_cpu.effective_address,
		   ice40_main.main_cpu.store_data);
     end

   always @(posedge clk) begin
      if (ice40_main.main_storage.tty.tty_tx.tx_active_q
	  & ice40_main.main_storage.tty.tty_tx.txbaud_q == 100)
	$display("txd: %b", txd);
   end
endmodule
