/* Copyright (C) 2022  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main(input clk, output txd);
   reg [5:0]   reset_cnt;
   reg [5:2]   cnt;

   wire        rst;

   assign rst = (reset_cnt != 6'b111111);

   initial begin
      reset_cnt <= 6'b000000;
      cnt <= 0;
   end

   always @(posedge clk) begin
     if (rst)
       reset_cnt <= reset_cnt + 1;
     else
       cnt <= cnt + 1;
   end
   logic_analyzer la(clk, rst, {8'b00100000 + {2'b00, cnt, 2'b00},
				8'b00100000 + {2'b00, cnt, 2'b01},
				8'b00100000 + {2'b00, cnt, 2'b10},
				8'b00100000 + {2'b00, cnt, 2'b11}}, txd);
endmodule
