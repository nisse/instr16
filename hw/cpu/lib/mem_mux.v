/* Copyright (C) 2015, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Multiplexer for the memory bus. Unit A is for load and store, unit
   B is instruction fetch. A is given higher priority than B. */
module mem_mux (input clk, input rst,

		// Wishbone master interface
		output 	      cyc, output stb,
		output 	      we, input ack,
		input 	      stall,
		output [ADDRESS_WIDTH-1:3] adr,
		output [63:0] dat_write,
		input [63:0]  dat_read,

		input 	      A_cyc, input A_stb,
		input 	      A_we,
		output 	      A_ack, output A_stall,
		input [ADDRESS_WIDTH-1:3]  A_adr,
		input [63:0]  A_dat_write,
		output [63:0] A_dat_read,

		input 	      B_cyc, input B_stb,
		output 	      B_ack, output B_stall,
		input [ADDRESS_WIDTH-1:3]  B_adr,
		output [63:0] B_dat_read);
   parameter ADDRESS_WIDTH = 64;

   reg B_pending;
   wire B_sel;

   assign B_sel = (B_cyc & (B_pending | ~A_cyc));

   assign A_stall = B_sel ? (A_cyc & A_stb) : stall;
   assign A_ack = ~B_sel & ack;

   assign B_stall = ~B_sel ? (B_cyc & B_stb) : stall;
   assign B_ack = B_sel & ack;

   assign cyc = A_cyc | B_cyc;

   assign stb = B_sel ? B_stb : A_stb;
   assign we =  B_sel ? 0 : A_we;
   assign adr = B_sel ? B_adr : A_adr;

   assign dat_write = A_dat_write;
   assign A_dat_read = dat_read;
   assign B_dat_read = dat_read;

   always @(posedge clk) begin
//       $display("mux: cyc %b, stb %b, we %b, ack %b, stall %b, adr %x, data %x",
// 	       cyc, stb, we, ack, stall, adr, dat_read);
//       $display("mux: A_cyc %b, A_stb %b, A_we %b, A_ack %b, A_stall %b, A_adr %x",
// 	       A_cyc, A_stb, A_we, A_ack, A_stall, A_adr);
//       $display("mux: B_cyc %b, B_sel %b, B_stb %b, B_ack %b, B_stall %b, B_adr %x",
// 	       B_cyc, B_sel, B_stb, B_ack, B_stall, B_adr);

      if (rst)
	B_pending <= 0;
      else
	B_pending <= B_sel;
   end
endmodule
