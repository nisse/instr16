/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "instr-defines.vh"

module mul_unit (input clk, input rst, input enable,
		 input 		   op_in,
		 input [63:0] 	   a,
		 input [63:0] 	   b,
		 output 	   pending,
		 output 	   ready,
		 output reg [63:0] res);
   reg         op;
   wire [127:0] c;

   always @(posedge clk) begin
      if (rst) begin
	 op <= 0;
      end
      else if (enable) begin
	 op <= op_in;
      end
   end

   always @(*) begin
      case (op)
	`OP_MUL_LOW:
	  res = c[63:0];
	`OP_MUL_HIGH:
	  res = c[127:64];
	default:
	  res = {64{1'bx}};
      endcase
   end

   // These multipliers latch the a, b inputs for us.
`ifdef ICE40
   mul_3w64 mul(clk, rst, enable, a, b, pending, ready, c);
`else
   mul_4w64 mul(clk, rst, enable, a, b, pending, ready, c);
`endif
endmodule
