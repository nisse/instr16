/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* ALU operations */
`define OP_ALU_AND 0
`define OP_ALU_OR 1
`define OP_ALU_XOR 2
`define OP_ALU_ADD 3
`define OP_ALU_INJ8 4
`define OP_ALU_INJ16 5
`define OP_ALU_INJ32 6
`define OP_ALU_MOVC 7

/* SHIFT operations */
`define OP_SHIFT_SHORT 2'b00
`define OP_SHIFT_EXT   2'b01
`define OP_SHIFT_LONG  2'b10
`define OP_SHIFT_ROT   2'b11

/* CNT operations */
`define OP_COUNT_CLZ 2'b00
`define OP_COUNT_CTZ 2'b01
`define OP_COUNT_CLS 2'b10
`define OP_COUNT_POP 2'b11

/* MUL subtypes TODO: Signed mulhi? */
`define OP_MUL_LOW 0
`define OP_MUL_HIGH 1

/* UNARY operations */
`define OP_UNARY_XSHIFT 0
`define OP_UNARY_BSWAP 1
`define OP_UNARY_FNEG 2
`define OP_UNARY_FABS 3

/* FLOAT subtypes (incomplete) */
`define OP_FLOAT_FMAC 0
`define OP_FLOAT_FLDEXP 1

/* BRANCH conditions */
`define BRANCH_CND_CC_FALSE 2'b00
`define BRANCH_CND_CC_TRUE 2'b01
`define BRANCH_CND_NZ 2'b10
`define BRANCH_CND_ALWAYS 2'b11
