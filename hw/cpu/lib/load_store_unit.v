module load_store_unit(input clk, input rst, input enable,
		       input [ADDRESS_WIDTH-1:0]  effective_address_in,
		       input 			  we_in,
		       input [63:0] 		  store_value_in,
		       output [63:0] 		  load_value,
		       output reg 		  pending,
		       output 			  ready,

		       // Wishbone interface to memory.
		       output 			  mem_cyc,
		       output reg 		  mem_stb,
		       output reg 		  mem_we,
		       output [ADDRESS_WIDTH-1:3] mem_adr,
		       output [63:0] 		  mem_data_write,
		       input [63:0] 		  mem_data_read,

		       input 			  mem_ack,
		       input 			  mem_stall);
   parameter ADDRESS_WIDTH = 64;

   reg [ADDRESS_WIDTH-1:0] 			 effective_address;
   reg [63:0] 					 store_value;
   wire [2:0] 					 shift;

   assign mem_adr = effective_address[ADDRESS_WIDTH-1:3];
   assign shift = effective_address[2:0];

   assign load_value = (mem_data_read << {shift, 3'b0})
     | (mem_data_read >> {-shift[2:0], 3'b0});

   assign mem_data_write = (store_value >> {shift[2:0], 3'b0})
     | (store_value << {-shift[2:0], 3'b0});

   assign mem_cyc = pending;
   assign ready = pending & mem_ack;

   always @(posedge clk) begin
     if (rst) begin
	effective_address <= 0;
	store_value <= 0;
	pending <= 0;
	mem_stb <= 0;
	mem_we <= 0;
     end
     else if (enable & !pending) begin
	effective_address <= effective_address_in;
	store_value <= store_value_in;
	mem_we <= we_in;
	pending <= 1;
	mem_stb <= 1;
     end
     else if (pending) begin
	mem_stb <= mem_stall;
	if (mem_ack)
	  pending <= 0;
     end
   end
endmodule
