/* Copyright (C) 2015, 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "instr-defines.vh"

/* BRANCH subtypes */
`define OP_BRANCH_JMP 0
`define OP_BRANCH_JSR 1
`define OP_BRANCH_BT 2
`define OP_BRANCH_BF 3
`define OP_BRANCH_BNZ 4

module instr_decode (input [15:0]      instr,
		     input	       prefix_active,
		     input [59:0]      prefix,
		     /* Output */

		     output reg	       invalid,
		     output reg	       halt_enable,
		     output reg	       prefix_enable,

		     output reg	       branch_enable,
		     output reg [1:0]  branch_cond,
		     output reg	       link_enable,

		     /* Indices of input registers. */
		     output reg [3:0]  reg_a_idx,
		     output reg [3:0]  reg_b_idx,
		     output reg [3:0]  reg_c_idx,

		     /* Non-zero for SIMD operations. No-care except
		      * for when alu_enable is set and alu_op ==
		      * OP_ALU_ADD, or shift_enable is set. */
		     output reg [1:0]  simd_mode,

		     output reg	       alu_enable,
		     output reg [2:0]  alu_op,
		     output reg	       unary_enable,
		     output reg [1:0]  unary_op,
		     output reg	       shift_enable,
		     output reg [1:0]  shift_op,
		     output reg	       cnt_enable,
		     output reg [1:0]  cnt_op,
		     output reg	       mul_enable,
		     output reg	       mul_op,
		     output reg	       recpr_enable,
		     output reg	       ldst_enable,

		     output reg [1:0]  op_cout,
		     /* 00 -> 0, 01 -> 1, 10 -> cc , 11 -> ~cc */
		     output reg [1:0]  op_cin,
		     output reg [3:0]  op_dst_idx,
		     output reg [63:0] imm_data,
		     /* If set, imm value replaces src register value. */
		     output reg	       imm_enable,
		     // Set if the instruction writes to dst register or cc.
		     output reg	       dst_write_enable,
		     output reg	       cc_write_enable,

		     // Controls use of dst input in the ALU.
		     output reg [1:0]  dst_mode,
		     output reg	       src_neg,
		     output reg	       ind_enable);
   parameter ADDRESS_WIDTH = 64;

   reg [63:0] uimm;

   function [63:0] decode_imm4 (input [3:0] code);
      if (code == 0)
	decode_imm4 = 32;
      else if (code[3] == 0)
	decode_imm4 = {61'b0, code[2:0]};
      else if (code[2] == 0)
	decode_imm4 = {60'b0, 1'b1, code[1:0], 1'b0};
      else
	decode_imm4 = {59'b0, 1'b1, code[1:0], 2'b00};
   endfunction

   function [63:0] decode_imm3 (input [3:0] code);
      case (code)
	4'b0000: decode_imm3 = 33;
	4'b0001: decode_imm3 = 11;
	4'b0010: decode_imm3 = 13;
	4'b0011: decode_imm3 = 15;
	4'b0100: decode_imm3 = 17;
	4'b0101: decode_imm3 = 21;
	4'b0110: decode_imm3 = 25;
	4'b0111: decode_imm3 = 29;
	4'b1000: decode_imm3 = -31;
	4'b1001: decode_imm3 = -9;
	4'b1010: decode_imm3 = -11;
	4'b1011: decode_imm3 = -13;
	4'b1100: decode_imm3 = -15;
	4'b1101: decode_imm3 = -19;
	4'b1110: decode_imm3 = -24;
	4'b1111: decode_imm3 = -27;
	default: decode_imm3 = {64{1'bx}};
      endcase
   endfunction // decode_imm3

   /* Actual decoding. */
   always @(*) begin
      /* Register indices */
      begin
	 reg_a_idx = instr[3:0];
	 reg_b_idx = instr[7:4];
	 reg_c_idx = instr[11:8];
      end
      op_dst_idx = reg_a_idx;

      dst_mode = 2'b01;
      src_neg = 0;

      uimm = (prefix_active
	      ? {prefix[59:0], instr[7:4]}
	      : decode_imm4(instr[7:4]));

      invalid = 0;
      halt_enable = 0;
      prefix_enable = 0;

      simd_mode = 2'bxx;
      alu_enable = 0;
      alu_op = 3'bxxx;
      unary_enable = 0;
      unary_op = 2'bxx;
      shift_enable = 0;
      shift_op = 2'bxx;
      cnt_enable = 0;
      cnt_op = 2'bxx;
      mul_enable = 0;
      mul_op = 1'bx;
      recpr_enable = 0;
      ldst_enable = 0;

      op_cout = 2'bxx;
      op_cin = 2'bxx;
      branch_enable = 1'b0;
      branch_cond = 2'bxx;
      link_enable = 0;

      cc_write_enable = 0;
      dst_write_enable = 0;
      ind_enable = 1'bx;
      imm_enable = 1'b0;
      imm_data = {64{1'bx}};

      casez (instr)
	/* Load and store with offset */
	'b00??_????_????_????: begin
	   ldst_enable = 1;
	   ind_enable = 1'b1;
	   src_neg = instr[12];
	   imm_data = uimm;
	   imm_enable = 1;
	   dst_write_enable = ~instr[13];
	end
	/* Load and store with index. */
	'b0100_????_????_????: begin
	   if (reg_b_idx == reg_c_idx) begin
	      invalid = 1;
	   end
	   else begin
	      ldst_enable = 1;
	      ind_enable = 1'b1;
	      dst_write_enable = (reg_b_idx < reg_c_idx);
	   end
	end
	/* Long shift */
	'b0101_????_????_????: begin
	   shift_enable = 1;
	   if (reg_c_idx == 15) begin
	      shift_op = `OP_SHIFT_EXT;
	   end
	   else
	     shift_op = `OP_SHIFT_LONG;
	   simd_mode = 2'b00;
	   dst_write_enable = 1;
	end
	// Bit counting instructions.
	'b0110_??00_0000_????: begin
	   cnt_enable = 1;
	   cnt_op = instr[11:10];
	   dst_write_enable = 1;
	end
	// Immediate shift left.
	'b0110_00??_????_????: begin
	   shift_enable = 1;
	   shift_op = `OP_SHIFT_SHORT;
	   /* Ignores higher bits of prefix. FIXME: Treat as invalid? */
	   simd_mode = prefix_active ? prefix[1:0] : 2'b00;
	   dst_write_enable = 1;
	   imm_data = {58'b0, instr[9:4]};
	   imm_enable = 1;
	end
	// Immediate logical shift right.
	'b0110_01??_????_????: begin
	   shift_enable = 1;
	   shift_op = `OP_SHIFT_SHORT;
	   /* Ignores higher bits of prefix. FIXME: Treat as invalid? */
	   simd_mode = prefix_active ? prefix[1:0] : 2'b00;
	   dst_write_enable = 1;
	   // FIXME: Change opcode encoding to make this simpler?
	   imm_data = {1'b0, {57{1'b1}}, -instr[9:4]};
	   imm_enable = 1;
	end
	// Immediate arithmetic shift right.
	'b0110_10??_????_????: begin
	   shift_enable = 1;
	   shift_op = `OP_SHIFT_SHORT;
	   /* Ignores higher bits of prefix. FIXME: Treat as invalid? */
	   simd_mode = prefix_active ? prefix[1:0] : 2'b00;
	   dst_write_enable = 1;
	   imm_data = {{58{1'b1}}, -instr[9:4]};
	   imm_enable = 1;
	end
	// Immediate rotate left.
	'b0110_11??_????_????: begin
	   shift_enable = 1;
	   shift_op = `OP_SHIFT_ROT;
	   /* Ignores higher bits of prefix. FIXME: Treat as invalid? */
	   simd_mode = prefix_active ? prefix[1:0] : 2'b00;
	   dst_write_enable = 1;
	   imm_data = {58'b0,instr[9:4]};
	   imm_enable = 1;
	end
	/* Prefix */
	'b0111_????_????_????: begin
	   prefix_enable = 1;
	   imm_data = {52'b0, instr[11:0]};
	   imm_enable = 1;
	end
	/* Branch */
	'b1000_????_????_????,
	'b1001_00??_????_????: begin
	   /* Offset relative to pc. */
	   src_neg = instr[9];
	   imm_data = {
		       {(64-ADDRESS_WIDTH){1'bx}},
		       (prefix_active
			? {prefix[ADDRESS_WIDTH-11:0], instr[8:0]}
			: {{(ADDRESS_WIDTH-10){1'b0}}, instr[8:0]}),
		       1'b1
		       };
	   imm_enable = 1;
	   ind_enable = 1;
	   branch_enable = 1;

	   case (instr[12:10])
	     `OP_BRANCH_JMP: 
	       branch_cond = `BRANCH_CND_ALWAYS;
	     `OP_BRANCH_JSR: begin
		branch_cond = `BRANCH_CND_ALWAYS;
		link_enable = 1;
		/* For link register */
		op_dst_idx = 14;
		dst_write_enable = 1;
	     end
	     `OP_BRANCH_BT:
	       branch_cond = `BRANCH_CND_CC_TRUE;
	     `OP_BRANCH_BF:
	       branch_cond = `BRANCH_CND_CC_FALSE;
	     `OP_BRANCH_BNZ: begin
		branch_cond = `BRANCH_CND_NZ;
		reg_b_idx = 8;
	     end
	     default:
		invalid = 1;
	   endcase

	end
	/* Float instructions, 1001 1..., not implemented. */
	/* Immediate add */
	'b1010_????_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   src_neg = instr[8];
	   imm_data = uimm;
	   imm_enable = 1;
	   op_cin = instr[11] ? 2'b10 : {1'b0,src_neg};
	   op_cout = instr[10:9];
	   cc_write_enable = |op_cout;
	   dst_write_enable = 1;
	end
	/* Immediate mov */
	'b1011_000?_????_????: begin
	   /* FIXME: Recognize pc destination as a jump. */
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   imm_data = uimm;
	   imm_enable = 1;
	   src_neg = instr[8];
	   dst_mode = 2'b00;
	   op_cin = {1'b0, src_neg};
	   dst_write_enable = 1;
	end
	/* Immediate and */
	'b1011_001?_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_AND;
	   dst_write_enable = 1;
	   src_neg = instr[8];
	   imm_data = uimm;
	   imm_enable = 1;
	end
	/* Immediate or */
	'b1011_010?_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_OR;
	   dst_write_enable = 1;
	   src_neg = instr[8];
	   imm_data = uimm;
	   imm_enable = 1;
	end
	/* Immediate xor */
	'b1011_011?_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_XOR;
	   dst_write_enable = 1;
	   src_neg = instr[8];
	   imm_data = uimm;
	   imm_enable = 1;
	end
	/* Reserved op codes, 1011 1...,  another 4 immediate instructions? */
	/* Immediate tst */
	'b1100_000?_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_AND;
	   cc_write_enable = 1;
	   src_neg = instr[8];
	   imm_data = uimm;
	   imm_enable = 1;
	end
	/* Immediate cmpeq */
	'b1100_001?_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   imm_data = uimm;
	   imm_enable = 1;
	   src_neg = ~instr[8];
	   op_cin = {1'b0, src_neg};
	   op_cout = 2'b00;
	   cc_write_enable = 1;
	end
	/* Immediate cmpugeq */
	'b1100_010?_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   op_cout = 2'b01;
	   cc_write_enable = 1;
	   src_neg = ~instr[8];
	   imm_data = uimm;
	   imm_enable = 1;
	   if (!prefix_active) // Handle stolen encodings
	      case (instr[8:4])
		'b10001: begin  // cmpsgeq #0
		   op_cout = 2'b10;
		   imm_data = 0;
		end
		'b00010: begin  // cmpeq, #0
		   op_cout = 2'b00;
		   src_neg = 0;
		   imm_data = 0;
		end
		'b00100: begin  // cmpsgt, #8
		   op_cout = 2'b10;
		   imm_data = 9;
		end
		'b01000: begin  // cmpugt, #8
		   imm_data = 9;
		end
	      endcase
	   op_cin = {1'b0, src_neg};
	end
	/* Immediate cmpsgeq */
	'b1100_011?_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   imm_data = uimm;
	   imm_enable = 1;
	   src_neg = ~instr[8];
	   op_cin = {1'b0, src_neg};
	   op_cout = 2'b10;
	   cc_write_enable = 1;
	end
	/* Special immediate cmpugt and cmpsgt. */
	'b1100_100?_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   imm_data = decode_imm3 (instr[7:4]);
	   imm_enable = 1;
	   src_neg = 1;
	   op_cin = 2'b01;
	   op_cout = {instr[8], ~instr[8]};
	   cc_write_enable = 1;
	end
	/* Reserved immediate op codes */
	/* Add and sub */
	'b1101_????_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   /* Ignores higher bits of prefix. FIXME: Treat as invalid? */
	   simd_mode = (prefix_active && (instr[10:8] == 3'b000)) ? prefix[1:0] : 2'b00;
	   src_neg = instr[11];
	   op_cin = instr[10] ? 2'b10 : {1'b0,src_neg};
	   op_cout = instr[9:8];
	   cc_write_enable = |op_cout;
	   dst_write_enable = 1;
	end
	/* Mov */
	'b1110_0000_????_????: begin
	   /* Note that reg_a_idx == reg_b_idx is a nop instruction. */
	   /* FIXME: Recognize pc destination as jump. */
	   alu_enable = 1;
	   alu_op = `OP_ALU_OR;
	   dst_mode = 2'b00;
	   dst_write_enable = 1;
	end
	/* And */
	'b1110_0001_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_AND;
	   dst_write_enable = 1;
	end
	/* Or */
	'b1110_0010_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_OR;
	   dst_write_enable = 1;
	end
	/* Xor */
	'b1110_0011_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_XOR;
	   dst_write_enable = 1;
	end
	/* Movt/movf */
	'b1110_010?_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_MOVC;
	   op_cin = {1'b1, instr[8]};
	   dst_write_enable = 1;
	end
	/* Mullo/mulhi */
	'b1110_011?_????_????: begin
	   mul_enable = 1;
	   mul_op = instr[8];
	   dst_write_enable = 1;
	end
	/* Shift */
	'b1110_1000_????_????: begin
	   shift_enable = 1;
	   shift_op = `OP_SHIFT_SHORT;
	   /* For now, no simd operations. */
	   simd_mode = 2'b00;
	   dst_write_enable = 1;
	end
	/* Inject 8 */
	'b1110_1001_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_INJ8;
	   dst_write_enable = 1;
	end
	/* Inject 16 */
	'b1110_1010_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_INJ16;
	   dst_write_enable = 1;
	end
	/* Inject 32 */
	'b1110_1011_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_INJ32 ;
	   dst_write_enable = 1;
	end
	/* Tst */
	'b1110_1100_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_AND;
	   op_cout = 2'b11;
	   cc_write_enable = 1;
	end
	/* Cmpeq */
	'b1110_1101_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   src_neg = 1;
	   op_cin = 2'b01;
	   op_cout = 2'b00;
	   cc_write_enable = 1;
	end
	/* Cmpugeq */
	'b1110_1110_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   op_cout = 2'b01;
	   src_neg = 1;
	   op_cin = 2'b01;
	   cc_write_enable = 1;
	end
	/* Cmpsgeq */
	'b1110_1111_????_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   src_neg = 1;
	   op_cin = 2'b01;
	   op_cout = 2'b10;
	   cc_write_enable = 1;
	end
	/* Plain load and store*/
	'b1111_000?_????_????: begin
	   ldst_enable = 1;
	   ind_enable = 1'b0;
	   dst_write_enable = ~instr[8];
	end
	/* Add r, cc, #0, Sub r, cc, #0 */
	'b1111_1000_0???_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   op_cin = 2'b10;
	   op_cout = instr[5:4];
	   src_neg = instr[6];
	   imm_data = 0;
	   imm_enable = 1;
	   cc_write_enable = |op_cout;
	   dst_write_enable = 1;
	end
	/* xshift */
	'b1111_1000_10??_????: begin
	   unary_enable = 1;
	   unary_op = `OP_UNARY_XSHIFT;
	   op_cout = instr[5:4];
	   cc_write_enable = |op_cout;
	   dst_write_enable = 1;
	end
	/* A few reserved op codes */
	/* Neg, Not */
	'b1111_1001_000?_????: begin
	   alu_enable = 1;
	   alu_op = `OP_ALU_ADD;
	   simd_mode = 2'b00;
	   imm_data = 0;
	   imm_enable = 1;
	   if (&instr[4:0]) begin
	      /* not cc, implemented as carry out from 0 + -1 + (1-cy) */
	      dst_mode = 2'b10; // -1
	      op_cin = 2'b11; // ~cy
	      op_cout = 2'b01;
	      cc_write_enable = 1;
	   end
	   else begin
	      dst_mode = 2'b11;
	      /* instr[4] == 0 means neg, and then we should add carry. */
	      op_cin = {1'b0, ~instr[4]};
	      dst_write_enable = 1;
	   end
	end
	/* Byte swap */
	'b1111_1001_0010_????: begin
	   unary_enable = 1;
	   unary_op = `OP_UNARY_BSWAP;
	   dst_write_enable = 1;
	end
	/* Reciprocal */
	'b1111_1001_0011_????: begin
	   recpr_enable = 1;
	   dst_write_enable = 1;
	end
	/* Jsr indirect */
	'b1111_1001_0100_????: begin
	   branch_enable = 1;
	   branch_cond = `BRANCH_CND_ALWAYS;
	   link_enable = 1;
	   /* For link register */
	   op_dst_idx = 14;
	   dst_write_enable = 1;
	end
	/* Fneg */
	'b1111_1001_0101_????: begin
	   unary_enable = 1;
	   unary_op = `OP_UNARY_FNEG;
	   dst_write_enable = 1;
	end
	/* Fabs */
	'b1111_1001_0110_????: begin
	   unary_enable = 1;
	   unary_op = `OP_UNARY_FABS;
	   dst_write_enable = 1;
	end
	/* Halt */
	'b1111_1111_1111_1111: begin
	   halt_enable = 1;
	end
	default: begin
	   invalid = 1;
	end
      endcase
   end
endmodule
