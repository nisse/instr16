/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "instr-defines.vh"

/* How to think about stages?

   ID: Decode instr, read operands
       Latch instruction word at clock edge k
       Decoded instruction and operands ready by clock edge k+1

   EX: Functional units, e.g., ALU.
       Latch op and operands at clock edge k+1.
       Output ready at clock edge k+2

   WB: Store result
       Store in in reg file on clock k+2
 */


   /* FIXME: Take a separate complement input? Perhaps we could then
      eliminate negation of immediate operands in the decode stage.
      Potential problem with negative immediates and carry in:

      add r1, r2       neg = 0, cin = 0
      add r1, cc, r2   neg = 0, cin = cc
      sub r1, r2       neg = 1, cin = 1
      sub r1, cc, r2   neg = 1, cin = cc
      add r1, #x       neg = 0, cin = 0
      add r1, #-x      neg = 1, cin = 1
      add r1, cc, #x   neg = 0, cin = cc
      sub r1, #x       neg = 1, cin = 1
      sub r1, cc, #x   neg = 1, cin = cc

      add r1, cc, #-x  
        r1 + cc + -x = r1 + cc + ~x + 1
    
      sub r1, cc, #-x
        r1 + cc + x - 1 = r1 + cc + x
    
      But we have perhaps solved this already by not having any
      immediate sub instructions, only add, and we can then let the
      assembler adjust the imm constant.
    
      Another potential problem is rsb (reverse subtract), with
      negative immediate, then we get two negeted operands... If we do
      reverse subtract, support only positive immediates?

      rsb r1, #x       neg = 1, cin = 1
      rsb r1, cc, #x   neg = 1, cin = cc
    */
/* The a_mode is interpreted as follows:
    00  Use 0
    01  Use a
    10  Use -1
    11  Use ~a
*/
module alu_unit (input clk, input rst, input enable,
		 input [2:0] 	   op_in,
		 input [1:0] 	   simd_mode,
		 input [1:0] 	   a_mode,
		 input [63:0] 	   a_in,
		 input 		   b_neg,
		 input [63:0] 	   b_in,
		 input 		   c_in,
		 input [1:0] 	   ccc_in,
		 output reg [63:0] res,
		 output reg 	   cc);
   reg [63:0]  a;
   reg [63:0]  b;
   reg	       c;
   reg [2:0]   op;
   // Bit 0 means that simd_mode >= 1, bit 1 that simd_mode >= 2, and
   // bit 3 that simd_mode = 3.
   reg [2:0]   simd;
   reg [1:0]   ccc; // condition code control
   reg	       cy;
   reg [63:0]  simd_mask;
   reg [63:0]  simd_cin;

   always @(posedge clk) begin
      if (rst) begin
	 a <= 0;
	 b <= 0;
	 c <= 0;
	 op <= 0;
	 simd <= 0;
	 ccc <= 0;
      end
      else if (enable) begin
	 a <= (a_in & {64{a_mode[0]}}) ^ {64{a_mode[1]}};
	 b <= b_in ^ {64{b_neg}};
	 c <= c_in;
	 op <= op_in;
	 simd <= {&simd_mode, simd_mode[1], |simd_mode};
	 ccc <= ccc_in;
      end
   end

   always @(*) begin
      cc = 0;
      cy = 1'bx;
      simd_mask = {64{1'bx}};
      simd_cin = {64{1'bx}};

      case (op)
	// Note that a & b, a | b and a ^ b are useful as inputs to a
	// Brent-Kung adder.
	`OP_ALU_AND: begin
	   res = a & b;
	   cc = |res;  // For TST instruction
	end
	`OP_ALU_OR:
	  res = a | b;

	`OP_ALU_XOR: begin
	   res = a ^ b;
	end
	`OP_ALU_ADD: begin
	   simd_mask = 64'b0;
	   simd_mask[31] = simd[0];
	   simd_mask[15] = simd[1];
	   simd_mask[47] = simd[1];
	   simd_mask[7] = simd[2];
	   simd_mask[23] = simd[2];
	   simd_mask[39] = simd[2];
	   simd_mask[55] = simd[2];

	   simd_cin = (simd_mask & {64{c}});
	   {cy, res } = ({1'b0, (a & ~simd_mask) | simd_cin}
			 + {1'b0, (b & ~simd_mask) | simd_cin}
			 + {64'b0, c})
	     ^ {1'b0, (a & simd_mask) ^ (b & simd_mask)};
	   case (ccc)
	     'b00:
	       cc = ~(|res);  // For CMPEQ instruction
	     'b01:
	       cc = cy;
	     'b10:
	       /* True if sign-extended result is non-negative */
	       cc = cy ^ a[63] ^ b[63] ^ 1;
	     'b11:
	       cc = ~(a[63] ^ b[63]) & (a[63] ^ res[63]);
	   endcase
	end
	`OP_ALU_INJ8:
	  res = {b[7:0],a[55:0]};
	`OP_ALU_INJ16:
	  res = {b[15:0],a[47:0]};
	`OP_ALU_INJ32:
	  res = {b[31:0],a[31:0]};
	`OP_ALU_MOVC:
	  res = c ? b : a;
      endcase
   end
endmodule
