/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Computed v = floor ((2^128 -1) / d'), where d' is d normalized.
// Mathematical result is 65-bit, but bith d' and v have the highest
// bit always set, and implicit.

module reciprocal_unit (input clk, input rst, input enable,
			input [63:0] d_in, output pending, output ready,
			output [63:0] v, output div_by_zero);

   wire [5:0] cnt;
   reg [63:0] d;
   wire [62:0] d_normalized;
   wire        busy;
   reg 	       start;

   srt_reciprocal recpr (clk, rst, start & !div_by_zero,
			 d_normalized, busy, ready, v);

   clz #(6) clz(d, {div_by_zero, cnt});

   always @(posedge clk) begin
      if (rst) begin
	 d <= 0;
	 start <= 0;
      end
      else if (enable) begin
	 d <= d_in;
	 start <= 1;
      end
      else if (start) begin
	 start <= 0;
      end
   end

   assign d_normalized = d[62:0] << cnt;
   assign pending = start | busy;
endmodule
