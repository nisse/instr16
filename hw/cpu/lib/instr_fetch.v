/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// `define FETCH_TRACE 1

/* Instruction fetch stage. When instr_ready is true, the instr and
   pc_next outputs are the current instruction and the pc of the next
   instruction. */
module instr_fetch(input clk, input rst,
		   /* Control of program counter. */
		   output [ADDRESS_WIDTH-1:1] pc_next,
		   input 	 pc_write_enable,
		   input [ADDRESS_WIDTH-1:1]  pc_write,
		   input 	 instr_wanted,
		   output [15:0] instr,
		   output 	 instr_ready,
		   /* Memory interface. */
		   output 	 mem_cyc,
		   output reg 	 mem_stb,
		   input 	 mem_ack, input mem_stall,
		   output [ADDRESS_WIDTH-1:3] mem_adr,
		   input [63:0]  mem_data);
   parameter ADDRESS_WIDTH = 64;

   reg [ADDRESS_WIDTH-1:1] pc;
   reg [63:0] word;
   reg	      word_valid;
   reg	      ignore_read;

   assign pc_next = pc + 1;
   assign instr_ready = instr_wanted & word_valid;
   assign instr = word[{~pc[2:1], 4'b0} +:16];
   assign mem_adr = pc[ADDRESS_WIDTH-1:3];

   assign mem_cyc = ~word_valid;

   always @(posedge clk) begin
      if (rst) begin
	 pc <= 0;
	 word_valid <= 0;
	 mem_stb <= 1;
	 ignore_read <= 0;
      end
      else begin
	 if (!word_valid) begin
`ifdef FETCH_TRACE
	    $display("fetch: mem_stall %b", mem_stall);
`endif
	    mem_stb <= mem_stall;
	    if (mem_ack) begin
`ifdef FETCH_TRACE
	       $display("fetch: mem_data %x, ignore %b",
			mem_data, ignore_read);
`endif
	       if (ignore_read)
		 ignore_read <= 0;
	       else begin
		  word <= mem_data;
		  word_valid <= 1;
	       end
	    end
	 end
	 if (pc_write_enable) begin
`ifdef FETCH_TRACE
	    $display("fetch: pc_write %x", pc_write << 1);
`endif
	    pc <= pc_write;
	    // FIXME: Could skip reading if we already have this word.
	    word_valid <= 0;
	    mem_stb <= 1;
	    // If we already have a read transaction in progress,
	    // cancel it.
	    if (!(word_valid || mem_ack))
	      ignore_read <= 1;
	 end
	 else if (instr_ready) begin
	    pc <= pc_next;
	    if (pc_next[2:1] == 0) begin
`ifdef FETCH_TRACE
	       $display("fetch: pc_next %x", pc_next << 1);
`endif
	       word_valid <= 0;
	       mem_stb <= 1;
	    end
	 end
      end
   end
endmodule // instr_fetch
