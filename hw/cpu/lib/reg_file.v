/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* To get explicit copies of the RAM (one for each read port), use

   `define EXPLICIT_DUPLICATE

   Needed for yosys-0.17 to be able to synthesize as block RAM. */

/* Register file with read ports, A, B and C, and one write port.

   Reads are asynchronous, but occur on *negative* clock edge.

   Writes are synchronous, with the register file updated on positive
   clock edge, if write_enable is true.

   To support pass through, if write_enable is true and any of the
   read indices equals write_idx, write_data is passed through to the
   corresponding read output immediately. The intention is that
   register values are latched into execution unit inputs on the
   positive clock edge.

   At reset, all registers are cleared to zero. */

/* A three-port RAM, without any pass-through. */
module reg_storage (input clk,
		    input [3:0]       a_idx,
		    output reg [63:0] a_data,
		    input [3:0]       b_idx,
		    output reg [63:0] b_data,
		    input [3:0]       c_idx,
		    output reg [63:0] c_data,
		    input 	      write_enable,
		    input [3:0]       write_idx,
		    input [63:0]      write_data);

   reg [63:0]  rf[0:14];
`ifdef EXPLICIT_DUPLICATE
   reg [63:0]  rf_b[0:14];
   reg [63:0]  rf_c[0:14];
`endif
   integer     i;

   initial
      for (i = 0; i < 15; i = i + 1) begin
	 rf[i] = 0;
`ifdef EXPLICIT_DUPLICATE
	 rf_b[i] = 0;
	 rf_c[i] = 0;
`endif
      end

   always @(negedge clk) begin
      a_data <= rf[a_idx];
`ifdef EXPLICIT_DUPLICATE
      b_data <= rf_b[b_idx];
      c_data <= rf_b[c_idx];
`else
      b_data <= rf[b_idx];
      c_data <= rf[c_idx];
`endif
   end

   always @(posedge clk)
      if (write_enable) begin
	 rf[write_idx] <= write_data;
`ifdef EXPLICIT_DUPLICATE
	 rf_b[write_idx] <= write_data;
	 rf_c[write_idx] <= write_data;
`endif
      end
endmodule

module reg_file (input clk,
		 input [3:0]   a_idx,
		 output reg [63:0] a_data,
		 input [3:0]   b_idx,
		 output reg [63:0] b_data,
		 input [3:0]   c_idx,
		 output reg [63:0] c_data,
		 input	       write_enable,
		 input [3:0]   write_idx,
		 input [63:0]  write_data);
   wire [63:0] stored_a_data;
   wire [63:0] stored_b_data;
   wire [63:0] stored_c_data;

   /* Pass-through logic. */
   always @(*) begin
      if (write_enable && a_idx == write_idx)
	a_data = write_data;
      else
	a_data = stored_a_data;
   end

   always @(*) begin
      if (write_enable && b_idx == write_idx)
	b_data = write_data;
      else
	b_data = stored_b_data;
   end

   always @(*) begin
      if (write_enable && c_idx == write_idx)
	c_data = write_data;
      else
	c_data = stored_c_data;
   end

   reg_storage storage(clk,
		       a_idx, stored_a_data,
		       b_idx, stored_b_data,
		       c_idx, stored_c_data,
		       write_enable, write_idx, write_data);

endmodule // reg_file
