/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "instr-defines.vh"

module cnt_unit(input clk, input rst, input enable,
		input [1:0]  op_in,
		input [63:0] a_in,
		output [6:0] res);
   reg [63:0] a;
   wire [63:0] a_rev;
   reg [1:0]  op;
   reg [63:0] clz_in;
   wire [6:0] clz_out;
   wire [6:0] popc_out;

   clz #(6) ctz(clz_in, clz_out);
   popc popc(a, popc_out);
   bitrev #(64) bitrev(a, a_rev);   

   assign res = (op == `OP_COUNT_POP) ? popc_out : clz_out;

   always @(posedge clk) begin
      if (rst) begin
	 op <= 0;
	 a <= 0;
      end
      else if (enable) begin
	 op <= op_in;
	 a <= a_in;
      end
   end
   always @(*) begin
      case (op)
	`OP_COUNT_CLZ:
	  clz_in = a;
	`OP_COUNT_CTZ:
	  clz_in = a_rev;
	`OP_COUNT_CLS:
	  clz_in = {{63{a[63]}} ^ a[62:0], 1'b1};
	default:
	  clz_in = {64{1'bx}};
      endcase
   end
endmodule // count_unit

module bitrev (input [n-1:0] x, output [n-1:0] r);
   parameter n = 64;
   
   genvar i;
   generate 
      for (i = 0; i < n; i = i + 1) begin : REV
	 assign r[i] = x[n-1-i];
      end
   endgenerate
endmodule
