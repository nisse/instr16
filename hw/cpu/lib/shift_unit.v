/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "instr-defines.vh"

module shift_unit (input clk, input rst, input enable,
		   input [1:0] 	     op_in,
		   input	     simd_mode,
		   input [63:0]      a_in,
		   input [63:0]      b_in,
		   input 	     cc_in,
		   input [63:0]      count_in,
		   output reg [63:0] res);

   reg [1:0]   op;
   reg	       simd;
   reg [63:0]  a;
   reg [63:0]  b;
   reg 	       cc;
   reg [63:0]  count;

   // Double word, from which 64-bit result is extracted, and 63 bits
   // ignored.
   reg [126:0] dw;
   // Permuted simd inputs
   reg [126:0] dw_simd;
   // Shifted
   reg [95:0] dw_shifted;

   reg	       sign;
   reg	       s31;

   reg	       high_and;
   reg	       high_or;
   reg	       out_of_range;

   always @(posedge clk) begin
      if (rst) begin
	 op <= 0;
	 simd <= 0;
	 a <= 0;
	 b <= 0;
	 cc <= 0;
	 count <= 0;
      end
      else if (enable) begin
	 op <= op_in;
	 // Note: 16-bit and 8-bit operations not currently supported.
	 simd <= simd_mode;
	 a <= a_in;
	 b <= b_in;
	 cc <= cc_in;
	 count <= count_in;
      end
   end

   always @(*) begin
      high_and = &count[62:7];
      high_or = |count[62:7];

      if (op == `OP_SHIFT_SHORT) begin
	 high_and = high_and & count[6];
	 high_or = high_or | count[6];
      end
      out_of_range = (count[63:62] == 2'b10) || (high_and != high_or);

      // Determine the appropriate sign bit for arithmetic right shift.
      case (op)
	`OP_SHIFT_SHORT:
	  sign = a[63] & count[63];
	`OP_SHIFT_EXT:
	  sign = cc & count[63];
	`OP_SHIFT_LONG:
	  sign = b[63] & count[63];
	default:
	  sign = 1'bx;
      endcase // case (op)

      s31 = simd ? a[31] & count[63] : sign;

      case (op)
	`OP_SHIFT_SHORT:
	  if (out_of_range)
	    // Shift by c >= 64 or c <= -65
	    dw = {127{sign}};
	  else if (count[62])
	    // Right shift, -64 <= c < 0, 0 <= 64 + c < 64
	    dw = {{32{sign}}, {32{s31}}, a[63:1]};
	  else
	    // Left shift, 0 < c < 64.
	    dw = {a, 63'b0};
	`OP_SHIFT_EXT:
	  if (out_of_range)
	    dw = {127{sign}};
	  else if (count[62]) begin
	     // Right shift
	     if (count[6])
	       dw = {{63{sign}}, cc, a[63:1]};
	     else
	       dw = {{127{sign}}};
	  end
	  else begin
	     // Left shift
	     if (count[6])
	       dw = {cc, 126'b0};
	     else
	       dw = {a, cc, 62'b0};
	  end
	`OP_SHIFT_ROT:
	  dw = {a, a[63:1]};

	`OP_SHIFT_LONG:
	  if (out_of_range)
	    // Shift by c >= 128 or c <= -129
	    dw = {127{sign}};
	  else if (count[62]) begin
	     // Right shift
	     if (count[6])
	       // Shift by -64 <= c < 0; 0 <= 64 + c < 64
	       dw = {b, a[63:1]};
	     else
	       // Shift by -128 <= c < -64
	       dw = {{64{sign}}, b[63:1]};
	  end
	  else begin
	     // Left shift
	     if (count[6])
	       // Shift left, 64 <= c < 128.
	       dw = {b, 63'b0};
	     else
	       // Shift left, 0 < c < 64.
	       dw = {a, b[63:1]};
	  end
      endcase // case (op)

      dw_simd = simd ? {dw[126:95], dw[62:31], dw[94:63], dw[30:0]} : dw;
      dw_shifted = dw_simd[{1'b0, ~count[5] & ~simd, ~count[4:0]} +:96];
      res = simd ? {dw_shifted[95:64], dw_shifted[31:0]} : dw_shifted[63:0];
   end
endmodule
