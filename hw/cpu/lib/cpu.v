/* Copyright (C) 2015, 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "instr-defines.vh"

module cc_flag(input clk, input rst,
	       output cc_read,
	       input  cc_write_enable,
	       input  cc_write);
   reg 		      cc;

   // Passthrough when writing
   assign cc_read = cc_write_enable ? cc_write : cc;

   always @(posedge clk) begin
      if (rst) begin
	 cc <= 0;
      end
      // Synchronous writes.
      else if (cc_write_enable)
	cc <= cc_write;
   end
endmodule

module cpu (input clk, input rst,
	    // Wishbone master interface
	    output 	  mem_cyc,
	    output 	  mem_stb,
	    output 	  mem_we,
	    input 	  mem_ack,
	    input 	  mem_stall,
	    input 	  mem_err,
	    output [ADDRESS_WIDTH-1:3] mem_adr,
	    output [63:0] mem_dat_write,
	    input [63:0]  mem_dat_read,

	    output reg 	  cpu_halted,
	    output reg    cpu_error,
	    output [63:0] diagnostics);
   parameter INITIAL_STACK = 'h8000;
   parameter ADDRESS_WIDTH = 64;

   reg [59:0] prefix;
   reg prefix_active;

   reg [63:0] instr_counter;
   reg [63:0] cycle_counter;

   wire [63:0] load_data;

   /* Wishbone interface between load-store and mem-mux. */
   wire ldst_cyc;
   wire ldst_stb;
   wire ldst_we;
   wire ldst_ack;
   wire ldst_stall;
   wire [ADDRESS_WIDTH-1:3] ldst_adr;
   wire [63:0] ldst_write_data;
   wire [63:0] ldst_read_data;

   /* Wishbone interface between instruction fetch and mem-mux. */
   wire fetch_cyc;
   wire fetch_stb;
   wire fetch_ack;
   wire fetch_stall;
   wire [ADDRESS_WIDTH-1:3] fetch_adr;
   wire [63:0] fetch_dat;

   reg	fetch_enable;
   wire [15:0] fetch_instr;
   wire fetch_ready;

   /* Decoder signals */
   wire        decode_invalid;
   wire        decode_halt_enable;
   wire        decode_prefix_enable;

   wire [1:0]  decode_simd_mode;

   wire        decode_alu_enable;
   wire [2:0]  decode_alu_op;
   wire        decode_unary_enable;
   wire [1:0]  decode_unary_op;
   wire        decode_shift_enable;
   wire [1:0]  decode_shift_op;
   wire        decode_cnt_enable;
   wire [1:0]  decode_cnt_op;
   wire        decode_mul_enable;
   wire        decode_mul_op;
   wire        decode_recpr_enable;
   wire        decode_ldst_enable;

   wire [1:0]  decode_cout;
   wire [1:0]  decode_cin;
   wire [3:0]  decode_dst_idx;
   wire        decode_dst_write_enable;
   wire        decode_cc_write_enable;

   wire [3:0]  reg_a_idx;
   wire [63:0] reg_a_data;
   wire [3:0]  reg_b_idx;
   wire [63:0] reg_b_data;
   wire [3:0]  reg_c_idx;
   wire [63:0] reg_c_data;
   wire        decode_branch_enable;
   wire [1:0]  decode_branch_cond;
   wire        decode_link_enable;

   wire [1:0]  dst_mode;
   wire        src_neg;
   wire        ind_enable;
   wire [63:0] imm_data;
   wire        imm_enable;

   reg 	       decode_ready;
   reg [15:0]  instr;
   /* We could perhaps avoid this flip-flop if we moved the primary pc
      register here. */
   reg [ADDRESS_WIDTH-1:1]  pc_next;

   reg         early_branch;

   reg         dst_reg_write_enable;
   reg         dst_pc_write_enable;
   reg [3:0]   dst_write_idx;

   wire       cc_read;
   reg	      cc_write_enable;

   /* Operation inputs */
   reg [63:0] dst_data;
   reg [63:0] src_data;
   reg [63:0] ind_data;
   reg [ADDRESS_WIDTH-1:0] effective_address;

   /* Instruction result ready. */
   reg 	      result_ready;
   reg [63:0] result_data;
   /* For single-cycle execution units. */
   reg 	      exec_ready;

   wire [ADDRESS_WIDTH-1:1] fetch_pc_next;

   reg 	       alu_unit_pending;
   reg	       alu_unit_enable;

   wire [63:0] alu_res;
   wire        alu_cout;

   reg	       unary_unit_pending;
   reg	       unary_unit_enable;
   wire [63:0] unary_res;
   wire        unary_cout;

   reg 	       shift_unit_pending;
   reg	       shift_unit_enable;
   wire [63:0] shift_res;
   reg	       cnt_unit_pending;
   reg	       cnt_unit_enable;
   wire [6:0]  cnt_res;
   reg         mul_unit_enable;
   wire        mul_pending;
   wire        mul_ready;
   wire [63:0] mul_res;

   reg 	       recpr_unit_enable;
   wire        recpr_pending;
   wire        recpr_ready;
   wire [63:0] recpr_res;
   wire        div_by_zero;

   reg 	       ldst_unit_enable;
   wire        ldst_pending;
   wire        ldst_ready;

   reg [ADDRESS_WIDTH-1:1]  pc_return_latch;

   reg	       halt_next;
   reg	       error_next;
   reg	       prefix_active_next;
   reg	       link_enable;
   reg	       link_pending;
   reg	       cc_next;

   mem_mux #(ADDRESS_WIDTH) mux (clk, rst,
		mem_cyc, mem_stb, mem_we, mem_ack, mem_stall,
		mem_adr, mem_dat_write, mem_dat_read,

		ldst_cyc, ldst_stb, ldst_we,
		ldst_ack, ldst_stall,
		ldst_adr, ldst_write_data, ldst_read_data,

		fetch_cyc, fetch_stb, fetch_ack, fetch_stall,
		fetch_adr, fetch_dat);

   reg_file rf (clk,
		reg_a_idx, reg_a_data,
		reg_b_idx, reg_b_data,
		reg_c_idx, reg_c_data,
		dst_reg_write_enable & result_ready,
		dst_write_idx,
		result_data);

   cc_flag ccf (clk, rst, cc_read,
		cc_write_enable & result_ready, cc_next);

   instr_fetch #(ADDRESS_WIDTH) fetch(clk, rst,
		     fetch_pc_next,
		     early_branch | (dst_pc_write_enable & result_ready),
		     (early_branch ? effective_address[ADDRESS_WIDTH-1:1] : result_data[ADDRESS_WIDTH-1:1]),
		     fetch_enable,
		     fetch_instr,
		     fetch_ready,
		     fetch_cyc, fetch_stb, fetch_ack, fetch_stall,
		     fetch_adr, fetch_dat);

   instr_decode #(ADDRESS_WIDTH) decode(instr,
		       prefix_active, prefix,
		       // Main decoder outputs.
		       decode_invalid,
		       decode_halt_enable, decode_prefix_enable,
		       decode_branch_enable, decode_branch_cond,
                       decode_link_enable,
		       reg_a_idx, reg_b_idx, reg_c_idx,
		       decode_simd_mode,
		       decode_alu_enable, decode_alu_op,
		       decode_unary_enable, decode_unary_op,
		       decode_shift_enable, decode_shift_op,
		       decode_cnt_enable, decode_cnt_op,
		       decode_mul_enable, decode_mul_op,
		       decode_recpr_enable,
		       decode_ldst_enable,

		       decode_cout, decode_cin, decode_dst_idx,
		       imm_data, imm_enable,
		       decode_dst_write_enable, decode_cc_write_enable,
		       dst_mode, src_neg, ind_enable);

   // Execution units
   alu_unit alu_eu(clk, rst, alu_unit_enable,
		   decode_alu_op, decode_simd_mode,
		   dst_mode, dst_data, src_neg, src_data,
		   (decode_cin[1] & cc_read) ^ decode_cin[0] ,
		   decode_cout,
		   alu_res, alu_cout);

   unary_unit unary_eu(clk, rst, unary_unit_enable,
		       decode_unary_op, dst_data, cc_read, decode_cout,
		       unary_res, unary_cout);

   shift_unit shift_eu(clk, rst, shift_unit_enable,
		       // Only one bit of simd_mode is supported.
		       decode_shift_op, decode_simd_mode[0],
		       dst_data, ind_data, cc_read, src_data,
		       shift_res);

   cnt_unit cnt_eu(clk, rst, cnt_unit_enable,
		   decode_cnt_op, dst_data, cnt_res);

   mul_unit mul_eu(clk, rst, mul_unit_enable,
		   decode_mul_op, dst_data, src_data,
		   mul_pending, mul_ready, mul_res);

   reciprocal_unit recpr_eu(clk, rst, recpr_unit_enable, dst_data,
			    recpr_pending, recpr_ready, recpr_res,
			    div_by_zero);

   load_store_unit #(ADDRESS_WIDTH) load_store_eu(clk, rst, ldst_unit_enable,
				 effective_address,
				 ~decode_dst_write_enable,
				 dst_data, load_data,
				 ldst_pending, ldst_ready,
				 ldst_cyc, ldst_stb, ldst_we, ldst_adr,
				 ldst_write_data, ldst_read_data,
				 ldst_ack, ldst_stall);

   /* Pipeline is close the the classic RISC pipeline:

      Fetch stage:

        Activated by the fetch_enable signal. Attempts to fetch one
        instruction per cycle, and increment the pc. Sets fetch_ready
        when a new instruction is ready. Fetch is inhibited while a
        slow instruction or branch is pending, but nevertheless we
        attempt to fetch a new instruction the cycle after fetch of a
        branch instruction (here, "branch" means any instruction
        changing the pc).

      Decode stage:

        When a fetch instruction is ready (and decode isn't inhibited
        the cycle following decode of a branch), the instruction and
        pc value are saved in the instr and pc_next flip-flops and fed
        to the instruction decoder.

        The decoder decodes the instruction and reads operands from
        the register file. It also resolves simple branches, where the
        target is known to the decoder.

        We set the decode_ready flag early at the start of the decode
        cycle, and maps the decoder output to enable signals for the
        various execution units.

      Execute stage:

        Execution of instructions starts when the decode_ready flag is
        set. Each execution unit has its own input latches (to not
        waste simulation time or power on the logic in unused
        executiom units). Most instructions complete in one cycle.
        Longer instructions, e.g., load, sets a pending flag which
        stalls the processor until the operation is finshed. Sets the
        result_ready flag when finished.

      Write back stage:

        Results are written back to the register file, cc flag or pc.
        The register file has a pass-through on write, so that the
        decode stage can retrieve the result of the preceding
        instruction, without waiting for it to be stored in the
        register file and read back.
    */

   always @(posedge clk) begin
      if (rst) begin
	 instr_counter <= 0;
	 cycle_counter <= 0;
	 cpu_halted <= 0;
	 cpu_error <= 0;

	 alu_unit_pending <= 0;
	 unary_unit_pending <= 0;
	 shift_unit_pending <= 0;
	 cnt_unit_pending <= 0;
	 link_pending <= 0;

	 exec_ready <= 0;
	 dst_reg_write_enable <= 0;
	 dst_pc_write_enable <= 0;
	 cc_write_enable <= 0;

	 // mov r13, #INITIAL_STACK
	 prefix <= INITIAL_STACK >> 4;
	 prefix_active <= 1;
	 instr <= {8'b10110000, INITIAL_STACK[3:0], 4'b1101};
	 decode_ready <= 1;
      end
      else if (cpu_halted) begin
	 // Do nothing. Cancel any pending activity.
	 dst_reg_write_enable <= 0;
	 dst_pc_write_enable <= 0;
      end
      // Halt on any memory error (load, store or fetch). Should maybe
      // take stb into account?
      else if (mem_cyc & mem_err) begin
	 cpu_halted <= 1;
	 cpu_error <= 1;
      end
      else begin
	 cycle_counter <= cycle_counter + 1;

	 alu_unit_pending <= alu_unit_enable;
	 unary_unit_pending <= unary_unit_enable;
	 shift_unit_pending <= shift_unit_enable;
	 cnt_unit_pending <= cnt_unit_enable;
	 link_pending <= link_enable;

	 if (recpr_pending & div_by_zero) begin
	    cpu_halted <= 1;
	    cpu_error <= 1;
	 end

	 /* Drive the decoder.

	    Despite the below logic to inhibit fetch_enable, we always
	    attempt to fetch the instruction after a branch. If we're
	    decoding a branch in this cycle, ignore the fetched
	    instruction. */
	 if (fetch_ready
	     & ~(decode_ready
		 & (early_branch | (decode_dst_write_enable & &decode_dst_idx)))) begin
            decode_ready <= 1;
            instr <= fetch_instr;
            pc_next <= fetch_pc_next;
	 end
	 else
           decode_ready <= 0;

	 // Latch the decoded instruction.
	 if (decode_ready) begin
	    instr_counter <= instr_counter + 1;

	    cc_write_enable <= decode_cc_write_enable;
	    dst_write_idx <= decode_dst_idx;
	    dst_reg_write_enable <= decode_dst_write_enable & ~&decode_dst_idx;
	    dst_pc_write_enable <= decode_dst_write_enable & &decode_dst_idx;

	    if (halt_next) begin
	       cpu_halted <= 1;
	       cpu_error <= error_next;
	    end
	    if (prefix_active_next) begin
	       if (prefix_active)
		 prefix <= {prefix[47:0], src_data[11:0]};
	       else
		 prefix <= src_data[59:0];
	    end
	    if (link_enable)
	      pc_return_latch <= pc_next;

	    prefix_active <= prefix_active_next;
	 end
	 else if (dst_pc_write_enable & result_ready)
	   // Clear, to reenable fetch after write to pc.
	   dst_pc_write_enable <= 0;

	 // Keep track of when single-cycle execution units are ready.
	 exec_ready <= decode_ready;
      end
   end // always @ (posedge clk)

   // Inhibit fetching of new instructions while load or store is pending.
   always @(*) begin
      fetch_enable = ~(dst_pc_write_enable | cpu_halted
		       | recpr_pending | recpr_unit_enable
		       | mul_pending | mul_unit_enable
		       | ldst_pending | ldst_unit_enable);
   end

   // Branch target and condition logic
   always @(*) begin
      early_branch = 0;
      if (decode_ready && decode_branch_enable) begin
	 // Early branch, at end of decode stage.
	 casez (decode_branch_cond)
	   `BRANCH_CND_CC_FALSE:
	     early_branch = ~cc_read;
	   `BRANCH_CND_CC_TRUE:
	     early_branch = cc_read;
	   `BRANCH_CND_NZ:
	     early_branch = |reg_b_data;
	   `BRANCH_CND_ALWAYS:
	     early_branch = 1;
	 endcase
      end
   end

   // Connect execution units. Their inputs are latched on positive
   // clock edge.
   always @(*) begin
      alu_unit_enable = decode_alu_enable & decode_ready;
      unary_unit_enable = decode_unary_enable & decode_ready;
      shift_unit_enable = decode_shift_enable & decode_ready;
      cnt_unit_enable = decode_cnt_enable & decode_ready;
      mul_unit_enable = decode_mul_enable & decode_ready;
      recpr_unit_enable = decode_recpr_enable & decode_ready;
      /* Load or store is implied by decode_dst_write_enable (0 for
         store, 1 for load). */
      ldst_unit_enable = decode_ldst_enable & decode_ready;

      link_enable = decode_link_enable & decode_ready;

      // "Internal" execution unit.
      halt_next = (decode_halt_enable | decode_invalid) & decode_ready;
      error_next = decode_invalid & decode_ready; // Valid only with halt_next.
      prefix_active_next = decode_prefix_enable & decode_ready;
   end

   // Connect execution unit output, based on latched pending signals.
   always @(*) begin
      // Valid for single-cycle instructions.
      result_ready = exec_ready;
      cc_next = 1'bx;
      result_data = {64{1'bx}};

      if (alu_unit_pending) begin
	   result_data = alu_res;
	   cc_next = alu_cout;
      end
      if (unary_unit_pending) begin
	 result_data = unary_res;
	 cc_next = unary_cout;
      end
      if (shift_unit_pending) begin
	 result_data = shift_res;
      end
      if (cnt_unit_pending) begin
	 result_data = {57'b0, cnt_res};
      end
      if (mul_pending) begin
	 result_data = mul_res;
	 result_ready = mul_ready;
      end
      if (recpr_pending) begin
	 result_data = recpr_res;
	 result_ready = recpr_ready;
      end
      if (ldst_pending) begin
	 result_data = load_data;
	 result_ready = ldst_ready;
      end
      if (link_pending) begin
	 result_data = {{(64-ADDRESS_WIDTH){1'b0}}, pc_return_latch, 1'b0};
      end
   end // always @ begin

   // Value of operand inputs.
   always @(*) begin
      if (&reg_a_idx)
	dst_data = {{64-ADDRESS_WIDTH{1'b0}}, pc_next, 1'b0};
      else
	dst_data = reg_a_data;

      if (imm_enable)
	src_data = imm_data;
      else if (&reg_b_idx)
	src_data = {{64-ADDRESS_WIDTH{1'b0}}, pc_next, 1'b0};
      else
	src_data = reg_b_data;

      if (&reg_c_idx | decode_branch_enable)
	ind_data = {{64-ADDRESS_WIDTH{1'b0}}, pc_next, 1'b0};
      else
	ind_data = reg_c_data;

      effective_address = (ind_data[ADDRESS_WIDTH-1:0] & {ADDRESS_WIDTH{ind_enable}})
	+ (src_data[ADDRESS_WIDTH-1:0] ^ {ADDRESS_WIDTH{src_neg}})
	  + {{(ADDRESS_WIDTH-1){1'b0}}, src_neg ^ decode_branch_enable};
   end

   assign diagnostics
     = { fetch_ready, decode_ready, result_ready, mem_cyc, mem_stb, mem_we, mem_ack, mem_stall, mem_err,
	 cpu_halted, pc_next[6:1], fetch_instr, 32'b0 };

endmodule // cpu
