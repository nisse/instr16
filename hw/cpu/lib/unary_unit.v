/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "instr-defines.vh"

module unary_unit (input clk, input rst, input enable,
		   input [1:0]	     op_in,
		   input [63:0]      a_in,
		   input	     c_in,
		   input [1:0]	     ccc_in,
		   output reg [63:0] res,
		   output reg	     cc);
   reg [63:0]  a;
   reg	       c;
   reg [1:0]   op;
   reg [1:0]   ccc;

   always @(posedge clk) begin
      if (rst) begin
	 a <= 0;
	 c <= 0;
	 op <= 0;
	 ccc <= 0;
      end
      else if (enable) begin
	 a <= a_in;
	 c <= c_in;
	 op <= op_in;
	 ccc <= ccc_in;
      end
   end

   always @(*) begin
      cc = c;

      case (op)
	`OP_UNARY_XSHIFT: begin
	   res[62:0] = a[63:1];
	   if (ccc[1])
	     res[63] = a[63] & ccc[0];
	   else
	     res[63] = c;

	   if (ccc != 2'b00)
	     cc = a[0];
	end
	`OP_UNARY_BSWAP:
	  res = {a[ 7: 0],a[15: 8],a[23:16],a[31:24],
		 a[39:32],a[47:40],a[55:48],a[63:56]};
	`OP_UNARY_FNEG:
	  res = {~a[63], a[62:0]};
	`OP_UNARY_FABS:
	  res = {1'b0, a[62:0]};
      endcase
   end
endmodule
