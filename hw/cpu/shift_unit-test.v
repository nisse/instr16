/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "lib/instr-defines.vh"

module main;
   reg [63:0] a;
   reg [63:0] b;
   wire [63:0] res_lefts;
   wire [63:0] res_lrights;
   wire [63:0] res_arights;
   reg [63:0] exp_lefts;
   reg [63:0] exp_lrights;
   reg [63:0] exp_arights;

   wire [63:0] res_leftx;
   wire [63:0] res_lrightx;
   wire [63:0] res_arightx;
   reg [63:0] exp_leftx;
   reg [63:0] exp_lrightx;
   reg [63:0] exp_arightx;

   wire [63:0] res_leftl;
   wire [63:0] res_lrightl;
   wire [63:0] res_arightl;
   reg [63:0] exp_leftl;
   reg [63:0] exp_lrightl;
   reg [63:0] exp_arightl;
   wire [63:0] res_rot;
   reg [63:0] exp_rot;

   reg	      clk, rst, enable;

   integer    count, j, seed;

   initial begin
      clk = 0;
      rst = 1;
      enable = 0;

      #10 clk = 1;
      #10 clk = 0;
      rst = 0;
      enable = 1;
      for (count = 0; count <= 130; count = count + 1)
	for (j = 0; j < 100; j = j + 1) begin
	   a[63:48] = $random(seed) % (1 << 16);
	   a[47:32] = $random(seed) % (1 << 16);
	   a[31:16] = $random(seed) % (1 << 16);
	   a[15:0]  = $random(seed) % (1 << 16);
	   b[63:48] = $random(seed) % (1 << 16);
	   b[47:32] = $random(seed) % (1 << 16);
	   b[31:16] = $random(seed) % (1 << 16);
	   b[15:0]  = $random(seed) % (1 << 16);
	   if (count == 0) begin
	      exp_lefts = a;
	      exp_leftx = a;
	      exp_leftl = a;
	      // Actually aright with a huge value
	      exp_lrights = {64{a[63]}};
	      exp_lrightx = {64{b[0]}};
	      exp_lrightl = {64{b[63]}};
	      exp_arights = a;
	      exp_arightx = a;
	      exp_arightl = a;
	   end
	   else begin
	      if (count < 64) begin
		 exp_lefts = a << count;
		 exp_arights = $signed(a) >>> count;
		 exp_lrights = a >> count;
	      end
	      else begin
		 exp_lefts = 0;
		 exp_arights = {64{a[63]}};
		 exp_lrights = 0;
	      end

	      if (count <= 64) begin
		 exp_leftx = {a[62:0], b[0]} << (count - 1);
		 exp_arightx = $signed({b[0], a[63:1]}) >>> (count - 1);
		 exp_lrightx = {b[0], a[63:1]} >> (count - 1);
	      end
	      else begin
		 exp_leftx = 0;
		 exp_arightx = {64{b[0]}};
		 exp_lrightx = 0;
	      end

	      if (count < 128) begin
		 exp_leftl = ({a, b} << count) >> 64;
		 exp_arightl = $signed({b, a}) >>> count;
		 exp_lrightl = {b, a} >> count;
	      end
	      else begin
		 exp_leftl = 0;
		 exp_arightl = {64{b[63]}};
		 exp_lrightl = 0;
	      end
	   end
	   exp_rot = (a << count[5:0]) | (a >> -count[5:0]);

	   #10 clk = 1;
	   #10 clk = 0;
	   if (exp_lefts !== res_lefts) begin
	      $display ("failed test left %d, count = %d, a = %x",
			j, count, a);
	      $display ("  result: %x", res_lefts);
	      $display ("  expect: %x", exp_lefts);
	      $finish_and_return(1);
	   end
	   if (exp_lrights !== res_lrights) begin
	      $display ("failed test lright %d, count = %d, a = %x",
			j, count, a);
	      $display ("  result: %x", res_lrights);
	      $display ("  expect: %x", exp_lrights);
	      $finish_and_return(1);
	   end
	   if (exp_arights !== res_arights) begin
	      $display ("failed test aright %d, count = %d, a = %x",
			j, count, a);
	      $display ("  result: %x", res_arights);
	      $display ("  expect: %x", exp_arights);
	      $finish_and_return(1);
	   end

	   if (exp_leftx !== res_leftx) begin
	      $display ("failed test leftx %d, count = %d, a = %x, c = %b",
			j, count, a, b[0]);
	      $display ("  result: %x", res_leftx);
	      $display ("  expect: %x", exp_leftx);
	      $finish_and_return(1);
	   end
	   if (exp_lrightx !== res_lrightx) begin
	      $display ("failed test lrightx %d, count = %d, a = %x, c = %b",
			j, count, a, b[0]);
	      $display ("  result: %x", res_lrightx);
	      $display ("  expect: %x", exp_lrightx);
	      $finish_and_return(1);
	   end
	   if (exp_arightx !== res_arightx) begin
	      $display ("failed test arightx %d, count = %d, a = %x, c = %b",
			j, count, a, b[0]);
	      $display ("  result: %x", res_arightx);
	      $display ("  expect: %x", exp_arightx);
	      $finish_and_return(1);
	   end

	   if (exp_leftl !== res_leftl) begin
	      $display ("failed test left %d, count = %d, a = %x\n b = %x",
			j, count, a, b);
	      $display ("  result: %x", res_leftl);
	      $display ("  expect: %x", exp_leftl);
	      $finish_and_return(1);
	   end
	   if (exp_lrightl !== res_lrightl) begin
	      $display ("failed test lright %d, count = %d, a = %x\n b = %x",
			j, count, a, b);
	      $display ("  result: %x", res_lrightl);
	      $display ("  expect: %x", exp_lrightl);
	      $finish_and_return(1);
	   end
	   if (exp_arightl !== res_arightl) begin
	      $display ("failed test aright %d, count = %d, a = %x\n b = %x",
			j, count, a, b);
	      $display ("  result: %x", res_arightl);
	      $display ("  expect: %x", exp_arightl);
	      $finish_and_return(1);
	   end
	   if (exp_rot !== res_rot) begin
	      $display ("failed test rot %d, count = %d, a = %x",
			j, count, a, b);
	      $display ("  result: %x", res_rot);
	      $display ("  expect: %x", exp_rot);
	      $finish_and_return(1);
	   end
	end
   end // initial begin
   initial begin
      if (0)
	$monitor ("At time %7t:\n  left: a %x, b %x, count %x, res %x\n  lright: a %x, b %x, count %x, res %x\n  aright: a %x, b %x, count %x, res %x",
		  $time,
		  left.a, left.b, left.count, left.res,
		  lright.a, lright.b, lright.count, lright.res,
		  aright.a, aright.b, aright.count, aright.res);
   end

   shift_unit lefts(clk, rst, enable, `OP_SHIFT_SHORT, 2'b00,
		    a, b, 1'bx, 64'b0 + count, res_lefts);
   shift_unit arights(clk, rst, enable, `OP_SHIFT_SHORT, 2'b00,
		      a, b, 1'bx, {64'b0} - count, res_arights);
   shift_unit lrights(clk, rst, enable, `OP_SHIFT_SHORT, 2'b00,
		      a, b, 1'bx, {1'b1,63'b0} - count, res_lrights);

   shift_unit leftx(clk, rst, enable, `OP_SHIFT_EXT, 2'b00,
		    a, {64{1'bx}}, b[0], 64'b0 + count, res_leftx);
   shift_unit arightx(clk, rst, enable, `OP_SHIFT_EXT, 2'b00,
		      a, {64{1'bx}}, b[0], {64'b0} - count, res_arightx);
   shift_unit lrightx(clk, rst, enable, `OP_SHIFT_EXT, 2'b00,
		      a, {64{1'bx}}, b[0], {1'b1,63'b0} - count, res_lrightx);

   shift_unit rot(clk, rst, enable, `OP_SHIFT_ROT, 2'b00,
		  a, b, 1'bx, 64'b0 + count, res_rot);

   shift_unit leftl(clk, rst, enable, `OP_SHIFT_LONG, 2'b00,
		    a, b, 1'bx, 64'b0 + count, res_leftl);
   shift_unit arightl(clk, rst, enable, `OP_SHIFT_LONG, 2'b00,
		      a, b, 1'bx, {64'b0} - count, res_arightl);
   shift_unit lrightl(clk, rst, enable, `OP_SHIFT_LONG, 2'b00,
		      a, b, 1'bx, {1'b1,63'b0} - count, res_lrightl);
endmodule
