/* Copyright (C) 2023  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "lib/instr-defines.vh"
module main;
   reg [63:0] a;

   wire [63:0] res_rot32;
   wire [63:0] exp_rot32;

   wire [63:0] res_left32;
   wire [63:0] exp_left32;

   wire [63:0] res_lright32;
   wire [63:0] exp_lright32;

   wire [63:0] res_aright32;
   wire [63:0] exp_aright32;

   reg	      clk, rst, enable;

   integer    count, j, seed;

   initial begin
      clk = 0;
      rst = 1;
      enable = 0;
      #10 clk = 1;
      #10 clk = 0;
      rst = 0;
      enable = 1;
      for (count = 0; count < 64; count = count + 1)
	for (j = 0; j < 100; j = j + 1) begin
	   a[63:48] = $random(seed) % (1 << 16);
	   a[47:32] = $random(seed) % (1 << 16);
	   a[31:16] = $random(seed) % (1 << 16);
	   a[15:0]  = $random(seed) % (1 << 16);

	   #10 clk = 1;
	   #10 clk = 0;
	   if (exp_rot32 !== res_rot32) begin
	      $display ("failed test rot32 %d, count = %d, a = %x",
			j, count, a);
	      $display ("  result: %x", res_rot32);
	      $display ("  expect: %x", exp_rot32);
	      $finish_and_return(1);
	   end

	   if (exp_left32 !== res_left32) begin
	      $display ("failed test left32 %d, count = %d, a = %x",
			j, count, a);
	      $display ("  result: %x", res_left32);
	      $display ("  expect: %x", exp_left32);
	      $finish_and_return(1);
	   end

	   if (count[4:0] != 0 && exp_lright32 !== res_lright32) begin
	      $display ("failed test lright32 %d, count = %d, a = %x",
			j, count, a);
	      $display ("  result: %x", res_lright32);
	      $display ("  expect: %x", exp_lright32);
	      $finish_and_return(1);
	   end

	   if (count[4:0] != 0 && exp_aright32 !== res_aright32) begin
	      $display ("failed test aright32 %d, count = %d, a = %x",
			j, count, a);
	      $display ("  result: %x", res_aright32);
	      $display ("  expect: %x", exp_aright32);
	      $finish_and_return(1);
	   end
	end
   end
   shift_unit rot32(clk, rst, enable, `OP_SHIFT_ROT, 2'b01,
		    a, {64{1'bx}}, 1'bx, 64'b0 + count[5:0], res_rot32);

   shift_unit left32(clk, rst, enable, `OP_SHIFT_SHORT, 2'b01,
		    a, {64{1'bx}}, 1'bx, 64'b0 + count[5:0], res_left32);

   shift_unit lright32(clk, rst, enable, `OP_SHIFT_SHORT, 2'b01,
		    a, {64{1'bx}}, 1'bx, {1'b1,63'b0} - count, res_lright32);

   shift_unit aright32(clk, rst, enable, `OP_SHIFT_SHORT, 2'b01,
		    a, {64{1'bx}}, 1'bx, {64'b0} - count, res_aright32);

   simd_ref_unit #(5) r_rot32(1'b1, {2'b00, count[4:0]}, a, exp_rot32);

   simd_ref_unit #(5) r_left32(1'b0, {2'b00, count[4:0]}, a, exp_left32);

   simd_ref_unit #(5) r_lright32(1'b0, 7'b1000000 - count[4:0], a, exp_lright32);

   simd_ref_unit #(5) r_aright32(1'b0, 7'b0000000 - count[4:0], a, exp_aright32);
endmodule

module simd_ref_unit(input enable_rot, input [SIZE+1:0] count, input [63:0] x, output [63:0] res);
   parameter SIZE = 0;
   localparam WIDTH = 1 << SIZE;
   genvar     i;
   generate
      for (i = 0; i < 64; i = i + WIDTH)
	begin : LABEL
	   reg [2*WIDTH-1:0] dw;
	   reg [WIDTH-1:0]   f;

	   assign f = x[i+WIDTH-1:i];
	   assign dw = enable_rot ? {f, f[WIDTH-1:1]}
		       : (count[SIZE] ? {{WIDTH{f[WIDTH-1] & count[SIZE+1]}}, f[WIDTH-1:1]}
			  : {f, {WIDTH-1{1'b0}}});
	   assign res[i+WIDTH-1:i] = dw[{1'b0, ~count[SIZE-1:0]} +: WIDTH];
	end
   endgenerate
endmodule
