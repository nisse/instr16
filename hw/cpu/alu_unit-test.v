/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "lib/instr-defines.vh"

module main;
   reg [63:0] a;
   reg [63:0] b;
   reg	      cy_in;
   reg [2:0]  op_in;
   reg [1:0]  ccc;
   reg [63:0] exp_res;
   reg	      exp_cc;
   wire [63:0] res;
   wire        cc;
   reg	      clk, rst, enable;

   integer     count, op, seed;

   initial begin
      clk = 0;
      rst = 1;
      enable = 0;

      #10 clk = 1;
      #10 clk = 0;
      rst = 0;
      enable = 1;
      for (count = 0; count <= 130; count = count + 1)
	for (op = 0; op < 7; op = op + 1) begin
	   a[63:48] = $random(seed) % (1 << 16);
	   a[47:32] = $random(seed) % (1 << 16);
	   a[31:16] = $random(seed) % (1 << 16);
	   a[15:0]  = $random(seed) % (1 << 16);
	   b[63:48] = $random(seed) % (1 << 16);
	   b[47:32] = $random(seed) % (1 << 16);
	   b[31:16] = $random(seed) % (1 << 16);
	   b[15:0]  = $random(seed) % (1 << 16);
	   ccc = $random(seed) % 4;
	   cy_in = $random(seed) % 2;
	   op_in = op;

	   case (op)
	     `OP_ALU_AND: begin
		exp_res = a & b;
		exp_cc = |exp_res;
	     end
	     `OP_ALU_OR: begin
		exp_res = a | b;
		exp_cc = 0;
	     end
	     `OP_ALU_XOR: begin
		exp_res = a ^ b;
		exp_cc = 0;
	     end
	     `OP_ALU_ADD: begin
		{exp_cc, exp_res} = {1'b0, a} + b + cy_in;
		case (ccc)
		  'b00:
		    exp_cc = ~|exp_res;
		  'b10:
		    exp_cc = exp_cc ^ a[63] ^ b[63] ^ 1;
		  'b11:
		    exp_cc = (a[63] == b[63]) & (a[63] != exp_res[63]);
		endcase // case (ccc)
	     end
	     `OP_ALU_INJ8: begin
		exp_res = {b[7:0], a[55:0]};
		exp_cc = 0;
	     end
	     `OP_ALU_INJ16: begin
		exp_res = {b[15:0], a[47:0]};
		exp_cc = 0;
	     end
	     `OP_ALU_INJ32: begin
		exp_res = {b[31:0], a[31:0]};
		exp_cc = 0;
	     end
	   endcase // case (op)
	   #10 clk = 1;
	   #10 clk = 0;
	   if (exp_res !== res | exp_cc !== cc) begin
	      $display("failed test %d, op %d, a = %x, b = %x, c = %d, ccc = %b", count, op, a, b, cy_in, ccc);
	      $display ("  result: %x, cy = %d", res, cc);
	      $display ("  expect: %x, cy = %d", exp_res, exp_cc);
	      $finish_and_return(1);
	   end
	end // for (op = 0; op < 8; op = op + 1)
   end

   alu_unit alu (clk, rst, enable, op_in, /* simd */ 2'b00,
		 /*mode*/ 2'b01, a, /*neg*/ 1'b0, b,
		 cy_in, ccc, res, cc);

endmodule
