/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`define T0 'h123
`define T1 'hfedcba9876543210
`define T2 'h0011223344556677

module main;
   reg clk;

   reg [3:0]   reg_a_idx;
   wire [63:0] reg_a_data;
   reg [3:0]   reg_b_idx;
   wire [63:0] reg_b_data;
   reg [3:0]   reg_c_idx;
   wire [63:0] reg_c_data;
   reg         reg_write_enable;
   reg [3:0]   reg_write_idx;
   reg [63:0]  reg_write_data;

   integer     i;

   reg_file rf (clk,
		reg_a_idx, reg_a_data,
		reg_b_idx, reg_b_data,
		reg_c_idx, reg_c_data,
		reg_write_enable, reg_write_idx, reg_write_data);

   initial begin
      clk = 0;

      reg_write_enable = 1;
      reg_write_data = `T0;
      reg_write_idx = 13;

      #10 clk = 1;
      #10 clk = 0;
      reg_a_idx = 5;
      reg_b_idx = 13;
      reg_c_idx = 14;
      reg_write_idx = 14;
      reg_write_enable = 0;
      #10 clk = 1;
      #10 clk = 0;

      if (reg_a_data !== 0) begin
	 $display("failed test 1, expected %x, got %x",
		  0, reg_a_data);
	 $finish_and_return(1);
      end
      if (reg_b_data !== `T0) begin
	 $display("failed test 2, expected %x, got %x",
		  `T0, reg_b_data);
	 $finish_and_return(1);
      end

      if (reg_c_data !== 0) begin
	 $display("failed test 3, expected %x, got %x",
		  0, reg_c_data);
	 $finish_and_return(1);
      end

      reg_write_idx = 1;
      reg_write_enable = 1;
      reg_write_data = `T1;

      reg_a_idx = 1;

      #10 clk = 1;
      #10 clk = 0;

      if (reg_a_data !== `T1) begin
	 $display("failed test 4, expected %x, got %x",
		  `T1, reg_a_data);
	 $finish_and_return(1);
      end
      if (reg_b_data !== `T0) begin
	 $display("failed test 5, expected %x, got %x",
		  `T0, reg_b_data);
	 $finish_and_return(1);
      end

      reg_write_idx = 2;
      reg_b_idx = 2;
      reg_write_data = `T2;

      #10 clk = 1;
      #10 clk = 0;

      if (reg_a_data !== `T1) begin
	 $display("failed test 6, expected %x, got %x",
		  `T1, reg_a_data);
	 $finish_and_return(1);
      end
      if (reg_b_data !== `T2) begin
	 $display("failed test 7, expected %x, got %x",
		  `T2, reg_b_data);
	 $finish_and_return(1);
      end
   end
endmodule
