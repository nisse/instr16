/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Wishbone-style memory
module ram_unit(input clk, input rst,
		input cyc, input stb,
		input we, output reg ack,
		input [SIZE-1:0]  adr,
		input [63:0] 	  dat_write,
		output reg [63:0] dat_read);
   parameter  SIZE = 10; // Address size
   reg [63:0] mem [0:(1 << SIZE)-1];

`ifdef RAM_HEX_FILE
   initial
     $readmemh(`RAM_HEX_FILE, mem);
`endif

   always @(posedge clk) begin
      if (rst) begin
	 dat_read <= 0;
	 ack <= 0;
      end
      else begin
	 if (cyc & stb) begin
	    if (we)
	      mem[adr] <= dat_write;
	    else
	      dat_read <= mem[adr];
	 end
	 ack <= cyc & stb;
      end
   end
endmodule
