/* Copyright (C) 2022  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Record state of up to 64 signals starting at reset, and send over
   serial port after log memory is filled. */
module logic_analyzer (input clk, input rst, input [63:0] signals, output txd);
   parameter CLK_FREQUENCY = 12000000;
   parameter LOG_BITS = 8;

   reg [63:0] buffer[0:(1<<LOG_BITS) - 1];
   reg [LOG_BITS+2:0] write_idx;
   reg [LOG_BITS-1:0] read_idx;
   reg [7:0] 	      tx_data;
   reg 		      tx_avail;
   wire 	      tx_ready;

   function [7:0] top_8(input [63:0] word);
      top_8 = word[63:56];
   endfunction

   always @(posedge clk) begin
      if (rst) begin
	 read_idx <= 0;
	 write_idx <= 0;
	 tx_avail <= 0;
      end
      else if (!(&read_idx)) begin
	 buffer[read_idx] <= signals;
	 read_idx <= read_idx + 1;
      end
      else if (!(&write_idx)) begin
	 tx_avail <= 1;
	 tx_data <= top_8(buffer[write_idx[LOG_BITS+2:3]] << {write_idx[2:0], 3'b0});
	 if (tx_avail & tx_ready)
	   write_idx <= write_idx + 1;
      end
      else
	tx_avail <= 0;
   end

   avr109tx #(.CLK_FREQUENCY(CLK_FREQUENCY))
   tty_tx(rst, clk, tx_data, tx_avail, txd, tx_ready);
endmodule // logic_analyzer
