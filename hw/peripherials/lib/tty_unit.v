/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module tty_unit (input clk, input rst,
		 input 	       cyc, input stb,
		 input 	       we, output ack,
		 output        stall, output err,
		 input 	       adr,
		 input [63:0]  dat_write,
		 output [63:0] dat_read,

		  // Serial line.
		 input 	       rxd,
		 output        txd);
   parameter CLK_FREQUENCY = 12000000;

   wire rx_ready;
   wire [7:0] rx_data;
   wire tx_ready;
   
   wire req_tty_read;
   wire req_tty_write;
   wire req_bogus_read;
   wire req_bogus_write;

   avr109tx #(.CLK_FREQUENCY(CLK_FREQUENCY))
   tty_tx(rst, clk, dat_write[7:0], req_tty_write, txd, tx_ready);

   avr109rx #(.CLK_FREQUENCY(CLK_FREQUENCY))
   tty_rx(rst, clk, rx_data, rx_ready, rxd, req_tty_read);

   assign req_tty_read = cyc & stb & ~adr & ~we;
   assign req_tty_write = cyc & stb & adr & we;
   assign req_bogus_write = cyc & stb & ~adr & we;
   assign req_bogus_read = cyc & stb & adr & ~we;

   assign stall = (req_tty_read & ~rx_ready)
                  | (req_tty_write & ~tx_ready);
   
   assign dat_read = {48'b0, rx_data};

   assign err = req_bogus_read | req_bogus_write;
   assign ack = (req_tty_read & rx_ready) | (req_tty_write & tx_ready);
endmodule
