/* Copyright (C) 2015, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [63:0] x;
   reg [63:0] y;
   wire [6:0] ctz;
   wire [6:0] clz;
   reg [6:0]  ref_ctz;
   reg [6:0]  ref_clz;

   integer    i, j;
   initial
     begin
	for (i = 0; i < 31; i=i+1)
	  for (j = 0; j < 63; j++)
	    begin
	       #((i | j) ? 10 : 0) begin
		  x = i << j;
		  if (i == 0) begin
		    ref_ctz = 64;
		    ref_clz = 64;
		  end
		  else begin
		     ref_ctz = (((i & 1) == 0)
				+ ((i & 3) == 0)
				+ ((i & 7) == 0)
				+ ((i & 15) == 0)
				+ ((i & 31) == 0)) + j;
		     /* Valid only for j < 60 */
		     ref_clz = (((i & 16) == 0)
				+ ((i & 24) == 0)
				+ ((i & 28) == 0)
				+ ((i & 30) == 0)
				+ ((i & 31) == 0)) + 59 - j;
		  end
	       end
	       #10 begin
		  if ((ctz === 64 && ref_ctz >= 64)
		      || ctz === ref_ctz)
		    ;
		  else begin
		     $display ("ctz fail: in: %x, out: %b, ref: %b",
			       x, ctz, ref_ctz);
		     $finish_and_return(1);
		  end
		  if (j >= 60 || clz == ref_clz)
		    ;
		  else begin
		     $display ("clz fail: in: %x, out: %b, ref: %b",
			       x, clz, ref_clz);
		     $finish_and_return(1);
		  end
	       end
	    end // for (j = 0; j < 63; j++)
     end // initial begin
   ctz #(6) test_ctz (x, ctz);
   clz #(6) test_clz (x, clz);
endmodule
