/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   parameter n = 64;

   reg [n-2:0] d;
   wire [n-1:0] q;
   wire [2*n:0]	r;

   reg	      clk;
   reg	      rst;
   reg	      enable;
   reg	      start;
   wire       busy;
   wire       ready;

   wire [n:0] pk;
   wire [n-1:0] qk;
   wire [2*n:0] rk;

   integer    t, seed;
   initial begin
      seed = 0;
      t = 0;

      clk = 0;
      rst = 1;
      enable = 0;
      start = 0;
   end
   always
     #10 begin
	clk <= !clk;
     end // always #10
   always @(posedge clk) begin
      if (rst) begin
	 rst <= 0;
	 start <= 1;
      end
      else
	start <= 0;
      if (ready && (|r[2*n:n] || r[n-1:0] >= {1'b1,d})) begin
	 $display("fail: in: %x", {1'b1, d});
	 $display("     out: %x", q);
	 $display("       r: %x", r);
	 $display("  %x * %x = %x",
		  {66'b1,d}, {65'b1,q}, {66'b1,d} * {65'b1,q});

	 $finish_and_return(1);
      end
      if (start || ready) begin
	 if (t == 0)
 	   d <= 0;
 	 else if (t == 1)
 	   d <= 'h7fffffffffffffff;
 	 else if (t == 2)
 	   d <= 'h4000000000000000;
 	 else if (t < 500) begin
 	    d[31: 0] <= $random(seed);
 	    d[62:32] <= $random(seed);
	 end
	 else
	   $finish;
	 t <= t + 1;

	 enable <= 1;
      end
      else
	enable <= 0;
   end // always @ (posedge clk)

   assign r = {1'b0,{128{1'b1}}} - {66'b1,d} * {65'b1,q};
   srt_reciprocal recpr (clk, rst, enable, d, busy, ready, q);

   /* Examine intermediate values */
   always @(negedge clk)
     if (!rst && recpr.busy) begin
	if (recpr.s_reg[64:61] == 'b0111) begin
	   $display("Invalid s, bits_left: %d", recpr.bits_left);
	   $display( "    d: %b", {1'b1, d});
	end
	if ((({{n{pk[n]}}, pk} + 1) << recpr.bits_left) - 1 !== rk) begin
	   $display("bits_left: %d, d: %x",
		    recpr.bits_left, {1'b1, recpr.d});
	   $display("   s_reg: %b", recpr.s_reg);
	   $display("   c_reg:     %b", recpr.c_reg);
	   $display("   q_reg: %b", recpr.q_reg[2*n-1:n]);
	   $display("          %b", recpr.q_reg[n-1:0]);
	   $display("      pk: %b", pk);
	   $display("      rk: %b", rk[2*n:n]);
	   $display("          %b", rk[n-1:0]);
	   $display("      qk: %b", qk);
	   $finish_and_return(1);
	end
     end

   assign rk = {1'b0,{128{1'b1}}} - {66'b1,d} * {65'b1,qk};
   assign pk = recpr.s_reg + {recpr.c_reg, 1'b0};
   srt_final_q gen_qk (recpr.q_reg << (2*recpr.bits_left), 1'b0, qk);
endmodule
