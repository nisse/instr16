module add_plain(input clk, input rst,
		 input [63:0] 	   a_in,
		 input [63:0] 	   b_in,
		 input 		   c_in,
		 output reg [63:0] res,
		 output reg 	   cy);
   reg [63:0] a;
   reg [63:0] b;
   reg 	      c;

   always @(posedge clk) begin
      if (rst) begin
	 a <= 0;
	 b <= 0;
	 c <= 0;
      end
      else begin
	 a <= a_in;
	 b <= b_in;
	 c <= c_in;
      end
   end

   always @(*) begin
      {cy, res } = {1'b0, a} + {1'b0, b} + {64'b0, c};
   end
endmodule
