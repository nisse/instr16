/* Copyright (C) 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

`include "umul4.v"

module main;
   reg [3:0] a;
   reg [3:0] b;
   wire [7:0] p;
   integer    i, j;

   initial
     begin
	for (i = 0; i < 16; i = i + 1)
	  for (j = 0; j < 16; j = j + 1) begin
	     #( (i | j) ? 10 : 0) begin
		b = i;
		a = j;
	     end
	     #10 begin
		if (p !== i * j) begin
		   $display ("fail: in: %d, %d, out: %d",
			     i, j, p);
		   $finish_and_return(1);
		end
	     end
	  end
     end

  umul4 foo (a, b, p);
endmodule
