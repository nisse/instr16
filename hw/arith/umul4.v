/* Copyright (C) 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Unsigned multiply, 4x4 -> 8 bits */
module umul4(input [3:0] a, input [3:0] b, output [7:0] p);

   reg [0:3] ra;
   
   reg [3:0] a0;
   reg [4:1] a1;
   reg [5:2] a2;
   reg [6:3] a3;

   wire [5:2] b0;
   wire [5:2] b1;

   wire [6:3] c0;
   wire [6:3] c1;

   always @(*) begin
      a0 = a & {4{b[0]}};
      a1 = a & {4{b[1]}};
      a2 = a & {4{b[2]}};
      a3 = a & {4{b[3]}};
   end
   /* Dadda tree (http://en.wikipedia.org/wiki/Dadda_multiplier). But
      we also apply a half-adder at the low bit in each stage, since
      this adds no delay, and lets us use a shorter adder at the end.

      First stage:

         . . x .        a3
           . x x x      a2
             x x x x    a1
               x x x .  a0
         1 2 3 4 3 2 1 (Sum 16)

      Second and final stage:

           x   x        b2 (a3)
           x x x x      b1
         . x x x x . .  b0
         1 3 2 3 2 1 1 (Sum 13)

      Before add, needing a 4-bit adder:

         . . . .        c1
         . . . . . . .  c0
         2 2 2 2 1 1 1 (Sum 11)
    */

   /* First stage */
   assign p[0] = a0[0];
   add_ha s0_1 (a0[1], a1[1],         p[1], b0[2]);
   add_fa s0_2 (a0[2], a1[2], a2[2], b1[2], b0[3]);
   add_fa s0_3 (a0[3], a1[3], a2[3], b1[3], b0[4]);
   add_fa s0_4 (a1[4], a2[4], a3[4], b1[4], b0[5]);
   assign b1[5] = a2[5];
   
   /* Second stage */
   add_ha s1_2 (b0[2], b1[2],         p[2], c0[3]);
   add_fa s1_3 (b0[3], b1[3], a3[3], c1[3], c0[4]);
   add_ha s1_4 (b0[4], b1[4],        c1[4], c0[5]);
   add_fa s1_5 (b0[5], b1[5], a3[5], c1[5], c0[6]);
   assign c1[6] = a3[6];
   
   add_bk4 add (c0, c1, 1'b0, p[7:3]);
endmodule
