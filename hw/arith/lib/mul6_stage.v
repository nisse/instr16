/* Copyright (C) 2017, 2021  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Reduce

                 +---------------------+
                 |           a0        |
                 +---------------------+
                 |           a1        |
              +--+---------------------+
              |           a2           |
           +--+---------------------+--+
           |           a3           |
        +--+---------------------+--+
        |           a4           |
     +--+---------------------+--+
   + |           a5           |
   --+------------------------+

   as the three-layer Dadda-tree with weights

      n+2    n n-1 n        3        0
    a  1  2  3  4  6 ...    6  5  4  3   (sum 382)
             F  F 2F ...   2F  F  F  F   (125 full-adders)
    b  1  3  2  4  4 ...    3  4  3  1   (sum 257)
                F  F ...    F  F  F      (63 full adders)
    c  1  3  3  3  3 ...    2  3  1  1   (sum 194)
          F  F  F  F ...    H  F         (63 full adders, one half adder)
       2  2  2  2  2 ...    2  1  1  1   (sum 131)

     +---------------------------------+
     |           s0                    |
     +------------------------+--------+
   + |           s1           |
   --+------------------------+
*/

module mul6_stage(input [n-2:0] a0, input [n-2:0] a1,
		  input [n-1:0]  a2, input [n:1] a3,
		  input [n+1:2]  a4, input [n+2:3] a5,
		  output [n+2:0] s0, output [n+2:3] s1);
   parameter	n = 64;

   wire [n-2:1] b0;
   wire [n-1:1] b1;
   wire [n:3] b2;
   wire [n+1:4] b3;

   wire [n-1:2]	c0;
   wire [n:2] c1;

   // Leaves a2[n-1] for later.
   add_n_fa #(n-1) stage1_1(a0, a1, a2[n-2:0], b1, {b0, s0[0]});
   // Leaves a3[2:1], a4[n+1], a4[2], a5[n+1] for later.
   add_n_fa #(n-2) stage1_2(a3[n:3], a4[n:3], a5[n:3], b3, b2);
   assign s0[n+2] = a5[n+2];

   /* Leaves b3 as input to next stage, b2[n], and remaining a4 and a5 bits. */
   add_n_fa #(n-1) stage2({a2[n-1], b0}, b1, {b2[n-1:3], a3[2:1]}, c1, {c0, s0[1]});

   add_n_fa #(n) stage3({a4[n+1], b2[n], c0}, {a5[n+1], c1}, {b3, 1'b0, a4[2]},
			  s1, s0[n+1:2]);

endmodule
