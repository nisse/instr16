/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Generates a mask with cnt ones at the low end. */
module genmask(input [n-1:0] cnt, output [(1<<n)-2:0] mask);
   parameter n = 6;
   genvar i;
   generate
      for (i = 0; i < n; i = i + 1)
	begin : EXPAND
	   wire [(1<<(i+1))-2:0] m;
	   assign m[(1<<i)-1] = cnt[i];
	   if (i > 0) begin
	      assign m[(1<<i) - 2:0] = EXPAND[i-1].m[(1<<i) - 2:0]
		| {((1<<i) - 1){cnt[i]}};
	      assign m[(1<<(i+1))-2:(1<<i)] = EXPAND[i-1].m[(1<<i) - 2:0]
		& {((1<<i) - 1){cnt[i]}};
	   end
	end // block: EXPAND
   endgenerate
   assign mask = EXPAND[n-1].m;
endmodule
