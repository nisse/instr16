/* Copyright (C) 2023  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Reduce

                          +------------------------+
                          |           a0           |
                          +------------------------+
                          |           a1           |
                       +--+------------------------+
                       |           a2              |
                    +--+------------------------+--+
                    |           a3              |
                 +--+------------------------+--+
                 |           a4              |
              +--+------------------------+--+
              |           a5              |
           +--+------------------------+--+
           |           a6              |
        +--+------------------------+--+
        |           a7              |
     +--+------------------------+--+
   + |           a8              |
   --+---------------------------+

   as the four-layer Dadda-tree with weights

      n+5 +4 +3 +2 +1 n n-1  7  6     4     2     0
   a   1  2  3  4  5  6  7  9...9  8  7  6  5  4  3  (sum 574)
             F  F  F 2F 2F 3F..3F 2F 2F 2F  F  F  F  (187 FA)
   b   1  3  2  3  5  4  6...6  5  6  5  3  4  3  1  (sum 387)
          F     F  F  F 2F  2F FH 2F  F  F  F  F     (125 FA, 1 HA)
   c   2  1  3  2  4  4  4...4  4  3  4  2  3  1  1  (sum 262)
             F     F  F  F   F  F  F  F     F        (64 FA)
   d   2  2  1  3  3  3  3  3...3  2  2  3  1  1  1  (sum 6 + 61 * 3 + 12 = 198)
                F  F  F  F  F   F  H  H  F           (62 FA)
       2  2  2  2  2  2  2  2   2  2  2  1  1  1  1  (sum 136 needs a 67-bit adder)

     +--------------------------------------------+
     |           s0                               |
     +---------------------------------+----------+
   + |           s1                    |
   --+---------------------------------+
*/

module mul9_stage(input [n-2:0] a0, input [n-2:0] a1,
		  input [n-1:0]  a2, input [n:1] a3,
		  input [n+1:2]  a4, input [n+2:3] a5,
		  input [n+3:4]  a6, input [n+4:5] a7,
		  input [n+5:6]  a8,
		  output [n+5:0] s0, output [n+5:4] s1);
   parameter	n = 64;

   wire [n-2:1] b0;
   wire [n-1:1] b1;
   wire [n:3] b2;
   wire [n+1:4] b3;
   wire [n+3:6] b4;
   wire [n+4:7] b5;

   // Leaves a2[n-1] for later.
   add_n_fa #(n-1) stage1_1(a0, a1, a2[n-2:0], b1, {b0, s0[0]});
   // Leaves a3[2:1], a4[n+1], a4[2], a5[n+2:n+1] for later.
   add_n_fa #(n-2) stage1_2(a3[n:3], a4[n:3], a5[n:3], b3, b2);
   // Leaves a6[5:4], a7[n+4], a7[5], a8[n+4] for later.
   add_n_fa #(n-2) stage1_3(a6[n+3:6], a7[n+3:6], a8[n+3:6], b5, b4);
   assign s0[n+5] = a8[n+5];

   wire [n-1:2] c0;
   wire [n:2] 	c1;
   wire [n+2:5] c2;
   wire [n+3:6] c3;

   // Leaves b2[n] for later.
   add_n_fa #(n-1) stage2_1({a2[n-1], b0}, b1, {b2[n-1:3], a3[2:1]}, c1, {c0, s0[1]});
   // Leaves b3[4], b4[n+3], b5[n+3] for later
   add_n_fa #(n-2) stage2_2({a5[n+2], b3[n+1:5]}, {b4[n+2:6], a6[5]}, {b5[n+2:7], 1'b0, a7[5]}, c3, c2);
   assign {s1[n+5], s0[n+4]} = a7[n+4] + a8[n+4] + b5[n+4];

   wire [n+1:3] d0;
   wire [n+2:5] d1;

   // Leaves c0[3], c1[3], c2[n+2], and all of c3 (but c3[n+3]), for later
   add_n_fa #(n-2) stage_3({a4[n+1], b2[n], c0[n-1:4]}, {a5[n+1], c1[n:4]}, {c2[n+1:5], b3[4]}, d1, d0[n+1:4]);
   assign {d0[3], s0[2]} = a4[2] + c0[2] + c1[2];
   assign {s1[n+4], s0[n+3]} = b4[n+3] + b5[n+3] + c3[n+3];

   add_n_fa stage_4({c3[n+2:6], 2'b0, c0[3]}, {c2[n+2], d0}, {d1, a6[4], c1[3]}, s1[n+3:4], s0[n+2:3]);
endmodule
