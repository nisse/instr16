/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module mul_3w64(input clk, input rst, input enable,
		input [63:0]   a_in,
		input [63:0]   b_in,
		output reg     busy,
		output 	       ready,
		output [127:0] res_out);

   reg [5:0]	bits_left;
   /* Input, unchanged */
   reg [63:0] 	a;
   /* Work register, bits of b shifted out on the right,
      result bits shifted in from the left. */
   reg [62:0]	work;
   /* Sum bits */
   reg [63:0] 	s_reg;
   reg [63:0] 	c_reg;

   wire [64:0] 	s;
   wire [64:1] 	c;

   always @(posedge clk) begin
      if (rst) begin
	 bits_left <= 0;
	 busy <= 0;
      end
      else if (enable) begin
	 a <= a_in;
	 work <= {a_in[0] & b_in[0], b_in[63:2]};
	 bits_left <= 62;
	 s_reg <= {1'b0, a_in[63:1] & {63{b_in[0]}}};
	 c_reg <= a_in & {64{b_in[1]}};

	 busy <= 1;
      end
      else if (bits_left > 0) begin
	 bits_left <= bits_left - 1;
	 s_reg <= s[64:1];
	 c_reg <= c;

	 work <= {s[0], work[62:1]};
      end // if (bits_left > 0)
      else
	busy <= 0;
   end

   /* Output. Low 63 bits of the product were shifted into work, while
      the high 65 bits are the output of the final adder. */

   assign res_out[62:0] = work;
   assign ready = busy && (bits_left == 0);

   mul3_stage stage (s_reg, c_reg, a & {64{work[0]}}, s, c);
`ifdef USE_ADD_H64
   add_h64 final_add (s_reg, c_reg, 1'b0, res_out[127:63]);
`else
   assign res_out[127:63] = {1'b0, s_reg} + {1'b0, c_reg};
`endif
endmodule
