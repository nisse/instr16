/* Copyright (C) 2015, 2022  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module clz (input [(1<<n)-1:0] x, output [n:0] cnt);
   parameter n = 6;
   reg [(1<<n)-1:0] rx;
   integer    i;
   always @(*)
     for (i = 0; i < (1<<n); i = i+1)
       rx[i] = x[(1<<n) - 1 - i];

   ctz #(n) ctz (rx, cnt);
endmodule
