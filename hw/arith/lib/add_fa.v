/* Copyright (C) 2014  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Sum of 3 bits, aka full adder */
module add_fa (input x0, input x1, input x2, output s0, output s1);
   assign s0 = x0 ^ x1 ^ x2;
   assign s1 = (x0 & x1) | (x0 & x2) | (x1 & x2);
endmodule
