/* Copyright (C) 2023  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Reduce

                                      +--------------------------------+
                                      |               a0               |
                                      +--------------------------------+
                                      |               a1               |
                                   +--+--------------------------------+
                                   |               a2                  |
                                +--+--------------------------------+--+
                                |               a3                  |
                             +--+--------------------------------+--+
                             |               a4                  |
                          +--+--------------------------------+--+
                          |               a5                  |
                       +--+--------------------------------+--+
                       |               a6                  |
                    +--+--------------------------------+--+
                    |               a7                  |
                 +--+--------------------------------+--+
                 |               a8                  |
              +--+--------------------------------+--+
              |                a9                 |
           +--+--------------------------------+--+
           |                a10                |
        +--+--------------------------------+--+
        |                a11                |
     +--+--------------------------------+--+
   + |                a12                |
   --+-----------------------------------+

   as the five-layer Dadda-tree with weights

     n+9 +8 +7 +6 +5 +4 +3 +2 n+1n n-1..10 9  8  7  6  5  4  3  2  1  0
    a  1  2  3  4  5  6  7  8  9 10 11..13 12 11 10 9  8  7  6  5  4  3 (sum 830)
             F  F  F 2F 2F 2F 3F 3F 3F  4F 4F 3F 3F 3F 2F 2F 2F F  F  F (252 FA)
    b  1  3  2  3  5  4  5  7  6  7  9.. 9 7  8? 7  5  6  5  3  4  3  1 (sum 578)
          F     F  F  F  F 2F 2F 2F 3F  3F 2F 2F 2F F  2F F  F  F  F    (186 FA)
    c  2  1  3  2  4  3  5  5  4  6  6.. 5 5  6  4  5  3  4  2  3  1  1 (sum 391)
             F     F  F  F  F  F 2F 2F  FH FH 2F F  F  F  F     F       (123 FA 2HA)
    d  2  2  1  3  3  2  4  4  4  4  4.. 4 4  3  3  4  2  2  3  1  1  1 (sum 268)
                F  F     F  F  F  F  F   F F  F  F  F        F          (65 FA)
    e  2  2  2  2  1  3  3  3  3  3  3   3 3  2  2  2  2  3  1  1  1  1 (sum 203)
                      F  F  F  F  F  F   F F  H? H  H  H  F             (61 FA 1HA)
    s  2  2  2  2  2  2  2  2  2  2  2   2 1  2  2  2  2  1  1  1  1  1 (sum 142)
*/

module mul13_stage(input [n-2:0]  a0, input [n-2:0] a1, input [n-1:0] a2,
		   input [n:1] 	  a3, input [n+1:2] a4, input [n+2:3] a5,
		   input [n+3:4]  a6, input [n+4:5] a7, input [n+5:6] a8,
		   input [n+6:7]  a9, input [n+7:8] a10, input [n+8:9] a11,
		   input [n+9:10] a12,
		   output [n+9:0] s0, output [n+9:5] s1);
   parameter	n = 64;

   wire [n-2:1] b0;
   wire [n-1:1] b1;
   wire [n+1:3] b2;
   wire [n+2:4] b3;
   wire [n+4:6] b4;
   wire [n+5:7] b5;
   wire [n+7:9] b6;
   wire [n+8:10] b7;

   // Bits [n-2:0]. Leaves a2[n-1] for later.
   add_n_fa #(n-1) stage1_1(a0, a1, a2[n-2:0], b1, {b0, s0[0]});
   // Bits [n+1:3]. Leaves a3[2:1], a4[n+1], a4[2], a5[n+2] for later.
   add_n_fa #(n-1) stage1_2({a12[n+1], a3[n:3]}, a4[n+1:3], a5[n+1:3], b3, b2);
   // Bits [n+4:6]. Leaves a6[5:4], a7[5], a8[n+5] for later.
   add_n_fa #(n-1) stage1_3({a12[n+4], a6[n+3:6]}, a7[n+4:6], a8[n+4:6], b5, b4);
   // Bits [n+7:9]. Leaves a9[8:7], a10[8], a11[n+8] for later
   add_n_fa #(n-1) stage1_4({a12[n+7], a9[n+6:9]}, a10[n+7:9], a11[n+7:9], b7, b6);
   // Leaves most of a12 (except a12[n+1], a12[n+4], a12[n+7] and a12[n+9]) for later.
   assign s0[n+9] = a12[n+9];

   wire [n-1:2]  c0;
   wire [n:2] 	 c1;
   wire [n+2:5]  c2;
   wire [n+3:8]  c3;
   wire [n+6:10] c4;
   wire [n+7:11] c5;

   // Bits [n-1:1]. Leaves b2[n] for later.
   add_n_fa #(n-1) stage2_1({a2[n-1], b0}, b1, { b2[n-1:3], a3[2:1]}, c1, {c0, s0[1]});
   // Bits [n+2:7]. Leaves b3[6], b3[4], b4[n+3], b4[6], b5[n+5:n+3] for later.
   add_n_fa #(n-4) stage2_2(b3[n+2:7], b4[n+2:7], b5[n+2:7], c3, c2[n+2:7]);
   // Bits [n+6:10]. Leaves a12[n+8], b6[n+7], b6[9]
   add_n_fa #(n-3) stage2_3(b6[n+6:10], b7[n+6:10],
			    {a12[n+6:n+5], b4[n+4], a12[n+3:n+2], b2[n+1], a12[n:10]}, c5, c4);
   assign {c2[6:5]} = a6[5] + a7[5] + b3[5];
   assign {s1[n+9], s0[n+8]} = a11[n+8] + a12[n+8] + b7[n+8];

   wire [n:3] 	 d0;
   wire [n+1:5]  d1;
   wire [n+5:8]  d2;
   wire [n+6:9]  d3;

   // Bits [n:4] .Leaves c0[3], c1[3], c2[n+2:n+1] for later.
   add_n_fa #(n-3) stage3_1({b2[n], c0[n-1:4]}, c1[n:4], {c2[n:5], b3[4]}, d1, d0[n:4]);
   // Bits [n+5:8]. Leaves c4[n+6], c5[n+6] for later.
   add_n_fa #(n-2) stage3_2({b5[n+5:n+4], c3}, {c4[n+5:10], b6[9], a9[8]},
			    {c5[n+5:11], 2'b0, a10[8]}, d3, d2);
   assign {d0[3], s0[2]} = a4[2] + c0[2] + c1[2];
   assign {s1[n+8], s0[n+7]} = b6[n+7] + b7[n+7] + c5[n+7];

   wire [n+3:6]  e0;
   wire [n+4:7]  e1;
   wire 	 e0_4;

   // Bits [n+3:6]. Leaves d0[5:4], d1[n+1], d1[5], d2[n+5:n+4], and d3[n+5:10] for later.
   add_n_fa #(n-2) stage4({b4[n+3], c2[n+2:n+1], d0[n:6]}, {b5[n+3], a5[n+2], d1[n+1:6]},
			  {d2[n+3:8], a9[7], b3[6]}, e1, e0);
   assign {e0_4, s0[3]} = c0[3] + c1[3] + d0[3];
   assign {s1[n+6], s0[n+5]} = a8[n+5] + d2[n+5] + d3[n+5];
   assign {s1[n+7], s0[n+6]} = c4[n+6] + c5[n+6] + d3[n+6];

   // Bits [n+4:4].
   add_n_fa #(n+1) stage_5({d3[n+4:9], 4'b0, a6[4]}, {d2[n+4], e0, d0[5:4]},
			   {e1, b4[6], d1[5], e0_4}, s1[n+5:5], s0[n+4:4]);
endmodule
