/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Sets P <-- P + q D. P is represented as s + c. Old sign bit is
   omitted, since the operation is expected to cancel one bit and
   hence s[n+1] == s[n]. q is represented as 00 for zero, 01 for +1
   and 11 for -1.
 */
module srt_update(input [n:0] s_in, input [n-3:2] c_in,
		  input [n-2:0] d, input [1:0] q,
		  output [n:0] s_out, output [n-4:1] c_out);
   parameter n = 64;
   wire [n:0]	qd;
   wire	cn3;

   genvar	i;

   assign qd = ({2'b01, d} ^ {(n+1){q[1]}}) & {(n+1){q[0]}};
   assign { c_out[1], s_out[0] } = s_in[0] + q[1] + qd[0];
   assign { c_out[2], s_out[1] } = s_in[1] + qd[1];

   generate
      for (i = 2; i < n-4; i = i + 1) begin
	 assign { c_out[i+1], s_out[i] } = s_in[i] + qd[i] + c_in[i];
      end
   endgenerate
   assign { cn3, s_out[n-4] } = s_in[n-4] + qd[n-4] + c_in[n-4];

   /* Remains to add
      s[n] s[n-1] s[n-2] s[n-3]
      qd    qd    qd     qd
                         c[n-3]
                         cn3
     --------------------------
      s[n] s[n-1] s[n-2] s[n-3]
    */

   /* FIXME: Could use something more sophisticated for lower latency. */
   assign s_out[n:n-3] = s_in[n:n-3] + qd[n:n-3] 
			 + {3'b000, c_in[n-3]} + {3'b00, cn3};
endmodule

module srt_qsel(input [3:0] s, output [1:0] q);
   wire q_non_zero;

   assign q_non_zero = (| s[3:0]) & !(&s[3:1]);
   assign q = {q_non_zero & !s[3], q_non_zero};
endmodule // srt_qsel

module srt_final_q (input [2*n-1:0] q, input adjust, output [n-1:0] out);
   parameter n = 64;
   genvar i;
   wire [n-1:0] q_pos;
   wire [n-1:0] q_neg;

   generate
      for (i = 0; i < n; i = i + 1) begin
	 assign q_pos[i] = q[2*i+1];
	 assign q_neg[i] = q[2*i+1] | !q[2*i];
      end
   endgenerate

   // Could use add_h64
   assign out = q_pos + q_neg + {{(n-1){1'b0}}, ~adjust};
endmodule // srt_final_q

// NOTE: Requires d input to be kept stable, in reciprocal_unit it is
// latched before normalization and the shifted value is passed here.
module srt_reciprocal(input clk, input rst, input enable,
		      input [n-2:0]  d,
		      output reg     busy, output ready,
		      output [n-1:0] q_out);
   localparam n = 64;
   reg [6:0]	bits_left; // FIXME: Trim one bit?
   // Partial remainder P represented as s + c
   reg [n:0]	s_reg;
   reg [n-4:1] 	c_reg;

   reg [2*n-1:0] q_reg;

   wire [1:0]	q;
   wire [n:0]	s;
   wire [n-4:1] c;
   wire [n:1]	p;

   always @(posedge clk) begin
      if (rst) begin
	 bits_left <= 0;
	 busy <= 0;
      end
      else if (enable) begin
	 s_reg <= {2'b0,~d};
	 c_reg <= 0;
	 q_reg <= {2*n{1'bx}};
	 bits_left <= n;
	 busy <= 1;
      end
      else if (bits_left > 0) begin
	 s_reg <= s;
	 c_reg <= c;
	 bits_left <= bits_left - 1;
	 q_reg <= {q_reg[2*n-3:0], q};
      end
      else
	busy <= 0;
   end // always @ (posedge clk)

   assign ready = busy && (bits_left == 0);

   srt_qsel qsel(s_reg[n:n-3], q);
   srt_update #(n) update({s_reg[n-1:0], 1'b1}, c_reg, d, q, s, c);

   /* FIXME: Only need sign bit from this addition. Should be arranged
      so we get the resulting sign from a Kogge-Stone tree, in
      parallel with most of the addition work to construct q, with
      only final carry-select logic remaining when the sign is ready.
    */
   assign p = s_reg[n:1] + {3'b0, c_reg};

   srt_final_q #(n) final_q (q_reg, p[n], q_out);
endmodule // srt_reciprocal
