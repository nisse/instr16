/* Copyright (C) 2017, 2021  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module mul_6w64(input clk, input rst, input enable,
		input [63:0]   a_in,
		input [63:0]   b_in,
		output reg     busy,
		output 	       ready,
		output [127:0] res_out);

   reg [4:0]	steps_left;
   /* Input, unchanged */
   reg [63:0]	a;
   /* Work register, bits of b shifted out on the right,
      result bits shifted in from the left. */
   reg [61:0]	work;
   /* Sum bits */
   reg [66:0]	s_reg;
   reg [66:3]	c_reg;
   /* Carry from summing the low bits of previous iteration. */
   reg		carry;

   wire [66:0] 	s;
   wire [66:3]	c;
   wire [4:0]	low_sum;

   always @(posedge clk) begin
      if (rst) begin
	 steps_left <= 0;
	 busy <= 0;
      end
      else if (enable) begin
	 a <= a_in;
	 work <= b_in[63:2];
	 steps_left <= 16;
	 s_reg <= {1'b0, a_in & {64{b_in[0]}}, 2'b0 };
	 c_reg <= a_in & {64{b_in[1]}};
	 carry <= 0;

	 busy <= 1;
      end // if (enable)
      else if (steps_left > 0) begin
	 steps_left <= steps_left - 1;
	 s_reg <= s;
	 c_reg <= c;
	 carry <= low_sum[4];
	 work <= {low_sum[3:0], work[61:4]};
      end
      else
	busy <= 0;
   end // always @ (posedge clk)

   mul6_stage stage (s_reg[66:4], c_reg[66:4],
		     a & {64{work[0]}},
		     a & {64{work[1]}},
		     a & {64{work[2]}},
		     a & {64{work[3]}},
		     s, c);
   assign low_sum = s_reg[3:0] + {c_reg[3], 2'b0, carry};

   /* Output. Low 61 bits of the product were shifted into work, while
      the high 67 bits are the output of the final adder. */
   assign res_out[61:0] = work;
   assign res_out[65:62] = low_sum[3:0];
   /* It should be possible to arrange that at least one of s_reg[65]
      and c_reg[65] is always zero, since the a4 and a5 inputs to
      mul6_stage are zero for the last iteration. */
   assign res_out[127:66] = {s_reg[65:4]} + {c_reg[65:4]} + low_sum[4];

   assign ready = busy && (steps_left == 0);
endmodule // mul_6w64
