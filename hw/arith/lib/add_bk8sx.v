/* Copyright (C) 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 8-bit Brent-Kung adder, with inputs g0 = x & y, p0 = x ^y, and
   separate outputs with and without input carry. */
module add_bk8sx (input [7:0] g0, input [7:0] p0,
		  output [8:0] s0, output [8:0] s1);
   wire [3:0]		     g1; /* 7,  5,  3,  1, */
   wire [3:0]		     p1; /* 7,  5,  3,  1, */
   wire [1:0]		     g2; /* 7,      3,     */
   wire [1:0]		     p2; /* 7,      3,     */
   wire [1:0]		     g3; /* 7,  5,         */
   wire [1:0]		     p3; /* 7,  5,         */
   wire [2:0] 		     g4; /*   6,  4,  2    */
   wire [2:0] 		     p4; /*   6,  4,  2    */
   reg [7:0] 		     G;
   reg [7:0] 		     P;
   reg [7:0] 		     c;

   add_gp  d0 (g0[1], p0[1], g0[0], p0[0], g1[0], p1[0]);
   add_gp  d1 (g0[3], p0[3], g0[2], p0[2], g1[1], p1[1]);
   add_gp  d2 (g0[5], p0[5], g0[4], p0[4], g1[2], p1[2]);
   add_gp  d3 (g0[7], p0[7], g0[6], p0[6], g1[3], p1[3]);

   add_gp  d4 (g1[1], p1[1], g1[0], p1[0], g2[0], p2[0]);
   add_gp  d5 (g1[3], p1[3], g1[2], p1[2], g2[1], p2[1]);

   add_gp  d6 (g1[2], p1[2], g2[0], p2[0], g3[0], p3[0]);
   add_gp  d7 (g2[1], p2[1], g2[0], p2[0], g3[1], p3[1]);

   add_gp  d8 (g0[2], p0[2], g1[0], p1[0], g4[0], p4[0]);
   add_gp  d9 (g0[4], p0[4], g2[0], p2[0], g4[1], p4[1]);
   add_gp d10 (g0[6], p0[6], g3[0], p3[0], g4[2], p4[2]);

   always @(*) begin
      G = {g3[1], g4[2], g3[0], g4[1], g2[0], g4[0], g1[0], g0[0] };
      P = {p3[1], p4[2], p3[0], p4[1], p2[0], p4[0], p1[0], p0[0] };
      c = G | P;
   end
   /* cin == 0 */
   assign s0 = {1'b0, p0} ^ {G, 1'b0};

   /* cin == 1 */
   assign s1 = {1'b0, p0 } ^ {c, 1'b1};
endmodule
