/* Copyright (C) 2014, 2015, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 64-bit "hybrid" adder, see
   http://robey.lag.net/2012/11/14/how-to-add-numbers-2.html. With
   inputs g = x & y, p = x ^ y. */
module add_h64x (input [63:0] g, input [63:0] p, input cin,
		 output [64:0] s);
   wire [8:0] 		      s1c0;
   wire [8:0] 		      s1c1;
   wire [8:0] 		      s2c0;
   wire [8:0] 		      s2c1;
   wire [8:0] 		      s3c0;
   wire [8:0] 		      s3c1;
   wire [8:0] 		      s4c0;
   wire [8:0] 		      s4c1;
   wire [8:0] 		      s5c0;
   wire [8:0] 		      s5c1;
   wire [8:0] 		      s6c0;
   wire [8:0] 		      s6c1;
   wire [8:0] 		      s7c0;
   wire [8:0] 		      s7c1;
   wire 		      c0;
   reg [6:1] 		      c;

   add_bk8x  a0 (g[ 7: 0], p[ 7: 0], cin, {c0, s[7:0]});
   add_bk8sx a1 (g[15: 8], p[15: 8], s1c0, s1c1);
   add_bk8sx a2 (g[23:16], p[23:16], s2c0, s2c1);
   add_bk8sx a3 (g[31:24], p[31:24], s3c0, s3c1);
   add_bk8sx a4 (g[39:32], p[39:32], s4c0, s4c1);
   add_bk8sx a5 (g[47:40], p[47:40], s5c0, s5c1);
   add_bk8sx a6 (g[55:48], p[55:48], s6c0, s6c1);
   add_bk8sx a7 (g[63:56], p[63:56], s7c0, s7c1);

   /* Carry select logic */
   always @(*) begin
      c[1] = c0 ? s1c1[8] : s1c0[8];
      c[2] = c[1] ? s2c1[8] : s2c0[8];
      c[3] = c[2] ? s3c1[8] : s3c0[8];
      c[4] = c[3] ? s4c1[8] : s4c0[8];
      c[5] = c[4] ? s5c1[8] : s5c0[8];
      c[6] = c[5] ? s6c1[8] : s6c0[8];
   end
   assign s[15:8]  = c0 ? s1c1[7:0] : s1c0[7:0];
   assign s[23:16] = c[1] ? s2c1[7:0] : s2c0[7:0];
   assign s[31:24] = c[2] ? s3c1[7:0] : s3c0[7:0];
   assign s[39:32] = c[3] ? s4c1[7:0] : s4c0[7:0];
   assign s[47:40] = c[4] ? s5c1[7:0] : s5c0[7:0];
   assign s[55:48] = c[5] ? s6c1[7:0] : s6c0[7:0];
   assign s[64:56] = c[6] ? s7c1[8:0] : s7c0[8:0];
endmodule

module add_h64(input [63:0] x, input [63:0] y, input cin,
	       output [64:0] s);
   add_h64x add(x & y, x ^ y, cin, s);
endmodule
