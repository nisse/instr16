/* Copyright (C) 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 4-bit Brent-Kung adder */
module add_bk4 (input [3:0] x, input [3:0] y, input cin, 
		output [4:0] s);
   reg [3:0]		     g0; /* 3,2,1,0 */
   reg [3:0]		     p0; /* 3,2,1,0 */
   wire [1:0] 		     g1; /* 3,  1,  */
   wire [1:0]		     p1; /* 3,  1,  */
   wire [1:0]		     g2; /* 3,2     */
   wire [1:0]		     p2; /* 3,2     */
   reg [3:0] 		     G;
   reg [3:0] 		     P;
   reg [3:0] 		     c;

   always @(*) begin
      g0 = x & y;
      /* Could use inclusive or for the carry propagation */
      p0 = x ^ y;
   end

   add_gp d0 (g0[1], p0[1], g0[0], p0[0], g1[0], p1[0]);
   add_gp d1 (g0[3], p0[3], g0[2], p0[2], g1[1], p1[1]);

   add_gp d2 (g0[2], p0[2], g1[0], p1[0], g2[0], p2[0]);
   add_gp d3 (g1[1], p1[1], g1[0], p1[0], g2[1], p2[1]);

   always @(*) begin
      G = { g2[1], g2[0], g1[0], g0[0]};
      P = { p2[1], p2[0], p1[0], p0[0]};

      c = G | (P & {4{cin}});
   end

   assign s[3:0] = p0[3:0] ^ {c[2:0], cin};
   assign s[4] = c[3];
endmodule   
