/* Copyright (C) 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 16-bit Brent-Kung adder */
module add_bk16 (input [15:0] x, input [15:0] y, input cin,
		 output [16:0] s);
   reg [15:0] 		     g0; /* 15...0 */
   reg [15:0] 		     p0; /* 15...0 */
   wire [7:0]		     g1; /* 15,13,11,9,7,5,3,1 */
   wire [7:0]		     p1; /* 15,13,11,9,7,5,3,1 */
   wire [3:0]		     g2; /* 15,   11,  7,  3   */
   wire [3:0]		     p2; /* 15,   11,  7,  3   */
   wire [1:0]		     g3; /* 15,        7,      */
   wire [1:0]		     p3; /* 15,        7,      */
   wire [1:0] 		     g4; /* 15,   11           */
   wire [1:0] 		     p4; /* 15,   11           */

   wire [2:0] 		     g5; /*    13,   9,  5     */
   wire [2:0] 		     p5; /*    13,   9,  5     */
 		     
   wire [6:0] 		     g6; /* 14,12,10,8,6,4,2*/
   wire [6:0] 		     p6; /* 14,12,10,8,6,4,2*/

   reg [15:0] 		     G;
   reg [15:0] 		     P;
   reg [15:0] 		     c;

   always @(*) begin
      g0 = x & y;
      /* Could use inclusive or for the carry propagation */
      p0 = x ^ y;
   end

   add_gp  d0 ( g0[1], p0[1], g0[0], p0[0], g1[0], p1[0]); /* 1 x 0 */
   add_gp  d1 ( g0[3], p0[3], g0[2], p0[2], g1[1], p1[1]); /* 3 x 2 */
   add_gp  d2 ( g0[5], p0[5], g0[4], p0[4], g1[2], p1[2]); /* 5 x 4 */
   add_gp  d3 ( g0[7], p0[7], g0[6], p0[6], g1[3], p1[3]); /* 7 x 6 */
   add_gp  d4 ( g0[9], p0[9], g0[8], p0[8], g1[4], p1[4]); /* 9 x 8 */
   add_gp  d5 (g0[11],p0[11],g0[10],p0[10], g1[5], p1[5]); /* 11 x 10 */
   add_gp  d6 (g0[13],p0[13],g0[12],p0[12], g1[6], p1[6]); /* 13 x 12 */
   add_gp  d7 (g0[15],p0[15],g0[14],p0[14], g1[7], p1[7]); /* 15 x 14 */

   add_gp  d8 (g1[1], p1[1], g1[0], p1[0], g2[0], p2[0]); /* 3 x 1 */
   add_gp  d9 (g1[3], p1[3], g1[2], p1[2], g2[1], p2[1]); /* 7 x 5 */
   add_gp d10 (g1[5], p1[5], g1[4], p1[4], g2[2], p2[2]); /* 11 x 9 */
   add_gp d11 (g1[7], p1[7], g1[6], p1[6], g2[3], p2[3]); /* 15 x 13 */

   add_gp d12 (g2[1], p2[1], g2[0], p2[0], g3[0], p3[0]); /* 7 x 3 */
   add_gp d13 (g2[3], p2[3], g2[2], p2[2], g3[1], p3[1]); /* 15 x 13 */

   add_gp d14 (g2[2], p2[2], g3[0], p3[0], g4[0], p4[0]); /* 11 x 7 */
   add_gp d15 (g3[1], p3[1], g3[0], p3[0], g4[1], p4[1]); /* 15 x 7 */

   add_gp d16 (g1[2], p1[2], g2[0], p2[0], g5[0], p5[0]); /* 5 x 3 */
   add_gp d17 (g1[4], p1[4], g3[0], p3[0], g5[1], p5[1]); /* 9 x 7 */
   add_gp d18 (g1[6], p1[6], g4[0], p4[0], g5[2], p5[2]); /* 13 x 11 */

   add_gp d19 (g0[2], p0[2], g1[0], p1[0], g6[0], p6[0]); /* 2 x 1 */
   add_gp d21 (g0[4], p0[4], g2[0], p2[0], g6[1], p6[1]); /* 4 x 3 */
   add_gp d22 (g0[6], p0[6], g5[0], p5[0], g6[2], p6[2]); /* 6 x 5 */
   add_gp d23 (g0[8], p0[8], g3[0], p3[0], g6[3], p6[3]); /* 8 x 7 */
   add_gp d24 (g0[10], p0[10], g5[1], p5[1], g6[4], p6[4]); /* 10 x 9 */
   add_gp d25 (g0[12], p0[12], g4[0], p4[0], g6[5], p6[5]); /* 12 x 11 */
   add_gp d26 (g0[14], p0[14], g5[2], p5[2], g6[6], p6[6]); /* 14 x 13 */

   always @(*) begin
      G = {g4[1],g6[6],g5[2],g6[5],g4[0],g6[4],g5[1],g6[3],
	   g3[0],g6[2],g5[0],g6[1],g2[0],g6[0],g1[0],g0[0]};
      P = {p4[1],p6[6],p5[2],p6[5],p4[0],p6[4],p5[1],p6[3],
	   p3[0],p6[2],p5[0],p6[1],p2[0],p6[0],p1[0],p0[0]};

      c = G | (P & {16{cin}});
   end

   assign s[15:0] = p0[15:0] ^ {c[14:0],cin};
   assign s[16] = c[15];
endmodule
