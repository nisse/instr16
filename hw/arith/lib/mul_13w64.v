/* Copyright (C) 2017, 2021, 2023  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module mul_13w64(input clk, input rst, input enable,
		input [63:0]   a_in,
		input [63:0]   b_in,
		output reg     busy,
		output 	       ready,
		output [127:0] res_out);

   reg [2:0]	steps_left;
   /* Input, unchanged */
   reg [63:0]	a;
   /* Work register, bits of b shifted out on the right,
      result bits shifted in from the left. */
   reg [61:0]	work;
   /* Sum bits */
   reg [73:0]	s_reg;
   reg [73:5]	c_reg;
   /* Carry from summing the low bits of previous iteration. */
   reg		carry;

   wire [73:0]	s;
   wire [73:5]	c;
   wire [11:0]	low_sum;

   always @(posedge clk) begin
      if (rst) begin
	 steps_left <= 0;
	 busy <= 0;
      end
      else if (enable) begin
	 a <= a_in;
	 work <= b_in[63:2];
	 steps_left <= 6;
	 s_reg <= {1'b0, a_in & {64{b_in[0]}}, 9'b0};
	 c_reg <= {a_in & {64{b_in[1]}}, 5'b0};
	 carry <= 0;

	 busy <= 1;
      end // if (enable)
      else if (steps_left > 0) begin
	 steps_left <= steps_left - 1;
	 s_reg <= s;
	 c_reg <= c;
	 carry <= low_sum[11];

	 work <= {low_sum[10:0], work[61:11]};
      end
      else
	busy <= 0;
   end // always @ (posedge clk)

   mul13_stage stage (s_reg[73:11], c_reg[73:11],
		     a & {64{work[0]}},
		     a & {64{work[1]}},
		     a & {64{work[2]}},
		     a & {64{work[3]}},
		     a & {64{work[4]}},
		     a & {64{work[5]}},
		     a & {64{work[6]}},
		     a & {64{work[7]}},
		     a & {64{work[8]}},
		     a & {64{work[9]}},
		     a & {64{work[10]}},
		     s, c);
   assign low_sum = s_reg[10:0] + {c_reg[10:5], 4'b0, carry};

   /* Output. Low 57 bits of the product were shifted into work, while
      the high 71 bits are the output of the final adder. */
   assign res_out[56:0] = work[61:5];
   assign res_out[67:57] = low_sum[10:0];
   /* Last iteration uses only the last 7 bits of b, making top bits
      of s_reg and c_reg always zero. */
   assign res_out[127:68] = s_reg[70:11] + c_reg[70:11] + low_sum[11];

   assign ready = busy && (steps_left == 0);
endmodule // mul_13w64
