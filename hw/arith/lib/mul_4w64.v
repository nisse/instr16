/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module mul_4w64(input clk, input rst, input enable,
		input [63:0]   a_in,
		input [63:0]   b_in,
		output reg     busy,
		output 	       ready,
		output [127:0] res_out);

   reg [4:0]	steps_left;
   /* Input, unchanged */
   reg [63:0]	a;
   /* Work register, bits of b shifted out on the right,
      result bits shifted in from the left. */
   reg [62:0]	work;
   /* Sum bits */
   reg [63:0]	s_reg;
   reg [63:0]	c_reg;

   wire [65:0]	s;
   wire [65:2]	c;

   always @(posedge clk) begin
      if (rst) begin
	 steps_left <= 0;
	 busy <= 0;
      end
      else if (enable) begin
	 a <= a_in;
	 work <= {a_in[0] & b_in[0], b_in[63:2]};
	 steps_left <= 31; /* Two bits per step. */
	 s_reg <= {1'b0, a_in[63:1] & {63{b_in[0]}}};
	 c_reg <= a_in & {64{b_in[1]}};

	 busy <= 1;
      end
      else if (steps_left > 0) begin
	 steps_left <= steps_left - 1;
	 s_reg <= s[65:2];
	 c_reg <= c;

	 work <= {s[1:0], work[62:2]};
      end // if (steps_left > 0)
      else
	busy <= 0;
   end

   /* Output. Low 63 bits of the product were shifted into work, while
      the high 65 bits are the output of the final adder. */
   assign res_out[62:0] = work;
   assign ready = busy && (steps_left == 0);

   mul4_stage stage (s_reg, c_reg, a & {64{work[0]}}, a & {64{work[1]}},
		     s, c);
`ifdef USE_ADD_H64
   add_h64 final_add (s_reg, c_reg, 1'b0, res_out[127:63]);
`else
   assign res_out[127:63] = {1'b0, s_reg} + {1'b0, c_reg};
`endif
endmodule
