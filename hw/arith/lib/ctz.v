/* Copyright (C) 2015, 2022  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module ctz (input [(1<<n)-1:0] x, output [n:0] out);
   parameter n = 6;
   genvar i, j;
   generate
      for (i = 0; i < n; i = i+1)
	begin : REDUCE
	   // Combine adjacent pairs of 1<<i bits. 
	   // Input bit counts are i+1 bits, output bit count is i+2 bits.
	   wire [i+1:0] cnt[(1<<(n-i-1))-1:0];
	   for (j = 0; j < (1<<(n-i-1)); j = j + 1)
	     begin : REDUCE_LOOP
		wire [i:0] lo, hi;
		if (i == 0) begin
		   assign lo = ~x[2*j];
		   assign hi = ~x[2*j+1];
		end
		else begin
		   assign lo = REDUCE[i-1].cnt[2*j];
		   assign hi = REDUCE[i-1].cnt[2*j+1];
		end
		// lo[i] set means lo half all-zero
		assign cnt[j] = lo[i] ? {1'b0, hi} + (1<<i) : {1'b0, lo};
	     end
	end
   endgenerate
   assign out = REDUCE[n-1].cnt[0];
endmodule
