/* Copyright (C) 2017, 2021, 2023  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module mul_9w64(input clk, input rst, input enable,
		input [63:0]   a_in,
		input [63:0]   b_in,
		output reg     busy,
		output 	       ready,
		output [127:0] res_out);

   reg [3:0]	steps_left;
   /* Input, unchanged */
   reg [63:0]	a;
   /* Work register, bits of b shifted out on the right,
      result bits shifted in from the left. */
   reg [61:0]	work;
   /* Sum bits */
   reg [69:0]	s_reg;
   reg [69:4]	c_reg;
   /* Carry from summing the low bits of previous iteration. */
   reg		carry;

   wire [69:0]	s;
   wire [69:4]	c;
   wire [7:0]	low_sum;

   always @(posedge clk) begin
      if (rst) begin
	 steps_left <= 0;
	 busy <= 0;
      end
      else if (enable) begin
	 a <= a_in;
	 work <= b_in[63:2];
	 steps_left <= 9;
	 s_reg <= {1'b0, a_in & {64{b_in[0]}}, 5'b0};
	 c_reg <= {a_in & {64{b_in[1]}}, 2'b0};
	 carry <= 0;

	 busy <= 1;
      end // if (enable)
      else if (steps_left > 0) begin
	 steps_left <= steps_left - 1;
	 s_reg <= s;
	 c_reg <= c;
	 carry <= low_sum[7];

	 work <= {low_sum[6:0], work[61:7]};
      end
      else
	busy <= 0;

   end // always @ (posedge clk)

   mul9_stage stage (s_reg[69:7], c_reg[69:7],
		     a & {64{work[0]}},
		     a & {64{work[1]}},
		     a & {64{work[2]}},
		     a & {64{work[3]}},
		     a & {64{work[4]}},
		     a & {64{work[5]}},
		     a & {64{work[6]}},
		     s, c);
   assign low_sum = s_reg[6:0] + {c_reg[6:4], 3'b0, carry};

   /* Output. Low 58 bits of the product were shifted into work, while
      the high 70 bits are the output of the final adder. */
   assign res_out[57:0] = work[61:4];
   assign res_out[64:58] = low_sum[6:0];
   assign res_out[127:65] = s_reg[69:7] + c_reg[69:7] + low_sum[7];

   assign ready = busy && (steps_left == 0);
endmodule // mul_9w64
