/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Reduce

           +------------------------+
           |           a0           |
           +------------------------+
           |           a1           |
        +--+---------------------+--+
        |           a2           |
     +--+---------------------+--+
   + |           a3           |
   --+------------------------+

   as the two-layer Dadda-tree with weights

       1  2  4  4  ...      4  3  2
             F  F  ...      F  F  H  (63 full-adders )
       1  3  3  3  ...      3  2  1
          F  F  F  ...      F  H     (63 full-adders
       2  2  2  2  ...      2  1

     +------------------------------+
     |           s0                 |
     +------------------------+-----+
   + |           s1           |
   --+------------------------+
*/

module mul4_stage(input [n-1:0] a0, input [n-1:0] a1,
		  input [n:1]  a2, input [n+1:2] a3,
		  output [n+1:0] s0, output [n+1:2] s1);
   parameter	n = 64;

   wire [n-1:1] b0;
   wire [n:1] 	b1;

   add_n_fa #(n) stage1(a0[n-1:0], a1[n-1:0], {a2[n-1:1], 1'b0}, b1[n:1], {b0[n-1:1], s0[0]});
   add_n_fa #(n) stage2({a2[n], b0[n-1:1]}, b1[n:1], {a3[n:2],1'b0}, s1[n+1:2], s0[n:1]);

   assign s0[n+1] = a3[n+1];
endmodule
