/* Copyright (C) 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Computed v = floor ((2^128 -1) / d).

// d is 64-bits and v is 65 bits. Both have the highest bit always
// set, and implicit in the interface.

module reciprocal (input [62:0] d, output [63:0] v);
   reg v0;
   reg [63:60] v1;
   reg [63:58] v2;
   reg [63:54] v3;
   reg [63:46] v4;
   reg [63:30] v5;
   reg [63:0]  v6;
   reg	       adj;

   reg signed [4:0] e0;
   reg signed [3:0] c1;

   reg signed [7:0] e1;
   reg signed [5:0] c2;

   reg signed [11:0] e2;
   reg signed [6:0] c3;

   reg signed [19:0] e3;
   reg signed [10:0] c4;

   reg signed [35:0] e4;
   reg signed [18:0] c5;

   reg signed [65:0] e5;
   reg signed [32:0] c6;

   always @(*) begin
      v0 = (d[62:60] < 5); // 1 bit

      e0 = -({1'b1,d[62:60]} * {1'b1,v0}); // -4 <= e0 <= 8
      c1 = ($signed({5'b1,v0}) * e0) >> 2; // -1 <= c1 <= 6, [5:2]
      v1 = {v0,3'b000} + c1; // 4 bits

      e1 = -{1'b1,d[62:58]} * {1'b1,v1}; // |e1| < 2^7, 5x4 --> 8
      c2 = ($signed({9'b1,v1}) * e1) >>> 8; // [12:8], 4x8
      v2 = $signed({v1,2'b00}) + c2; // 6 bits

      e2 = -{1'b1,d[62:54]} * {1'b1,v2}; // |e2| < 2^11, 9x6 --> 12
      c3 = ($signed({14'b01,v2}) * e2) >> 12; // [18:12], 6x12
      v3 = $signed({v2,4'b0000}) + c3; // 10 bits

      e3 = -{1'b1,d[62:46]} * {1'b1,v3}; // |e3| < 2^19, 17x10 --> 20
      c4 = ($signed({21'b01,v3}) * e3) >> 20; // [30:20], 10x20
      v4 = $signed({2'b01,v3,8'b0}) + c4; // 18 bits

      e4 = -{1'b1,d[62:30]} * {1'b1,v4}; // |e4| < 2^35, 33x18 --> 36
      c5 = ($signed({37'b01,v4}) * e4) >> 36; // [54:36], 18x36
      v5 = $signed({2'b01,v4,16'b0}) + c5; // 34 bits

      e5 = -{1'b1,d}*{1'b1,v5}; // |e5| < 2^65, 63x34 --> 66
      c6 = ($signed({67'b01,v5}) * e5) >> 68; // [100:68], 34x66
      v6 = $signed({2'b01,v5,30'b0}) + c6; // 64 bits

      adj = ~((({65'b1, v6} + 1) * {1'b1, d}) >> 128); // 64x63
   end
   assign v = v6 + adj;
endmodule
