/* Copyright (C) 2022  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Produce carry-out only from an n-bit addition. */
module carry_only(input [n-1:0] a, input [n-1:0] b, input c_in, output c_out);
   parameter n = 64;
   genvar i, j;

   generate
      for (i = 0; n > (1<<i); i = i + 1)
	begin : LOOP
	   localparam m = (n + ((1 << i) - 1)) >> i;
	   localparam k = (m + 1) >> 1;

	   wire [m-1:0] g_in, p_in;
	   wire [k-1:0] g_out, p_out;
	   if (i == 0) begin
	      assign g_in = a & b;
	      assign p_in = a ^ b ;
	   end
	   else begin
	      assign g_in = LOOP[i-1].g_out;
	      assign p_in = LOOP[i-1].p_out;
	   end
	   for (j = 0; j < k; j = j+1) begin
	      if (2*j + 1 < m)
		add_gp combine (g_in[2*j+1], p_in[2*j+1], g_in[2*j], p_in[2*j],
				g_out[j], p_out[j]);
	      else begin
		 assign g_out[j] = g_in[2*j];
		 assign p_out[j] = p_in[2*j];
	      end
	   end
	   if (k == 1)
	     assign c_out = g_out[0] | (p_out[0] & c_in);
	end
   endgenerate
endmodule
