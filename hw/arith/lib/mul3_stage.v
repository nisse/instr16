/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Reduce
        +------------------------+
        |           a0           |
        +------------------------+
        |           a1           |
     +--+---------------------+--+
   + |           a2           |
   --+------------------------+

   to

     +---------------------------+
     |           s0              |
     +------------------------+--+
   + |           s1           |
   --+------------------------+

   using a *parallel* series of full-adders and a half-adder at the
   right-most position, as a trivial Dadda-tree.
 */

module mul3_stage(input [n-1:0] a0, input [n-1:0] a1, input [n:1] a2,
		  output [n:0] s0, output [n:1] s1);
   parameter	n = 64;

   add_n_fa #(n) stage(a0[n-1:0], a1[n-1:0], {a2[n-1:1], 1'b0}, s1[n:1], s0[n-1:0]);
   assign s0[n] = a2[n];

endmodule // sum_3
