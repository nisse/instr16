/* Copyright (C) 2023  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// n parallel full adders.
module add_n_fa(input [n-1:0] a0, input [n-1:0] a1, input [n-1:0] a2,
		output [n:1] s1, output [n-1:0] s0);
   parameter n = 64;
   genvar 		     i;

   generate
      for (i = 0; i < n; i = i + 1)
	assign {s1[i+1], s0[i]} = a0[i] + a1[i] + a2[i];
   endgenerate
endmodule
