/* Copyright (C) 2014  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Combines g and p bits, building block for Kogge-Stone adders and
   related variants. */
module add_gp (input g1, input p1, input g0, input p0,
	       output gout, output pout);
   assign gout = g1 | (p1 & g0);
   assign pout = p1 & p0;
endmodule
