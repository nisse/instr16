/* Copyright (C) 2014, 2015, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [63:0] x;
   reg [63:0] y;
   reg 	      cin;
   wire [64:0] s;
   reg [64:0] r;

   integer     t, c, seed;

   initial begin
      seed = 0;
      for (t = 0; t < 1000; t = t + 1)
	begin
	   x[31: 0] = $random(seed);
	   x[63:32] = $random(seed);
	   y[31: 0] = $random(seed);
	   y[63:32] = $random(seed);

	   for (c = 0; c < 2; c = c + 1) begin
	      #( (x | y | c) ? 10 : 0) begin
		 cin = c;
	      end
	      #10 begin
		 r = x + y + c;

		 if (s !== r) begin
		    $display ("fail: cin = %b", cin);
		    $display ("   x =  %b", x);
		    $display ("   y =  %b", y);
		    $display (" out = %b", s);
		    $display (" ref = %b", r);
		    $finish_and_return(1);
		 end
	      end
	   end // for (c = 0; c < 2; c = c + 1)
	end // for (t = 0; t < 1000; t = t + 1)
   end // initial begin
   add_h64 foo (x, y, cin, s);
endmodule // main
