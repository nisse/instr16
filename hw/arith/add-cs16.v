module add_cs16(input clk, input rst,
		 input [63:0] 	   a_in,
		 input [63:0] 	   b_in,
		 input 		   c_in,
		 output reg [63:0] res,
		 output reg 	   cy);
   reg [63:0] a;
   reg [63:0] b;
   reg 	      c;
   reg [16:0] s0;
   reg [16:0] s10;
   reg [16:0] s11;
   reg [16:0] s20;
   reg [16:0] s21;
   reg [16:0] s30;
   reg [16:0] s31;
   reg        c32, c48;
   
   always @(posedge clk) begin
      if (rst) begin
	 a <= 0;
	 b <= 0;
	 c <= 0;
      end
      else begin
	 a <= a_in;
	 b <= b_in;
	 c <= c_in;
      end
   end

   always @(*) begin
      s0 = {1'b0, a[15:0]} + {1'b0, b[15:0]} + {16'b0, c};
      s10 = {1'b0, a[31:16]} + {1'b0, b[31:16]};
      s11 = {1'b0, a[31:16]} + {1'b0, b[31:16]} + {16'b0, 1};
      s20 = {1'b0, a[47:32]} + {1'b0, b[47:32]};
      s21 = {1'b0, a[47:32]} + {1'b0, b[47:32]} + {16'b0, 1};
      s30 = {1'b0, a[63:48]} + {1'b0, b[63:48]};
      s31 = {1'b0, a[63:48]} + {1'b0, b[63:48]} + {16'b0, 1};
      res[15:0] = s0[15:0];
      {c32, res[31:16]} = s0[16] ? s11 : s10;
      {c48, res[47:32]} = c32 ? s21 : s20;
      {cy, res[63:48]} = c48 ? s31: s30;
   end
endmodule
