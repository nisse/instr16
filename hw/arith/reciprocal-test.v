/* Copyright (C) 2015, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [63:0] d;
   wire [63:0] v;
   reg [128:0] r;

   integer    t, seed;
   initial begin
      seed = 0;
      for (t = 0; t < 10000; t = t + 1)
	begin
	   d[63:48] = $random(seed) % (1 << 16);
	   d[47:32] = $random(seed) % (1 << 16);
	   d[31:16] = $random(seed) % (1 << 16);
	   d[15:0] = $random(seed) % (1 << 16);

	   d[63] = 1;
	   #(t ? 10 : 0) begin
	      r = {1'b1,128'b0} - {1'b1,v}*d;
	      if (r >= d) begin
		 $display ("failed test %d, d = %x, v = %x\n r = %x",
			   t, d, v, r);
		 $display (" v0: %b", recpr.v0);
		 $display (" v1: %b", recpr.v1);
		 $display (" v2: 0x%x", recpr.v2);
		 $display (" v3: 0x%x", recpr.v3);
		 $display (" v4: 0x%x", recpr.v4);
		 $display (" v5: 0x%x", recpr.v5);
		 $display (" v6: 0x%x", recpr.v6);

		 $finish_and_return(1);
	      end
	   end
	end
   end // initial begin

   reciprocal recpr (d[62:0], v);
endmodule
