module main;
   reg [63:0] x;
   reg [63:0] y;
   reg 	      cin;
   reg 	      c64, c60, c5;
   reg [64:0] r64;
   reg [60:0] r60;
   reg [5:0]  r5;

   integer     t, c, seed;

   initial begin
      seed = 0;
      for (t = 0; t < 1000; t = t + 1)
	begin
	   x[31: 0] = $random(seed);
	   x[63:32] = $random(seed);
	   y[31: 0] = $random(seed);
	   y[63:32] = $random(seed);

	   for (c = 0; c < 2; c = c + 1) begin
	      #10 begin
		 cin = c;
	      end
	      #10 begin
		 r64 = x + y + c;
		 r60 = x[59:0] + y[59:0] + c;
		 r5 = x[4:0] + y[4:0] + c;

		 if (c64 !== r64[64]) begin
		    $display ("fail: cin = %b", cin);
		    $display ("   x =  %b", x);
		    $display ("   y =  %b", y);
		    $display (" out = %b", c64);
		    $display (" ref = %b", r64);
		    $finish_and_return(1);
		 end
		 if (c5 !== r5[5]) begin
		    $display ("fail (n = 5): cin = %b", cin);
		    $display ("   x =  %b", x[4:0]);
		    $display ("   y =  %b", y[4:0]);
		    $display (" out = %b", c5);
		    $display (" ref = %b", r5);
		    $display ("g0_i = %b", c5_only.LOOP[0].g_in);
		    $display ("p0_i = %b", c5_only.LOOP[0].p_in);
		    $display ("g1_i = %b", c5_only.LOOP[1].g_in);
		    $display ("p1_i = %b", c5_only.LOOP[1].p_in);
		    $display ("g2_i = %b", c5_only.LOOP[2].g_in);
		    $display ("p2_i = %b", c5_only.LOOP[2].p_in);
		    $finish_and_return(1);
		 end
		 if (c60 !== r60[60]) begin
		    $display ("fail (n = 60): cin = %b", cin);
		    $display ("   x =  %b", x[59:0]);
		    $display ("   y =  %b", y[59:0]);
		    $display (" out = %b", c60);
		    $display (" ref = %b", r60);
		    $finish_and_return(1);
		 end
	      end
	   end
	end
   end
   carry_only #(64)c64_only (x, y, cin, c64);
   carry_only #(60) c60_only (x[59:0], y[59:0], cin, c60);
   carry_only #(5) c5_only (x[4:0], y[4:0], cin, c5);
endmodule // main
