/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [5:0] cnt;
   wire [62:0] out;
   reg [62:0] ref_out;
   integer     i;
   initial
     begin
	for (i = 0; i < 64; i++)
	  begin
	     #(i ? 10 : 0) begin
		cnt = i;
		ref_out = {63{1'b1}} >> (63 - i);
	     end
	     #10 begin
		if (out !== ref_out) begin
		   $display("genmask fail: in: %d, out %b, ref: %b",
			    cnt, out, ref_out);

		   $finish_and_return(1);
		end
	     end
	  end // for (i = 0; i < 64; i++)
     end // initial begin
   genmask #(6) test_genmask (cnt, out);
endmodule
