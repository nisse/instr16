module add_with_h64(input clk, input rst,
		 input [63:0] 	   a_in,
		 input [63:0] 	   b_in,
		 input 		   c_in,
		 output reg [63:0] res,
		 output reg 	   cy);
   reg [63:0] a;
   reg [63:0] b;
   reg 	      c;

   always @(posedge clk) begin
      if (rst) begin
	 a <= 0;
	 b <= 0;
	 c <= 0;
      end
      else begin
	 a <= a_in;
	 b <= b_in;
	 c <= c_in;
      end
   end

   add_h64 add(a, b, c, {cy, res});
endmodule
