/* Copyright (C) 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [2:0] x;
   wire [1:0] s;
   integer     i;
   
   initial
     begin
	for (i = 0; i < 8; i = i + 1) begin
	   #10 x = i;
	   #10 begin
	      if (s != 2'b00 + x[0] + x[1] + x[2]) begin
		 $display ("fail: in: %b, out: %d",
			       x, s);
		 $finish_and_return(1);
	      end
	   end
	end
     end

   add_fa foo (x[0], x[1], x[2], s[0], s[1]);
endmodule
