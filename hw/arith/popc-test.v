/* Copyright (C) 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [63:0] x;
   wire [6:0] cnt;
   integer    i, j;
   reg [6:0] ref_popc;

   initial
     begin
	for (i = 0; i <= 64; i = i + 1)
	  for (j = i; j <= 64; j = j + 1) begin
	     #((i | j) ? 10 : 0) begin
		if (i == j && i > 0) begin
		   x = {64{1'b1}};
		   ref_popc = 7'd64;
		end
		else begin
		   x = (64'b1 << j) - (64'b1 << i);
		   ref_popc = j - i;
		end
	     end
	     #10 begin
		if (ref_popc !== cnt) begin
		   $display ("popc fail: in: %x, out: %b, ref: %b",
			     x, cnt, ref_popc);
		   $finish_and_return(1);
		end
	     end
	  end // for (j = i; j <= 64; j = j + 1)
     end
   popc test_popc(x, cnt);
endmodule
