/* Copyright (C) 2017, 2021  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [62:0] a0;
   reg [62:0] a1;
   reg [63:0] a2;
   reg [63:0] a3;
   reg [63:0] a4;
   reg [63:0] a5;
   reg [63:0] a6;
   reg [63:0] a7;
   reg [63:0] a8;
   reg [63:0] a9;
   reg [63:0] a10;
   reg [63:0] a11;
   reg [63:0] a12;
   wire [73:0] s0;
   wire [73:5] s1;

   reg [74:0] res_1;
   reg [74:0] res_2;

   integer    t, seed;
   initial begin
      seed = 0;
      for (t = 0; t < 1000; t = t + 1)
	begin
	   if (t < 100) begin
	      a0 = {63'b1} << ($random(seed) & 63);
	      a1 = {63'b1} << ($random(seed) & 63);
	      a2 = {64'b1} << ($random(seed) & 63);
	      a3 = {64'b1} << ($random(seed) & 63);
	      a4 = {64'b1} << ($random(seed) & 63);
	      a5 = {64'b1} << ($random(seed) & 63);
	      a6 = {64'b1} << ($random(seed) & 63);
	      a7 = {64'b1} << ($random(seed) & 63);
	      a8 = {64'b1} << ($random(seed) & 63);
	      a9 = {64'b1} << ($random(seed) & 63);
	      a10 = {64'b1} << ($random(seed) & 63);
	      a11 = {64'b1} << ($random(seed) & 63);
	      a12 = {64'b1} << ($random(seed) & 63);
	   end
	   else begin
	      a0[31: 0] = $random(seed);
	      a0[62:32] = $random(seed);
	      a1[31: 0] = $random(seed);
	      a1[62:32] = $random(seed);
	      a2[31: 0] = $random(seed);
	      a2[63:32] = $random(seed);
	      a3[31: 0] = $random(seed);
	      a3[63:32] = $random(seed);
	      a4[31: 0] = $random(seed);
	      a4[63:32] = $random(seed);
	      a5[31: 0] = $random(seed);
	      a5[63:32] = $random(seed);
	      a6[31: 0] = $random(seed);
	      a6[63:32] = $random(seed);
	      a7[31: 0] = $random(seed);
	      a7[63:32] = $random(seed);
	      a8[31: 0] = $random(seed);
	      a8[63:32] = $random(seed);
	      a9[31: 0] = $random(seed);
	      a9[63:32] = $random(seed);
	      a10[31: 0] = $random(seed);
	      a10[63:32] = $random(seed);
	      a11[31: 0] = $random(seed);
	      a11[63:32] = $random(seed);
	      a12[31: 0] = $random(seed);
	      a12[63:32] = $random(seed);
	   end
	   #10 begin
	      res_1 = {12'b0, a0} + {12'b0, a1} +
		      {11'b0, a2}+ {10'b0, a3, 1'b0} +
		      {9'b0, a4, 2'b0} + {8'b0, a5, 3'b0} +
		      {7'b0, a6, 4'b0} + {6'b0, a7, 5'b0} +
	              {5'b0, a8, 6'b0} + {4'b0, a9, 7'b0} +
	              {3'b0, a10, 8'b0} + {2'b0, a11, 9'b0} +
		      {1'b0, a12, 10'b0};

	      res_2 = {1'b0, s0} + {1'b0, s1, 5'b0};

	      if (res_1 !== res_2) begin
		 $display ("fail: in: %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x",
			   a0, a1, a2,
			   {a3, 1'b0}, {a4, 2'b0}, {a5, 3'b0},
			   {a6, 4'b0}, {a7, 5'b0}, {a8, 6'b0},
			   {a9, 7'b0}, {a10, 8'b0}, {a11, 9'b0},
			   {a12, 10'b0});
		 $display ("  out: %b, %b", s0, s1);
		 $display ("  a0 + a1 + a2 + 2 a3 + 4 a4 + 8 a5 + 16 a6 + 32 a7 + ... = %b", res_1);
		 $display ("  s0 + 32 s1      = %b", res_2);
		 $finish_and_return(1);
	      end
	   end
	end
   end // initial begin
   mul13_stage mul (a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, s0, s1);
endmodule // main
