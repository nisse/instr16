module add_cs32(input clk, input rst,
		 input [63:0] 	   a_in,
		 input [63:0] 	   b_in,
		 input 		   c_in,
		 output reg [63:0] res,
		 output reg 	   cy);
   reg [63:0] a;
   reg [63:0] b;
   reg 	      c;
   reg [32:0] s0;
   reg [32:0] s10;
   reg [32:0] s11;
   
   always @(posedge clk) begin
      if (rst) begin
	 a <= 0;
	 b <= 0;
	 c <= 0;
      end
      else begin
	 a <= a_in;
	 b <= b_in;
	 c <= c_in;
      end
   end

   always @(*) begin
      s0 = {1'b0, a[31:0]} + {1'b0, b[31:0]} + {32'b0, c};
      s10 = {1'b0, a[63:32]} + {1'b0, b[63:32]};
      s11 = {1'b0, a[63:32]} + {1'b0, b[63:32]} + {32'b0, 1};
      res[31:0] = s0[31:0];
      {cy, res[63:32]} = s0[32] ? s11: s10;
   end
endmodule
