/* Copyright (C) 2014, 2015, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;   
   reg [3:0] x;
   reg [3:0] y;
   reg 	     cin;
   wire [4:0] s;

   integer   i, j, c;
  
   initial
     begin
	for (i = 0; i < 16; i = i + 1)
	  for (j = 0; j < 16; j = j + 1)
	    for (c = 0; c < 2; c = c + 1)
	      begin
		 #( (i | j | c) ? 10 : 0) begin
		    x = i;
		    y = j;		 
		    cin = c;
		 end
		 #10 begin
		    if (s !== i + j + c) begin
		       $display ("fail: in: %d, %d, %d, out: %d",
				 x, y, cin, s);
		       $finish_and_return(1);
		    end
		 end
	      end
     end // initial begin
   add_bk4 foo (x, y, cin, s);
endmodule
