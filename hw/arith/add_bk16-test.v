/* Copyright (C) 2014, 2015, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [15:0] x;
   reg [15:0] y;
   reg 	      cin;
   wire [16:0] s;
   reg [16:0] r;

   integer     t, c, seed;

   initial begin
      seed = 0;
      for (t = 0; t < 10090; t = t + 1)
	begin
	   x = $random(seed) % (1 << 16);
	   y = $random(seed) % (1 << 16);

	   // $display("x %d, y %d, seed %d", x, y, seed);

	   for (c = 0; c < 2; c = c + 1) begin
	      #( (x | y | c) ? 10 : 0) begin
		 cin = c;
	      end
	      #10 begin
		 r = x + y + c;

		 if (s !== r) begin
		    $display ("fail: in: %b, %b, %1d, out: %b, expected: %b",
			      x, y, c, s, r);
		    $finish_and_return(1);
		 end
	      end
	   end // for (c = 0; c < 2; c = c + 1)
	end // for (t = 0; t < 1000; t = t + 1)
   end // initial begin
   add_bk16 foo (x, y, cin, s);
endmodule // main
