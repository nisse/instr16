/* Copyright (C) 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Unsigned multiply, 8x8 -> 16 bits */
module umul8(input [7:0] a, input [7:0] b, output [15:0] p);
   reg [7:0]  a0;
   reg [8:1]  a1;
   reg [9:2]  a2;
   reg [10:3] a3;
   reg [11:4] a4;
   reg [12:5] a5;
   reg [13:6] a6;
   reg [14:7] a7;

   wire [13:2] b0;
   wire [13:2] b1;
   wire [11:3] b2;
   wire [10:5] b3;

   wire [14:3] c0;
   wire [12:3] c1;
   wire [12:5] c2;
   wire [8:7]  c3;

   wire [14:4] d0;
   wire [14:6] d1;
   wire [9:7]  d2;

   wire [15:5] e0; /* e0[7] unused */
   wire [14:5] e1;

   wire [5:0] dummy;

   always @(*) begin
      a0 = a & {8{b[0]}};
      a1 = a & {8{b[1]}};
      a2 = a & {8{b[2]}};
      a3 = a & {8{b[3]}};
      a4 = a & {8{b[4]}};
      a5 = a & {8{b[5]}};
      a6 = a & {8{b[6]}};
      a7 = a & {8{b[7]}};
   end
   /* Dadda tree

      Notation: 
    
        F: Full adder
        H: Half adder    
        1-2: Signals copied through
    
      1 1 1 1 1  bits
      4 3 2 1 0 9 8 7 6 5 4 3 2 1 0

      1 2 3 4 5 6 7 8 7 6 5 4 3 2 1 (Sum 64)
          F F F F F F F F F F F H
            1 2 F F F F F H 1
                  1 2 1

      1 3 2 3 5 4 5 6 5 4 3 3 2 1 1 (Sum 48)
        F   F F F F F F F F F H
              H 1 2 F H 1

      2 1 3 3 3 3 5 4 3 3 2 2 1 1 1 (Sum 37)
          F F F F F F F F   H
                  H 1 

      2 2 2 2 2 3 3 3 2 1 3 1 1 1 1 (Sum 29)
      H H H H H F F F     F

    1 2 2 2 2 2 2 2 1 2 2 1 1 1 1 1 (Sum 25)
    
      And a final 11-bit adder.
    */

   /*
      . . x . . x . .               a7
        . x x . x x . .             a6
          x x x x x x x x           a5
            x x x x x x x x         a4
              x x x x x x x .       a3
                x x x x x x x x     a2
                  x x x x x x x x   a1
                    x x x x x x x . a0
    */
   /* Stage 0 */
   assign p[0] = a0[0];
   add_ha s0_1 ( a0[1],  a1[1],           p[1],  b0[2]);
   add_fa s0_2 ( a0[2],  a1[2],  a2[2],  b1[2],  b0[3]);
   add_fa s0_3 ( a0[3],  a1[3],  a2[3],  b1[3],  b0[4]); /* a3[3] left */
   add_fa s0_4a( a0[4],  a1[4],  a2[4],  b1[4],  b0[5]); /* new */
   add_ha s0_4b( a3[4],  a4[4],          b2[4],  b3[5]);
   add_fa s0_5a( a0[5],  a1[5],  a2[5],  b1[5],  b0[6]);
   add_fa s0_5b( a3[5],  a4[5],  a5[5],  b2[5],  b3[6]);
   add_fa s0_6a( a0[6],  a1[6],  a2[6],  b1[6],  b0[7]);
   add_fa s0_6b( a3[6],  a4[6],  a5[6],  b2[6],  b3[7]); /* a6[6] */
   add_fa s0_7a( a0[7],  a1[7],  a2[7],  b1[7],  b0[8]);
   add_fa s0_7b( a3[7],  a4[7],  a5[7],  b2[7],  b3[8]); /* a6[7], a7[7] */
   add_fa s0_8a( a1[8],  a2[8],  a3[8],  b1[8],  b0[9]);
   add_fa s0_8b( a4[8],  a5[8],  a6[8],  b2[8],  b3[9]); /* a7[8] */
   add_fa s0_9a( a2[9],  a3[9],  a4[9],  b1[9], b0[10]);
   add_fa s0_9b( a5[9],  a6[9],  a7[9],  b2[9], b2[10]);
   add_fa s0_10(a3[10], a4[10], a5[10], b1[10], b0[11]); /* a6[10], a7[10] */
   add_fa s0_11(a4[11], a5[11], a6[11], b1[11], b0[12]); /* a7[11] */
   add_fa s0_12(a5[12], a6[12], a7[12], b1[12], b0[13]);

   assign b2[3] = a3[3];
   assign b1[13] = a6[13];
   assign b3[10] = a7[10];
   assign b2[11] = a7[11];

   /*
      1 1 1 1 1  bits
      4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
      . x         x x               a7
              x     x x             a6
              x x x x x x           b3 10:5
            x x x x x x x x x       b2 11:3
        x . x x x . x x x x x x     b1 13:2
        x . x x . . x x . x x x     b0 13:2
 
    */ 
   /* Stage 1 */
   add_ha s1_2 ( b0[2],  b1[2],           p[2],  c0[3]);
   add_fa s1_3 ( b0[3],  b1[3],  b2[3],  c1[3],  c0[4]);
   add_fa s1_4 ( b0[4],  b1[4],  b2[4],  c1[4],  c0[5]);
   add_fa s1_5 ( b1[5],  b2[5],  b3[5],  c1[5],  c0[6]); /* b0[5] */
   add_fa s1_6a( b0[6],  b1[6],  b2[6],  c1[6],  c0[7]);
   add_ha s1_6b( b3[6],  a6[6],          c2[6],  c3[7]);
   add_fa s1_7a( b0[7],  b1[7],  b2[7],  c1[7],  c0[8]);
   add_fa s1_7b( b3[7],  a6[7],  a7[7],  c2[7],  c3[8]);
   add_fa s1_8 ( b2[8],  b3[8],  a7[8],  c1[8],  c0[9]); /* b0[8], b1[8] */
   add_fa s1_9 ( b1[9],  b2[9],  b3[9],  c1[9], c0[10]); /* b0[9] */
   add_fa s1_10a(b0[10], b1[10], b2[10],c1[10], c0[11]);
   add_ha s1_10b(b3[10], a6[10],        c2[10], c2[11]);
   add_fa s1_11(b0[11], b1[11], b2[11], c1[11], c0[12]);
   add_fa s1_13(b0[13], b1[13], a7[13], c0[13], c0[14]);

   assign c2[5] = b0[5];
   assign c2[9:8] = b0[9:8];
   assign c1[12] = b0[12];
   assign c2[12] = b1[12];
   
   /*
      1 1 1 1 1  bits
      4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
      .                             a7
                  x                 b1
                  x x               c3
          x x x x x x x x           c2 12:5
          x x x x x x x x . x       c1 12:3
      . . x x x x x . x x . x       c0 14:3
    */
   /* Stage 2 */
   add_ha s2_3 ( c0[3],  c1[3],           p[3],  d0[4]);
   add_fa s2_5 ( c0[5],  c1[5],  c2[5],  d0[5],  d1[6]);
   add_fa s2_6 ( c0[6],  c1[6],  c2[6],  d0[6],  d1[7]);
   add_fa s2_7 ( c1[7],  c2[7],  c3[7],  d0[7],  d1[8]); /* c0[7] */
   add_fa s2_8a( c0[8],  c1[8],  c2[8],  d0[8],  d1[9]);
   add_ha s2_8b( c3[8],  b1[8],          d2[8],  d2[9]);
   add_fa s2_9 ( c0[9],  c1[9],  c2[9],  d0[9], d1[10]);
   add_fa s2_10(c0[10], c1[10], c2[10], d0[10], d1[11]);
   add_fa s2_11(c0[11], c1[11], c2[11], d0[11], d1[12]);
   add_fa s2_12(c0[12], c1[12], b1[12], d0[12], d1[13]);   

   assign d2[7] = c0[7];
   assign d0[14:13] = c0[14:13];
   assign d1[14] = a7[14];
   
   /*
      1 1 1 1 1  bits
      4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
                          x         c1
                          x         c0
                x x x               d2 9:7
      x x x x x x x x .             d1 14:6
      x x x x x x x x . . x         d0 14:4
    */
   /* Stage 3 */
   add_fa s3_4 ( d0[4],  c0[4],  c1[4],   p[4],  e0[5]);
   add_fa s3_7 ( d0[7],  d1[7],  d2[7],  e1[7],  e0[8]);
   add_fa s3_8 ( d0[8],  d1[8],  d2[8],  e1[8],  e0[9]);
   add_fa s3_9 ( d0[9],  d1[9],  d2[9],  e1[9], e0[10]);
   add_ha s3_10(d0[10], d1[10],         e1[10], e0[11]);
   add_ha s3_11(d0[11], d1[11],         e1[11], e0[12]);
   add_ha s3_12(d0[12], d1[12],         e1[12], e0[13]);
   add_ha s3_13(d0[13], d1[13],         e1[13], e0[14]);
   add_ha s3_14(d0[14], d1[14],         e1[14], e0[15]);

   assign e1[6:5] = d0[6:5];
   assign e0[6] = d1[6];
     
   /*
      1 1 1 1 1  bits
      4 3 2 1 0 9 8 7 6 5 4 3 2 1 0


      x x x x x x x x x x           e1 14:5
    x x x x x x x x   x x           e0 15:8,6:5
    */


   /* Final 11-bit add */
   add_bk16 add ({e0[15:8], 1'b0, e0[6:5], 5'b0},
		 {1'b0, e1[14:5], 5'b0}, 1'b0,
		 {dummy[5], p[15:5], dummy[4:0]});
endmodule
