/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [63:0] a0;
   reg [63:0] a1;
   reg [63:0] a2;
   reg [63:0] a3;
   wire [65:0] s0;
   wire [65:2] s1;

   reg [66:0] res_1;
   reg [66:0] res_2;

   integer    t, seed;
   initial begin
      seed = 0;
      for (t = 0; t < 1000; t = t + 1)
	begin
	   if (t < 100) begin
	      a0 = {64'b1} << ($random(seed) & 63);
	      a1 = {64'b1} << ($random(seed) & 63);
	      a2 = {64'b1} << ($random(seed) & 63);
	      a3 = {64'b1} << ($random(seed) & 63);
	   end
	   else begin
	      a0[31: 0] = $random(seed);
	      a0[63:32] = $random(seed);
	      a1[31: 0] = $random(seed);
	      a1[63:32] = $random(seed);
	      a2[31: 0] = $random(seed);
	      a2[63:32] = $random(seed);
	      a3[31: 0] = $random(seed);
	      a3[63:32] = $random(seed);
	   end
	   #10 begin
	      res_1 = {3'b0, a0} + {3'b0, a1} +
		      {2'b0, a2, 1'b0}+ {1'b0, a3, 2'b0};
	      res_2 = {1'b0, s0} + {1'b0, s1, 2'b0};
	      if (res_1 !== res_2) begin
		 $display ("fail: in: %x, %x, %x, %x",
			   a0, a1, {a2, 1'b0}, {a3, 2'b0});
		 $display ("  out: %x, %x", s0, {1'b0,s1});
		 $display ("  a0 + a1 + 2 a2 + 4 a3= %b", res_1);
		 $display ("  s0 + 2 s1      = %b", res_2);
		 $finish_and_return(1);
	      end
	   end
	end
   end // initial begin
   mul4_stage mul (a0, a1, a2, a3, s0, s1);
endmodule // main
