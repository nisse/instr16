/* Copyright (C) 2014, 2015, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static void
dadda_tree (unsigned n, unsigned *a)
{
  unsigned count_ha, count_fa;

  count_ha = count_fa = 0;

  for (;;)
    {
      unsigned i;
      unsigned m = 0;
      unsigned out;
      bool final;
      bool lsb;
      for (i = n; i-- > 0; )
	{
	  printf (" %2d", a[i]);
	  if (m < a[i])
	    m = a[i];
	}
      printf ("\n");
      if (m <= 2)
	break;
      final = (m == 3);

      for (i = out = 0, lsb = true; i < n; i++)
	{
	  unsigned fa = a[i] / 3;
	  unsigned copy = a[i] - 3 * fa;
	  if (a[i] == 1)
	    {
	      a[i] += out;
	      out = 0;

	      /* Leave lsb flag unchanged */
	      continue;
	    }
	  if (lsb && a[i] == 2)
	    {
	      assert (out == 0);
	      /* Apply half adder at lsb end */
	      a[i] = 1;
	      out = 1;
	      count_ha++;
	    }
	  else if (copy == 2 && ((fa + out) % 3) >= (final ? 1 : 2))
	    {
	      count_ha++;
	      a[i] = fa + out + 1;
	      out = fa + 1;
	    }
	  else
	    {
	      a[i] = copy + fa + out;
	      out = fa;
	    }
	  count_fa += fa;
	  lsb = false;
	}
      if (out)
	{
	  fprintf (stderr, "Addition overflows\n");
	  exit (EXIT_FAILURE);
	}
    }
  printf ("Total: %u full adders, %u half adders.\n",
	  count_fa, count_ha);
}

int
main (int argc, char **argv)
{
  unsigned *a;
  unsigned n;
  unsigned i;
  unsigned add;
  if (argc < 2)
    {
      fprintf (stderr, "Usage: %s n\n", argv[0]);
      return EXIT_FAILURE;
    }
  n = atoi (argv[1]);
  if (n < 4 || n > 500)
    {
      fprintf (stderr, "%s: n out of range\n", argv[0]);
      return EXIT_FAILURE;
    }

  if (argc > 2)
    {
      add = atoi(argv[2]);
      if (add < 0 || add > n)
	{
	  fprintf (stderr, "%s: a out of range\n", argv[0]);
	  return EXIT_FAILURE;	  
	}
    }
  else
    add = 0;
  a = malloc (2*n * sizeof(*a));
  if (!a)
    {
      fprintf (stderr, "%s: Memory exhausted.\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  for (i = 0; i < n; i++)
    {
      a[i] = i+1 + (i < add);
      a[2*n-1-i] = i;
    }
  dadda_tree (2*n, a);

  return EXIT_SUCCESS;
}
