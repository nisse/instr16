/* Copyright (C) 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module main;
   reg [63:0] x;
   reg [63:0] y;
   wire [127:0] res;
   wire [127:0] ref_res;
   reg 	      clk;
   reg 	      rst;
   reg 	      enable;
   reg 	      start;
   wire       pending;
   wire       ready;

   integer    t, seed;
   initial begin
      seed = 0;
      t = 0;

      clk = 0;
      rst = 1;
      enable = 0;
      start = 0;
   end
   always
     #10 begin
	clk <= !clk;
     end // always #10
   always @(posedge clk) begin
      if (rst) begin
	 rst <= 0;
	 start <= 1;
      end
      else
	start <= 0;
      if (ready && res !== ref_res) begin
	 $display("fail: in: %x, %x", x, y);
	 $display("     out: %b", res);
	 $display("     ref: %b", ref_res);
	 $finish_and_return(1);
      end
      if (start || ready) begin
	 if (t < 10) begin
	    x <= 1;
	    y <= {64'b1} << t;
	 end
	 else if (t < 20) begin
	    x <= {64'b1} << (t - 10);
	    y <= 1;
	 end
	 else if (t < 50) begin
	    x <= {64'b1} << ($random(seed) & 63);
	    y <= {64'b1} << ($random(seed) & 63);
	 end
	 else if (t == 50) begin
	    x <= {64{1'b1}};
	    y <= {64{1'b1}};
	 end
	 else if (t < 300) begin
	    x[31: 0] <= $random(seed);
	    x[63:32] <= $random(seed);
	    y[31: 0] <= $random(seed);
	    y[63:32] <= $random(seed);
	 end
	 else
	   $finish;
	 t <= t + 1;

	 enable <= 1;
      end
      else
	enable <= 0;
   end // always @ (posedge clk)

   assign ref_res = {64'b0, x} * {64'b0, y};
   `MUL64_MODULE mul (clk, rst, enable, x, y, pending, ready, res);
endmodule
