// Warmup example, blinking 4 of the leds.
module top (input clk, output reg [9:2] d);
   // Count to 23 * 2^18, appr. 0.5s of the 12 MHz input.
   reg [17:0] count_hi;
   reg [5:0] count_lo;

   initial begin
      count_hi <= 0;
      count_lo <= 0;
      d <= 0;
   end

   always @(posedge clk) begin
      count_hi <= count_hi + 1;
      if (&count_hi) begin
	 if (count_lo == 22) begin
	    count_lo <= 0;
	    d <= d + 1;
	 end
	 else
	   count_lo <= count_lo + 1;
      end
   end
endmodule
