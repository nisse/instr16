\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}

\author{Niels Möller}
\title{Hardware reciprocal}
\date{}

\frenchspacing

\begin{document}

\section{Introduction}

These notes are an extension to the paper ``Improved division by
invariant integers'', targeting hardware implementation.

The basic Newton iteration for computing $x = 1/D$ is
\begin{equation*}
  x' = x + x (1 - x D) 
\end{equation*}
If we put $V = 2^{2n} x$, this get an integer iteration
\begin{equation*}
  V' = V + \lfloor 2^{-2n} V (2^{2n} - V D) \rfloor
\end{equation*}
We will assume that $D$ is normalized, $2^{n-1} \leq D < 2^n$.

Define the error $E = 2^{2n} - V D$, we then get
\begin{equation*}
  E' = 2^{-2n} E^2 + \xi D
\end{equation*}
where $0 \leq \xi < 1$ is the truncaton from the floor expression.

\section{Final adjustment}

The objective is to find the reciprocal $V = \lfloor (2^{2n} - 1)/D
\rfloor$, which implies that the corresponding error $E$ is in the
range $1 \leq E \leq d$. We will need a final adjustment step, but to
have an error in $V$ of at most one, we must have $E$ in he larger
range $1 \leq E \leq 2D$. To guarantee that $E' \leq 2D$, we need
$2^{-2n} E^2 \leq D$, or
\begin{equation*}
  |E| \leq 2^n \sqrt{D}
\end{equation*}
Substituting the smallest $D$, $2^{n-1}$, we get the requirement
\begin{equation*}
  |E| \leq 2^{n + (n-1)/2}
\end{equation*}

\section{Truncating $v$}

When iterating, we will not use more bits to represent $V$ than
corresponds to the accuracy. For the final iteration, we still need
all bits of $D$. So let $V = 2^\ell v$. Substituting into the error
expression gives
\begin{equation*}
  E = 2^{2n} - V D = 2^\ell (2^{2n - \ell} - v D)
\end{equation*}
Define $e = 2^{2n - \ell} - v D$, so that $E = 2^\ell e$. The
iteration becomes
\begin{equation*}
  V' = 2^\ell v + \lfloor 2^{-2(n-\ell)} v e \rfloor
\end{equation*}
and the new error
\begin{equation*}
  E' = 2^{-2(n - \ell)} e^2 + \xi D
\end{equation*}
Again, we need this smaller than $2D$, so we need
\begin{equation*}
  |e| < 2^{n-\ell} \sqrt{D}
\end{equation*}
which is true for any $D$ if we require
\begin{equation*}
  |e| < 2^{n - \ell + (n-1)/2}
\end{equation*}
On the other hand, $e$ also depends on $\ell$. We get small $e$ if
$v \approx 2^{2n - \ell} / D$, and then $e$ is on the same order
as $D$. If we require that $|e| < 2^{n+1}$, we have a little margin.
This gives the sufficient condition $n+1 \leq n - \ell + (n-1)/2$, or
\begin{equation*}
  \ell \leq (n-3)/2
\end{equation*}
E.g., if $n = 64$, we need $\ell \leq 30$, so $v_1$ must be 34 bits.

\section{Truncating $d$}

For the iterations before the final one, we will also ignore low bits
of $D$, when they don't affect the significant bits of $V$. So assume
that input $V$ is truncated to $n-\ell$ bits and output $V'$ to $n -
\ell'$ bits, with $\ell' < \ell$.
\begin{align*}
  V &= 2^\ell v & V' &= 2^{\ell'} v' & D &= 2^j (d + \delta)
\end{align*}
where $\delta$ represents the low, ignored, bits. Substituting in the
error expression gives
\begin{equation*}
  2^{2n} - V D = 2^{j + \ell} \left(2^{2n - j - \ell} - v (d + \delta)\right)
\end{equation*}
Define
\begin{equation*}
  e = 2^{2n - j - \ell} - v d
\end{equation*}
and use this for the modified iteration,
\begin{equation*}
  v' = 2^{\ell-\ell'} v + \lfloor 2^{-(2n + \ell'- \ell - j)} e v \rfloor
\end{equation*}
First consider the error with respect to $d$,
\begin{equation*}
  e' = 2^{2n -j - \ell'} - v' d = 2^{-(2n + \ell' - 2 \ell - j)} e^2 + \xi d
\end{equation*}
The error with respect to $D$ is
\begin{align*}
  2^{2n - \ell'} - v'D &= 2^j e' - 2^j v' \delta \\
  &= 2^{-(2n + \ell' - 2 \ell - 2j)} e^2 + 2^j \xi d - 2^j v' \delta
\end{align*}

\section{Backtracking the iterations}

For the lower bound on the error (the negative term related to the $d$
truncation error $\delta$), we get $2^j v' \delta < 2^{j + n - \ell' +
  1}$. We need to bound it by $2^{n+1}$, so choose $j = \ell'$. Then
the first term reduces to
\begin{equation*}
  2^{-(2n - 2\ell - \ell')} e^2
\end{equation*}
Furthermore, we require that the input error is bounded by $|e| <
2^{n+1-j} = 2^{n+1-\ell'}$, which gives us
\begin{equation*}
  2^{-(2n - 2 \ell - \ell') + 2 (n+1 - \ell')} = 2^{2 \ell - \ell' + 2}  
\end{equation*}
To get the total error bounded by $2^{n+1}$, we bound this term by
$2^n$, which requires that 
\begin{equation*}
  \ell \leq (n + \ell')/2 - 1  
\end{equation*}

We now consider the case of 64-bit words, $n = 64$. Consider the next
to final iteration. For the output, we have seen that we can choose
$\ell' = 30$. By the above equations we get $j = 30$ and $\ell = 46$.
I.e., we need as inputs 34 bits of $d$, and 18 bits $v$.

Going back one step more, we reduce to $n = 34$, since the low bits of
$d$ aren't involved at all. So we use an output $\ell' = 46 - 30 =
16$. We then get $j = 16$ and $\ell = 24$. I.e., we need 18 bits of
$d$, and 10 bits of $v$.

Next, we have $n = 18$, $\ell' = 24 - 16 = 8$. We get $j = 8$ and $\ell
= 12$. So we need 10 bits of $d$ and 6 bits of $v$.

To get there, put $n = 10$, $\ell' = 4$. We get $j = 4$ and $\ell =
6$. I.e., we need 6 bits of $d$ and 4 bits of $v$. Which is in the
range for a table lookup on 5 $d$-bits.

If we go back one more step, we have $n = 6$, $\ell' = 2$. We get $j =
2$ and $\ell = 3$. So we need 4 bits of $d$ and 3 bits of $v$. Which
clearly is in the range of a table lookup.

But we could perhaps do slightly better. For the table lookup, we
could get an error $|e| \leq d/2 < 2^{n-j -1}$, rather than $|e| <
2^{n - j + 1}$ as we have used above. And use $\ell \leq (n + \ell')/2
+ 1$. This means that to get to $n = 10$, $\ell' = 4$, we can use $j =
4$ and $\ell = 8$. I.e, we need 6 bits of $d$ and 2 bits of $v$. Maybe
some cheating is possible.

Or for $n = 6$, $\ell' = 2$, we could use $j = 2$ and $\ell = 5$, so
we start the iteration with 4 bits of $d$ and only a single bit of
$v$, which is going to be a boolean function on three inputs.

\section{Algorithm}

Try the following iteration. Input: $d$, of $n$ bits, and an
approximation $v$ of $\ell$ bits (not counting the leading 1), and an
output $v'$ of $\ell'$ bits. We keep track of errors with respect to
$d$.

\begin{align*}
  e &= 2^{n + \ell} - v d \\
  v' &= 2^{\ell' - \ell} v + \lfloor 2^{-(n + 2\ell - \ell')} e v
  \rfloor \\
  &= 2^{\ell' - \ell} \left(v + 2^{-(n+\ell)} e v\right) - \xi \\ 
  e' &= 2^{n + \ell'} - v' d
\end{align*}
We get
\begin{align*}
  e' &= 2^{n + \ell'} - 2^{\ell' - \ell} vd (1 + 2^{-(n+\ell)} e) +
  \xi d \\
  &= 2^{n + \ell'} - 2^{\ell' - \ell} (2^{n + \ell} - e)(1 +
  2^{-(n+\ell)} e) + \xi d \\
  &= 2^{n + \ell'} - 2^{\ell' - \ell} (2^{n + \ell} - 2^{-(n+\ell)}
  e^2) + \xi d \\
  &= 2^{\ell' -n - 2\ell} e^2 + \xi d
\end{align*}
If we allow signed errors, of size up to $|e'| \leq 2^{n+1}$, we can
use $n = \ell'$. The formulas then simplify to
\begin{align*}
  e &= 2^{n + \ell} - vd \\
  v' &= 2^{n - \ell} \lfloor 2^{-2\ell} e v \rfloor \\
  e' &= 2^{2n} - v'd \\
  &= 2^{-2\ell} e^2 + \xi d 
\end{align*}

\begin{itemize}
\item 1-bit $v_0$, 4-bit of $d$.
  \begin{align*}
    d_0 &= \lfloor 2^{-60} d \rfloor \\
    v_0 &= 2 + [d_0 < 13] \\
    e_0 &= 2^5 - d_0 v_0
  \end{align*}
  Then $-4 \leq e_0 \leq 8$, or $|e_0| \leq 8$.
\item 4-bit $v_1$: $\ell = 1$, $n = 4$.
  \begin{align*}
    d_1 &= d_0 = \lfloor 2^{-60} d \rfloor \\
    v_1 &= 2^3 v_0 + \lfloor 2^{-2} d_1 e_0 \rfloor \\
    e_1 &= 2^{8} - v_1 d_1 \leq 2^{-2} e_0^2 + \xi d < 2^5
  \end{align*}
\item 6-bit $v_2$: $\ell = 4$, $n = 6$.
  \begin{align*}
    d_2 &= \lfloor 2^{-58} d \rfloor \\
    e'_1 &= 2^{10} - v_1 d_2 = 2^2 (e_1 - v_1 \epsilon) \\
    v_2 &= 2^2 v_1 + \lfloor 2^{-8} d_2 e'_1 \rfloor \\
    e_2 &= 2^{12} - v_2 d_2 < 2^{-8} e'^2_1 + \xi d_2 \\
    &< 2^{14-8} + 2^{6} = 2^7
  \end{align*}

\item 10-bit $v_3$: $\ell = 6$, $n = 10$.
  \begin{align*}
    d_3 &= \lfloor 2^{-54} d \rfloor \\
    e'_2 &= 2^{16} - v_2 d_3 = 2^4 (e_2 - v_2 \epsilon) \\
    v_3 &= 2^4 v_2 + \lfloor 2^{-12} d_3 e'_2 \rfloor \\
    e_3 &= 2^{20} - v_3 d_3 < 2^{-12} e'^2_2 + \xi d_3 \\
    &< 2^{22-12} + 2^{10} = 2^{11}
  \end{align*}

\item 18-bit $v_4$: $\ell = 10$, $n = 18$.
  \begin{align*}
    d_4 &= \lfloor 2^{-46} d \rfloor \\
    e'_3 &= 2^{28} - v_3 d_4 = 2^8 (e_3 - v_3 \epsilon) \\
    v_4 &= 2^8 v_3 + \lfloor 2^{-20} d_4 e'_3 \rfloor \\
    e_4 &= 2^{36} - v_4 d_4 < 2^{-20} e'^2_3 + \xi d_4 \\
    &< 2^{38-20} + 2^{18} = 2^{19}
  \end{align*}

\item 34-bit $v_5$: $\ell = 18$, $n = 34$.
  \begin{align*}
    d_5 &= \lfloor 2^{-30} d \rfloor \\
    e'_4 &= 2^{52} - v_4 d_5 = 2^{16} (e_4 - v_4 \epsilon) \\
    v_5 &= 2^{16} v_4 + \lfloor 2^{-36} d_5 e'_4 \rfloor \\
    e_5 &= 2^{68} - v_5 d_5 < 2^{-36} e'^2_4 + \xi d_5 \\
    &< 2^{70-36} + 2^{34} = 2^{35}
  \end{align*}

\item 64-bit $v_6$: $\ell = 34$, $n = 64$.
  \begin{align*}
    e'_5 &= 2^{98} - v_5 d_6 = 2^{30} (e_5 - v_5 \epsilon) \\
    v_6 &= 2^{30} v_5 + \lfloor 2^{-68} d e'_5 \rfloor \\
    e_6 &= 2^{128} - v_6 d < 2^{-68} e'^2_5 + \xi d_5 \\
    &< 2^{130-68} + d = 2^{62} + d < 2 d
  \end{align*}
\end{itemize}
\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
