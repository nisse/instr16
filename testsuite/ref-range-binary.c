/* Generate range of inputs for a binary operation. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef CODE
#error Must define CODE
#endif

int
main (int argc, char **argv)
{
  int low, high, a, b;
  if (argc != 3)
    {
    fail:
      fprintf (stderr, "bad argument.\n");
      return EXIT_FAILURE;
    }
  low = atoi (argv[1]);
  high = atoi (argv[2]);

  for (a = low; a < high; a++)
    for (b = low; b < high; b++)
      printf ("%d %d %d\n",
              CODE (a, b), a, b);

  return EXIT_SUCCESS;
}
