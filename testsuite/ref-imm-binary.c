/* Generate random small inputs for a binary operation. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef CODE
#error Must define CODE
#endif

int
main (int argc, char **argv)
{
  int count, i;
  unsigned short rstate[3] = { 0, 0, 0x330E };
  if (argc != 2)
    {
    fail:
      fprintf (stderr, "bad argument.\n");
      return EXIT_FAILURE;
    }
  count = atoi (argv[1]);
  if (count < 1)
    goto fail;

  for (i = 0; i < count; i++)
    {
      uint64_t a, b;
      a = jrand48 (rstate) % 100 - 50;
      b = jrand48 (rstate) % (2*i + 1) - i;
      if (i & 1)
        b += a;
      printf ("%ld %ld %ld\n", CODE (a, b), a, b);
    }
  return EXIT_SUCCESS;
}
