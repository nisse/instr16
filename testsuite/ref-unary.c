/* Generate random inputs for a unary operation. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef CODE
#error Must define CODE
#endif

int
main (int argc, char **argv)
{
  int count, i;
  unsigned short rstate[3] = { 0, 0, 0x330E };
  if (argc != 2)
    {
    fail:
      fprintf (stderr, "bad argument.\n");
      return EXIT_FAILURE;
    }
  count = atoi (argv[1]);
  if (count < 1)
    goto fail;

  for (i = 0; i < count; i++)
    {
      uint64_t a;
      a = (uint32_t) jrand48 (rstate);
      a = (a << 32) | (uint32_t) jrand48 (rstate);
      printf ("%lu %lu %lu\n", CODE (a), a);
    }
  return EXIT_SUCCESS;
}
