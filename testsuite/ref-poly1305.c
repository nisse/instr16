/* Generate random poly1305 inputs and corresponding output. */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <nettle/poly1305.h>

static void
random_bytes (unsigned short rstate[3], size_t size, uint8_t *buf)
{
  while (size > 0)
    {
      uint32_t x;
      unsigned j;
      for (j = 0, x = (uint32_t) jrand48 (rstate); j < 4; j++, x >>= 8)
	{
	  *buf++ = x;

	  if (!--size)
	    return;
	}
    }
}

static void
write_hex (size_t size, const uint8_t *data, char sep)
{
  size_t i;
  for (i = 0; i < size; i++)
    printf ("%02X", data[i]);

  printf ("%c", sep);
}

static void
one (const uint8_t *p1305_key, const uint8_t *blind, size_t size, const uint8_t *msg)
{
  struct poly1305_aes_ctx ctx;
  struct aes128_ctx aes;
  static const uint8_t zero[16] = {0};
  uint8_t nonce[16] = {0};
  uint8_t key[POLY1305_AES_KEY_SIZE];
  uint8_t digest[16];

  /* Reverse the AES nonce-processing. */
  aes128_set_decrypt_key (&aes, zero);
  aes128_decrypt (&aes, 16, nonce, blind);

  memcpy (key, zero, 16);
  memcpy (key + 16, p1305_key, 16);

  poly1305_aes_set_key (&ctx, key);
  poly1305_aes_set_nonce (&ctx, nonce);
  poly1305_aes_update (&ctx, size, msg);
  poly1305_aes_digest (&ctx, 16, digest);

  /* Output digest, p1305 key, blinding key, msg. */
  write_hex (16, digest, ' ');
  write_hex (16, p1305_key, ' ');
  write_hex (16, blind, ' ');
  write_hex (size, msg, '\n');
}

#define MAX_SIZE 32

int
main(int argc, char **argv)
{
  static const uint8_t rfc8439_p1305_key[16] = {
    0x85, 0xd6, 0xbe, 0x78, 0x57, 0x55, 0x6d, 0x33,
    0x7f, 0x44, 0x52, 0xfe, 0x42, 0xd5, 0x06, 0xa8,
  };
  static const uint8_t rfc8439_blind[16] = {
    0x01, 0x03, 0x80, 0x8a, 0xfb, 0x0d, 0xb2, 0xfd,
    0x4a, 0xbf, 0xf6, 0xaf, 0x41, 0x49, 0xf5, 0x1b,
  };
  static const uint8_t rfc8439_message[34] =
    "Cryptographic Forum Research Group";

  unsigned short rstate[3] = { 0, 0, 0x330E };
  int count, i;
  if (argc != 2)
    {
    fail:
      fprintf (stderr, "bad argument.\n");
      return EXIT_FAILURE;
    }
  count = atoi (argv[1]);
  if (count < 1)
    goto fail;

  one (rfc8439_p1305_key, rfc8439_blind, 34, rfc8439_message);
  for (i = 1; i < count; i++)
    {
      uint8_t key[16];
      uint8_t blind[16];
      uint8_t msg[MAX_SIZE];
      size_t size = 1 + nrand48(rstate) % MAX_SIZE;
      random_bytes (rstate, 16, key);
      random_bytes (rstate, 16, blind);
      random_bytes (rstate, size, msg);

      one (key, blind, size, msg);
    }
  return 0;
}
