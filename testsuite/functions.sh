set -e

die () {
    echo "$@" 1>&2
    exit 1
}

hex_int64 () {
    while [ "$1" ] ; do
	x="$1"
	if expr $x : "^-" >/dev/null ; then
	    x=`echo "$x+2^64" | bc`
	fi
	for k in `seq 56 -8 0` ; do
	    printf "%02x" `echo "( $x / 2^$k ) % 256" | bc`
	done
	echo
	shift
    done
}

test_as () {
    echo "$2" > tmp.s \
    && ../assembler/instr16-as tmp.s -o tmp.o \
    && od -An -tx1 tmp.o | sed -e 's/^ //' > tmp.hex \
    && echo "$1" | diff -u - tmp.hex \
    || die "Incorrect assembly for: $2"
}

CC=gcc

test_unary() {
    args="$1"
    code="$2"
    asm="$3"
    $CC "-DCODE${args}=${code}" ref-unary.c -o tmp-ref
    echo "$asm" > tmp.s
    ../assembler/instr16-as -o tmp.o tmp.s
}

# For testing binary operations
test_sim_unary () {
    test_unary "$1" "$2" "$3
halt"
    ./tmp-ref 100 | while read out in ; do
    echo $out > tmp.ref
    ../simulator/instr16 -d tmp.o $in | awk '/r00:/ { print $3; }' > tmp.out
    diff -u tmp.ref tmp.out || die "failed for inputs $in"
  done
}

# For testing binary operations
test_hw_unary () {
    test_unary "$1" "$2" "
mov r3, #data
ld r0, [r3, #8]
$3
halt
data:
.int64 0 ; Dummy value to get right alignment
"
    ./tmp-ref 30 | while read out in ; do
    (../examples/to-hex < tmp.o ; hex_int64 $in) > tmp.hex
    echo $out > tmp.ref
    ../hw/main +img=tmp.hex +decimal=1 +quiet=1 | awk '/reg *0:/ { print $3; }' > tmp.out
    diff -u tmp.ref tmp.out || die "failed for inputs $in"
  done
}

test_binary () {
    args="$1"
    code="$2"
    asm="$3"
    $CC "-DCODE${args}=${code}" ref-binary.c -o tmp-ref
    echo "$asm" > tmp.s
    ../assembler/instr16-as -o tmp.o tmp.s
}

# For testing binary operations
test_sim_binary () {
    test_binary "$1" "$2" "$3
halt"
    ./tmp-ref 100 | while read out in ; do
    echo $out > tmp.ref
    ../simulator/instr16 -d tmp.o $in | awk '/r00:/ { print $3; }' > tmp.out
    diff -u tmp.ref tmp.out || die "failed for inputs $in"
  done
}

# For testing binary operations
test_hw_binary () {
    test_binary "$1" "$2" "
mov r3, #data
ld r0, [r3, #8]
ld r1, [r3, #16]
$3
halt
data:
.int64 0 ; Dummy value to get right alignment
"
    ./tmp-ref 30 | while read out in ; do
    (../examples/to-hex < tmp.o ; hex_int64 $in) > tmp.hex
    echo $out > tmp.ref
    ../hw/main +img=tmp.hex +decimal=1 +quiet=1 | awk '/reg *0:/ { print $3; }' > tmp.out
    diff -u tmp.ref tmp.out || die "failed for inputs $in"
  done
}

test_range_binary () {
    args="$1"
    code="$2"
    asm="$3"
    $CC "-DCODE${args}=${code}" ref-range-binary.c -o tmp-ref
    echo "$asm" > tmp.s
    ../assembler/instr16-as -o tmp.o tmp.s
}

test_sim_range() {
  test_range_binary "$1" "$2" "$3"
  ./tmp-ref $4 $5 | while read out in ; do
    echo $out > tmp.ref
    ../simulator/instr16 -d tmp.o -- $in | awk '/r00:/ { print $3; }' > tmp.out
    diff -u tmp.ref tmp.out || die "failed for inputs $in"
  done
}

test_hw_range() {
  test_range_binary "$1" "$2" "
mov r3, #data
ld r0, [r3, #8]
ld r1, [r3, #16]
$3
halt
data:
.int64 0 ; Dummy value to get right alignment
"
  ./tmp-ref $4 $5 | while read out in ; do
    echo $out > tmp.ref
    (../examples/to-hex < tmp.o ; hex_int64 $in) > tmp.hex
    echo $out > tmp.ref
    ../hw/main +img=tmp.hex +decimal=1 +quiet=1 | awk '/reg *0:/ { print $3; }' > tmp.out
    diff -u tmp.ref tmp.out || die "failed for inputs $in"
  done
}

test_imm_ref () {
    args="$1"
    code="$2"
    $CC "-DCODE${args}=${code}" ref-imm-binary.c -o tmp-ref
}

test_sim_imm() {
    test_imm_ref "$1" "$2"
    ./tmp-ref 250 | \
	while read out a b ; do
	    echo $out > tmp.ref
	    cat > tmp.s <<EOF
mov r3, #data
ld r0, [r3]
$3, #$b
$4
halt
data:
.int64 $a
EOF
	    # Ignore failures due to invalid imm values, e.g., tst r0, #0
	    ../assembler/instr16-as tmp.s -o tmp.o 2>/dev/null || continue
            ../simulator/instr16 -d tmp.o $in | awk '/r00:/ { print $4; }' > tmp.out
	    diff -u tmp.ref tmp.out || die "failed for inputs $a, $b"
	done
}

test_hw_imm() {
    test_imm_ref "$1" "$2"
    ./tmp-ref 30 | \
	while read out a b ; do
	    echo $out > tmp.ref
	    cat > tmp.s <<EOF
mov r3, #data
ld r0, [r3]
$3, #$b
$3
halt
data:
.int64 $a
EOF
	    # Ignore failures due to invalid imm values, e.g., tst r0, #0
	    ../assembler/instr16-as tmp.s -o tmp.o 2>/dev/null || continue
	    ../examples/to-hex < tmp.o > tmp.hex
            ../hw/main +img=tmp.hex +decimal=1 +quiet=1 | awk '/reg *0:/ { print $4; }' > tmp.out
	    diff -u tmp.ref tmp.out || die "failed for inputs $a, $b"
	done
}

verbose_flag=false

enable_verbose () {
    verbose_flag=true
}

verbose () {
    if [ "$verbose_flag" = "true" ] ; then
       echo 1>&2 "$@"
    fi
}
