	;; Write the unsigned bignum {r0, r1}, in base r2 (default 10).
put_bignum:

	mov	r6, #10
	cmpeq	r2, #0
	movf	r6, r2

	;; Maximum number of digits
	mov	r3, r1
	lshift	r3, #6
	and	r3, #-8
	add	r3, #8 		; Includes space for a NUL
	;; Use r12 as a frame pointer
	sub	r13, #48
	st	r6, [r13]
	st	r7, [r13, #8]
	st	r8, [r13, #16]
	st	r9, [r13, #24]
	st	r12, [r13, #32]
	st	r14, [r13, #40]
	mov	r12, r13
	mov	r9, r13
	sub	r13, r3

	mov	r7, r0
	mov	r8, r1
	lshift	r8, #3
	mov	r0, #12288	; '0', NUL
	st	r0, [r9, #-8]
	sub	r9, #1
put_bignum_loop:
	;; Strip leading zero limbs
	mov	r1, r8
	subc	r1, #8
	bf 	put_bignum_output
	ld	r0, [r7, r1]
	cmpeq	r0, #0
	movt	r8, r1
	bt	put_bignum_loop

	mov	r0, r7
	mov	r1, r7
	mov	r2, r8
	rshift	r2, #3
	mov	r3, r6
	jsr	mpn_div_1

	;; Convert to ascii digit
	cmpugeq	r0, #10
	add	r0, #'0'
	mov	r1, r0
	add	r1, #39
	movt	r0, r1

	sub	r9, #1
	ld	r1, [r9]
	injt8	r1, r0
	st	r1, [r9]
	jmp	put_bignum_loop

put_bignum_output:
	mov	r0, r9
	add	r9, #1
	cmpeq	r9, r12
	bf	put_bignum_nonzero
	sub	r0, #1

put_bignum_nonzero:
	jsr	puts
	mov	r13, r12
	ld	r6, [r13]
	ld	r7, [r13, #8]
	ld	r8, [r13, #16]
	ld	r9, [r13, #24]
	ld	r12, [r13, #32]
	add	r13, #48
	ld	r15, [r13, #-8]
