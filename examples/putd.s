	;; Write a unsigned number r0, in base r1 (default 10).
putd:
	;; Push return address, saved r6, and input
	sub	r13, #24
	st	r14, [r13, #16]
	st	r6,  [r13, #8]
	st	r0, [r13]
	;; Select base
	mov	r6, #10
	cmpugeq	r1, #2
	movt	r6, r1
	mov	r0, r6
	jsr	udiv_prepare
	ld	r2, [r13]
	mov	r3, #0
	;; Store terminating NUL
	st	r3, [r13]
	;; Allocate 72 byte area on stack.
	;; (Keep stack pointer word aligned)
	sub	r13, #64
	;; Index for writing
	mov	r3, #71
putd_loop:
	;; Set (r2, r4) <-- (r2 / base, r2 % base)
	mov	r4, r2
	umulhi	r2, r0
	addc	r2, r4
	xshift	r2, r1
	mov	r5, r6
	mullo	r5, r2
	sub	r4, r5
	;; Convert to ascii digit
	cmpugeq	r4, #10
	add	r4, #'0'
	mov	r5, r4
	add	r5, #7
	movt	r4, r5
	sub	r3, #1
	;; Store byte. Collecting digits into a register would be more
	;; efficient.
	ld	r5, [r13, r3]
	injt8	r5, r4
	st	r5, [r13, r3]

	cmpeq	r2, #0
	bf	putd_loop

	mov	r0, r13
	add	r0, r3
	jsr	puts
	add	r13, #88
	ld	r6, [r13, #-16]
	ld	r15, [r13, #-8]
