;; Needs m4 preprocessing

start:
	;;  Should use a pc-relative offset.
	mov	r0, #hello
	jsr	puts
	mov	r0, #0
	halt
hello:
	.ascii	"Hello world."
	.int8	10
	.int8	0

include(puts.s)

