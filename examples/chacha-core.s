changecom(`;')
	;; Implementation of the chacha core function.

	;; State, even elements in X, odd elements in Y
define(`X0', `r0')
define(`Y0', `r1')
define(`X1', `r2')
define(`Y1', `r3')
define(`X2', `r4')
define(`Y2', `r5')
define(`X3', `r6')
define(`Y3', `r7')
define(`T', `r8')
define(`COUNT', `r9')

	;; TRANSPOSE(A, B, T), if A = A0, A1, B = B0, B1, sets
	;; A <-- A0, B0
	;; B <-- A1, B1
define(`TRANSPOSE', `
	mov	$3, $2		; B0, B1
	injt32	$2, $1		; A1, B1
	;; FIXME: Some more clever way, fewer instructions?
	rot	$3, #32		; B1, B0
	rot	$1, #32		; A1, A0
	injt32	$1, $3		; B0, A0
	rot	$1, #32		; A0, B0
')

	;; Input in registers r0--r7, with register rk holding state
	;; value 2k in the high 32 bits, and 2k+1 in the low 32 bits.
	;; Output produced in the same register layout.
chacha_core:
	;; Save original values
	sub	r13, #64
	st	r0, [r13]
	st	r1, [r13, #8]
	st	r2, [r13, #16]
	st	r3, [r13, #24]
	st	r4, [r13, #32]
	st	r5, [r13, #40]
	st	r6, [r13, #48]
	st	r7, [r13, #56]
	mov	COUNT, #9

	;; Permute. FIXME: Keep state permuted?
	TRANSPOSE(r0, r1, r8)	; X0:  0, 2, Y0:  1,  3
	TRANSPOSE(r2, r3, r8)	; X1:  4, 6, Y1:  5,  7
	TRANSPOSE(r4, r5, r8)	; X2:  8,10, Y2:  9, 11
	TRANSPOSE(r6, r7, r8)	; X3: 12,14, Y3: 13, 15
loop:
	add32	X0, X1		; s0 += s4; s2 += s6
	add32	Y0, Y1		; s1 += s5; s3 += s7
	xor	X3, X0		; s12 ^= s0; s14 ^= s2
	xor	Y3, Y0		; s13 ^= s1; s15 ^= s3
	rot32	X3, #16
	rot32	Y3, #16

	add32	X2, X3		; s8 += s12; s10 += s14
	add32	Y2, Y3		; s9 += s13; s11 += s15
	xor	X1, X2		; s4 ^= s8; s6 ^= s10
	xor	Y1, Y2		; s5 ^= s9; s7 ^= s11
	rot32	X1, #12
	rot32	Y1, #12

	add32	X0, X1		; s0 += s4; s2 += s6
	add32	Y0, Y1		; s1 += s5; s3 += s7
	xor	X3, X0		; s12 ^= s0; s14 ^= s2
	xor	Y3, Y0		; s13 ^= s1; s15 ^= s3
	rot32	X3, #8
	rot32	Y3, #8

	add32	X2, X3		; s8 += s12; s10 += s14
	add32	Y2, Y3		; s9 += s13; s11 += s15
	xor	X1, X2		; s4 ^= s8; s6 ^= s10
	xor	Y1, Y2		; s5 ^= s9; s7 ^= s11
	rot32	X1, #7
	rot32	Y1, #7

	;; Shift rows
				; X0:  0,  2, Y0:  1,  3
	rot	X1, #32		; Y1:  5,  7, X1:  6,  4 (X1 swapped)
	rot	X2, #32
	rot	Y2, #32		; X2: 10,  8, Y2: 11,  9 (X2, Y2 swapped)
	rot	Y3, #32		; Y3: 15, 13, X3: 12, 14 (X3 swapped)

	add32	X0, Y1
	add32	Y0, X1
	xor	Y3, X0
	xor	X3, Y0
	rot32	Y3, #16
	rot32	X3, #16

	add32	X2, Y3		; s8 += s12; s10 += s14
	add32	Y2, X3		; s9 += s13; s11 += s15
	xor	Y1, X2		; s4 ^= s8; s6 ^= s10
	xor	X1, Y2		; s5 ^= s9; s7 ^= s11
	rot32	Y1, #12
	rot32	X1, #12

	add32	X0, Y1		; s0 += s4; s2 += s6
	add32	Y0, X1		; s1 += s5; s3 += s7
	xor	Y3, X0		; s12 ^= s0; s14 ^= s2
	xor	X3, Y0		; s13 ^= s1; s15 ^= s3
	rot32	Y3, #8
	rot32	X3, #8

	add32	X2, Y3		; s8 += s12; s10 += s14
	add32	Y2, X3		; s9 += s13; s11 += s15
	xor	Y1, X2		; s4 ^= s8; s6 ^= s10
	xor	X1, Y2		; s5 ^= s9; s7 ^= s11
	rot32	Y1, #7
	rot32	X1, #7

	;; Shift rows back
	rot	X1, #32
	rot	X2, #32
	rot	Y2, #32
	rot	Y3, #32

	subc	COUNT, #1
	bt	loop

	;; Transpose back.
	TRANSPOSE(r0, r1, r8)
	TRANSPOSE(r2, r3, r8)
	TRANSPOSE(r4, r5, r8)
	TRANSPOSE(r6, r7, r8)

	;; Add original values
	ld	T, [r13]
	add32	r0, T
	ld	T, [r13, #8]
	add32	r1, T
	ld	T, [r13, #16]
	add32	r2, T
	ld	T, [r13, #24]
	add32	r3, T
	ld	T, [r13, #32]
	add32	r4, T
	ld	T, [r13, #40]
	add32	r5, T
	ld	T, [r13, #48]
	add32	r6, T
	ld	T, [r13, #56]
	add32	r7, T

	;; Byte swap 32-bit parts. FIXME: Half of the rotations
	;; cancels rotations part of the transpose.
	rot	r0, #32
	bswap	r0
	rot	r1, #32
	bswap	r1
	rot	r2, #32
	bswap	r2
	rot	r3, #32
	bswap	r3
	rot	r4, #32
	bswap	r4
	rot	r5, #32
	bswap	r5
	rot	r6, #32
	bswap	r6
	rot	r7, #32
	bswap	r7

	add	r13, #64
	halt

divert(-1)
;;  One expanded qround:
;;  s0 = s0 + s4; s12 = ROTL32(16, (s0 ^ s12));
;;  s8 = s8 + s12; s4 = ROTL32(12, (s4 ^ s8));
;;  s0 = s0 + s4; s12 = ROTL32(8,  (s0 ^ s12));
;;  s8 = s8 + s12; s4 = ROTL32(7,  (s4 ^ s8));
;;
;;  s1 = s1 + s5; s13 = ROTL32(16, (s1 ^ s13));
;;  s9 = s9 + s13; s5 = ROTL32(12, (s5 ^ s9));
;;  s1 = s1 + s5; s13 = ROTL32(8,  (s1 ^ s13));
;;  s9 = s9 + s13; s5 = ROTL32(7,  (s5 ^ s9));
;;
;;  s2 = s2 + s6; s14 = ROTL32(16, (s2 ^ s14));
;;  s10 = s10 + s14; s6 = ROTL32(12, (s6 ^ s10));
;;  s2 = s2 + s6; s14 = ROTL32(8,  (s2 ^ s14));
;;  s10 = s10 + s14; s6 = ROTL32(7,  (s6 ^ s10));
;;
;;  s3 = s3 + s7; s15 = ROTL32(16, (s3 ^ s15));
;;  s11 = s11 + s15; s7 = ROTL32(12, (s7 ^ s11));
;;  s3 = s3 + s7; s15 = ROTL32(8,  (s3 ^ s15));
;;  s11 = s11 + s15; s7 = ROTL32(7,  (s7 ^ s11));
