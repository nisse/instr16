;;; RPN calculator. Operators:
;;;  + addition
;;;  - subtraction
;;;  * multiplication
;;;  / division
;;;  % remainder
;;;  = print top of stack (in current base)
;;;  # set base for input and output

	;; Stack registers, S0 on top.
	pushdef(`S0', `r6')
	pushdef(`S1', `r7')
	pushdef(`S2', `r8')
	pushdef(`S3', `r9')
	pushdef(`BASE', `r10')

	mov	S0, #0
	mov	S1, #0
	mov	S2, #0
	mov	S3, #0

calc_restart:
	mov	BASE, #10
loop:
	mov	r0, BASE
	jsr	getd
	cmpeq	r1, #0
	bt	calc_dispatch

	;; Push r0 on stack.
	mov	S3, S2
	mov	S2, S1
	mov	S1, S0
	mov	S0, r0

calc_dispatch:
	cmpeq	r2, #-1
	bt	done
	cmpeq	r2, #' '
	bt	loop
	cmpeq	r2, #'\n'
	bt	loop
	cmpeq	r2, #'\r'
	bt	loop
	cmpeq	r2, #'\t'
	bt	loop

	cmpeq	r2, #'='
	bt	calc_print

	;; Pop r0 from stack
	mov	r0, S0
	mov	S0, S1
	mov	S1, S2
	mov	S2, S3
	mov	S3, #0

	cmpeq	r2, #'#'
	bt	calc_base

	cmpeq	r2, #'+'
	bt	calc_add
	cmpeq	r2, #'-'
	bt	calc_sub
	cmpeq	r2, #'*'
	bt	calc_mul
	cmpeq	r2, #'/'
	bt	calc_div
	cmpeq	r2, #'%'
	bt	calc_mod
	
calc_error:
	mov	r0, #error
	jsr	puts
	jmp	loop
calc_add:
	add	S0, r0
calc_print:
	mov	r0, S0
	mov	r1, BASE
	jsr	putd
	mov	r0, #newline
	jsr	puts
	jmp	loop
calc_sub:
	sub	S0, r0
	jmp	calc_print
calc_mul:
	mullo	S0, r0
	jmp	calc_print
calc_div:
	cmpeq	r0, #0
	bt	calc_error
	jsr	udiv_prepare

	umulhi	r0, S0
	addc	S0, r0
	xshift	S0, r1
	jmp	calc_print

calc_mod:
	cmpeq	r0, #0		; x mod 0 == x
	bt	calc_print
	mov	r12, r0
	jsr	udiv_prepare

	umulhi	r0, S0
	addc	r0, S0
	xshift	r0, r1

	mullo	r0, r12
	sub	S0, r0
	jmp	calc_print

calc_base:
	cmpeq	r0, #0
	bt	calc_restart
	cmpugeq	r0, #2
	bf	calc_error
	cmpugt	r0, #36
	bt	calc_error
	mov	BASE, r0
	jmp	loop
done:
	halt

error:
	.ascii "error"
newline:
	.int8	'\r'
	.int8	'\n'
	.int8	0

	popdef(`S0')
	popdef(`S1')
	popdef(`S2')
	popdef(`S3')
	popdef(`BASE')

include(getd.s)
include(get-digit.s)
include(putd.s)
include(puts.s)
include(udiv.s)
