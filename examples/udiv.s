;;;  Input:  r0: divisor d
;;;  Output: r0: reciprocal
;;;	     r1: shift
;;;  If r2 holds any number, r2 / d (unsigned) is computed as
;;;  mov	r3, r2
;;;  umulhi	r3, r0
;;;  addc	r3, r2
;;;  xshift	r3, r1
udiv_prepare:
	mov	r1, r0
	clz	r1
	recpr	r0
	addc	r0, #1
	;; Overflow, and r0 = 0, when d is a power of two.
	;; Reducing the shift count gives the correct result.
	add	r1, cc, #-64
	;; Clear high bit, to force logic right shift
	fabs	r1
	;; Return
	mov	r15, r14
