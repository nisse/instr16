	;;  i/o tty read register
	mov	r2, #524288
	mov	r1, #'>'
loop:
	st	r1, [r2, #8]	; tty write
	ld	r1, [r2]
	cmpeq	r1, #-1
	bf	loop
	halt
