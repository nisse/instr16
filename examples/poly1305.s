changecom(`;')
;;; Total context size 56 bytes.
define(`P1305_R0', 0)
define(`P1305_R1', 8)
define(`P1305_S0', 16)
define(`P1305_S1', 24)
define(`P1305_H0', 32)
define(`P1305_H1', 40)
define(`P1305_H2', 48)

define(`CTX', `r0')
	;; Takes poly1305 key in r1 (low), r2 (high), Clears selected
	;; bits, computes premultiplies, and stores at r0
poly1305_set_key:
	mov	r3, #0x0ffffffc0fffffff
	and	r1, r3
	st	r1, [r0, #P1305_R0]
	and	r2, r3
	and	r2, #-4
	st	r2, [r0, #P1305_R1]
	mov	r3, #5
	mullo	r1, r3
	st	r1, [r0, #P1305_S0]
	rshift	r2, #2
	mullo	r2, r3
	st	r2, [r0, #P1305_S1]

	;; Clear state
	mov	r3, #0
	st	r3, [CTX, #P1305_H0]
	st	r3, [CTX, #P1305_H1]
	st	r3, [CTX, #P1305_H2]
	jmp	[r14]

	;; r0 points at the context, r1, r2, r3 holds the message
define(`H0', `r1')
define(`T1', `r2')
define(`T2', `r3')
define(`H1', `r4')
define(`H2', `r5')
define(`F0', `r6')
define(`F1', `r7')
define(`P0', `r8')
define(`S1', `r9')

poly1305_update:
	sub	r13, #24
	st	r7, [r13]
	st	r8, [r13, #8]
	st	r9, [r13, #16]

	;; First accumulate the independent products
	;;
	;; {H1,H0} = R0 T0 + S1 T1 + S0 (T2 >> 2)
	;; {F1,F0} = R1 T0 + R0 T1 + S1 T2
	;; H2 = R0 * (T2 & 3)

	ld	r4, [CTX, #P1305_H0]
	addc	H0, r4
	ld	r4, [CTX, #P1305_H1]
	addc	T1, cc, r4
	ld	r4, [CTX, #P1305_H2]
	add	T2, cc, r4		; Live: H0, T1, T2

	ld 	F0, [CTX, #P1305_R1] ; R1 * T0, last use of R1
	mov	F1, F0
	mullo	F0, H0
	umulhi	F1, H0		; Live: H0, T1, T2, F0, F1

	ld	P0, [CTX, #P1305_R0] ; R0 * T0, last use of T0
	mov	H1, H0
	mullo	H0, P0
	umulhi	H1, P0		; Live: H0, H1, T1, T2, F0, F1, P0

	mov	H2, #3		; R0 * (T2 & 3)
	and	H2, T2
	mullo	H2, P0		; Live: H0, H1, H2, T1, T2, F0, F1, P0

	mov	S1, P0		; R0 * T1, last use of R0
	mullo	P0, T1
	umulhi	S1, T1

	addc	F0, P0
	add	F1, cc, S1	; Live: H0, H1, H2, T1, T2, F0, F1

	ld	S1, [CTX, #P1305_S1] ; S1 * T1, last use of T1
	mov	P0, S1
	mullo	P0, T1
	umulhi	T1, S1

	addc	H0, P0
	add	H1, cc, T1	; Live: H0, H1, H2, S1, T2, F0, F1

	mov	P0, S1		; S1 * T2, last use of S1
	mullo	P0, T2
	umulhi	S1, T2

	addc	F0, P0
	add	F1, cc, S1	; Live: H0, H1, H2, T2, F0, F1

	ld	P0, [CTX, #P1305_S0] ; S0 * (T2 >> 2) ; Last use of S0, T2
	rshift	T2, #2
	mov	S1, P0
	mullo	P0, T2
	umulhi	S1, T2

	addc	H0, P0
	add	H1, cc, S1	; Live: H0, H1, H2, F0, F1

	;; Then add together as
	;;
	;;     +--+--+--+
	;;     |H2|H1|H0|
	;;     +--+--+--+
	;;   + |F1|F0|
	;;   --+--+--+--+
	;;     |H2|H1|H0|
	;;     +--+--+--+

	addc	H1, F0
	add	H2, cc, F1

	st	H0, [CTX, #P1305_H0]
	st	H1, [CTX, #P1305_H1]
	st	H2, [CTX, #P1305_H2]

	ld	r7, [r13]
	ld	r8, [r13, #8]
	ld	r9, [r13, #16]
	add	r13, #24
	jmp	[r14]

undefine(`H0')
undefine(`T1')
undefine(`T2')
undefine(`H1')
undefine(`H2')
undefine(`F0')
undefine(`F1')
undefine(`P0')
undefine(`S1')

	;; Final fold mod p={2^130 - 5}, returned in r0, r1
define(`SP', `r1')		; Blinding values
define(`H0', `r2')
define(`H1', `r3')
define(`H2', `r4')
define(`T0', `r5')
define(`T1', `r0')		; Overlap CTX

poly1305_digest:
	ld	H2, [CTX, #P1305_H2]

	mov	T0, H2
	and	H2, #3
	rshift	T0, #2
	mov	H1, #5
	mullo	T0, H1		; Can we have an immediate mullo instr?
	ld	H0, [CTX, #P1305_H0]
	ld	H1, [CTX, #P1305_H1]
	addc	H0, T0
	addc	H1, cc, #0
	add	H2, cc, #0

	;; Check if {H2, H1, H0} + 5 >= 2^130
	mov	T0, #0
	st	T0, [CTX, #P1305_H0]
	st	T0, [CTX, #P1305_H1]
	st	T0, [CTX, #P1305_H2]
	mov	T1, #5
	addc	T1, H0
	addc	T0, cc, H1
	addc	H2, cc, #-4		; Carry if r2 + c >= 4

	movt	H0, T1
	movt	H1, T0

	ld	r0, [SP]
	ld	r1, [SP, #8]
	addc	r0, H0
	add	r1, cc, H1
	jmp	[r14]
undefine(`SP')
undefine(`H0')
undefine(`H1')
undefine(`H2')
undefine(`T0')
undefine(`T1')

;;
;; 	;; {H1,H0} = R0 T0 + S1 T1 + S0 (T2 >> 2)
;; 	;; {F1,F0} = R1 T0 + R0 T1 + S1 T2
;; 	;; H2 = R0 * (T2 & 3)
;;
;; Use once: R1, S0
;;
;; R1 * T0 ; F1, F0, T0, (kill R1)
;; R0 * T0 ; F1, F0, H1, H0, R0, (kill T0)
;; R0 * (T2 & 3) ; F1, F0, H2, H1, H0, R0, T1, T2
;; R0 * T1 ; F1, F0, H1, H0, R0, T1, T2 (kill R0)
;; S1 * T1 ; F1, F0, H2, H1, H0, S1, T2 (kill T1)
;; S1 * T2 ; F1, F0, H2, H1, H0, T2 (kill S1)
;; S0 * (T2 >> 2) ; F1, F0, H2, H1, H0,
