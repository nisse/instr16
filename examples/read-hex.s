	;; Reads a hex value, up to 16 digits. Returns number in r0,
	;; number of valid digits in r1, and terminating (non-digit)
	;; character in r2 (if r1 < 16)

	pushdef(`COUNT', `r1')
	pushdef(`CHAR', `r2')
	pushdef(`NUMBER', `r3')
	pushdef(`TTY', `r4')
read_hex:
	mov	r5, r14
	;; i/o tty in register
	mov	TTY, #0x80000
	mov	COUNT, #0
	mov	NUMBER, #0
	mov	COUNT, #0
read_hex_skip:
	ld	CHAR, [TTY]
	cmpeq	CHAR, #' '
	bt	read_hex_skip
	cmpeq	CHAR, #'\n'
	bt	read_hex_skip

read_hex_loop:
	mov	r0, CHAR
	jsr	a2digit
	cmpugeq	r0, #16	; True also for the r0 == -1 case.
	bt	read_hex_done

	add	COUNT, #1
	lshift	NUMBER, #4
	add	NUMBER, r0
	cmpeq	COUNT, #16
	bt	read_hex_done
	ld	CHAR, [TTY]
	jmp	read_hex_loop

read_hex_done:
	mov	r0, NUMBER
	jmp	[r5]

	popdef(`CHAR')
	popdef(`COUNT')
	popdef(`NUMBER')
	popdef(`TTY')
