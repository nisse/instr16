start:
	mov	r0, #0
	jsr	getd		; input base
	mov	r8, r0
	mov	r0, #0
	jsr	getd		; output base
	mov	r9, r0
	mov	r10, #20	; Maximum limb size
	sub	r13, #160

	mov	r0, r13
	mov	r1, r10
	mov	r2, r8
	jsr	get_bignum
	mov	r10, r0
	cmpeq	r10, #0
	bt	die
	mov	r0, r13
	mov	r1, r10
	mov	r2, r9
	jsr	put_bignum
	mov	r0, #newline
	jsr	puts
	mov	r0, #0
	halt
die:
	mov	r0, #1
	halt
newline:
.int8	'\n'
.int8	0

include(getd.s)
include(get-digit.s)
include(get-bignum.s)
include(put-bignum.s)
include(puts.s)
include(mpn_mul_1.s)
include(mpn_div_1.s)
