start:
	;; Read poly1305 key (evaluation point)
	jsr	read_hex_le
	mov	r6, r0

	jsr	read_hex_le
	mov	r1, r6
	mov	r2, r0
	sub	r13, #56
	mov	r0, r13
	jsr	poly1305_set_key

	;; Read poly1305 blinding key
	sub	r13, #16
	jsr	read_hex_le
	st	r0, [r13]
	jsr	read_hex_le
	st	r0, [r13, #8]

loop:
	jsr	read_block
	cmpeq	r2, #-1
	bt	digest
	mov	r7, r2
	mov	r3, r2
	and	r3, #1
	mov 	r2, r1
	mov	r1, r0
	mov	r0, r13
	add	r0, #16
	jsr	poly1305_update
	cmpeq	r7, #0
	bf	loop

digest:
	mov	r0, r13
	add	r0, #16
	mov	r1, r13
	jsr	poly1305_digest
	mov	r6, r1
	jsr	write_hex
	mov	r0, r6
	jsr	write_hex
	mov	r0, #end_of_line
	jsr	puts
	mov	r0, #0
	halt

	;; Read a message block, terminated by a non-digit or EOF. Returned in
	;; r0-r2, with r2 == -1 at end of data.
read_block:
	sub	r13, #8
	st	r14, [r13]

	jsr	read_hex
	rshiftc	r1
	bt	fail
	cmpeq	r1, #8
	bf	msg_end_r0
	bswap	r0
	mov	r6, r0

	jsr	read_hex
	rshiftc	r1
	bt	fail
	cmpeq	r1, #8
	bf	msg_end_r1
	bswap	r0

	mov	r1, r0
	mov	r0, r6
	mov	r2, #1
read_block_end:
	add	r13, #8
	ld	r15, [r13, #-8]

msg_end_r0:
	cmpeq	r1, #0
	mov	r2, #-1
	bt	read_block_end
	lshift	r0, #8
	or	r0, #1
	xor	r1, #7
	lshift	r1, #3
	shift	r0, r1

	bswap	r0
	mov	r1, #0
	mov	r2, #0
	jmp	read_block_end

msg_end_r1:
	mov	r2, #0
	cmpeq	r1, #0
	add	r1, cc, #0
	movt	r0, r6
	bt	read_block_end

	lshift	r0, #8
	or	r0, #1
	xor	r1, #7
	lshift	r1, #3
	shift	r0, r1

	bswap	r0
	mov	r1, r0
	mov	r0, r6
	jmp	read_block_end

	;; Read a 64-bit little-endian hex value.
read_hex_le:
	sub	r13, #8
	st	r14, [r13]
	jsr	read_hex

	cmpeq 	r1, #16
	bf	fail
	bswap	r0
	add	r13, #8
	ld	r15, [r13, #-8]

fail:
	mov	r0, #error
	jsr	puts
	mov	r0, #1
	halt

	;; Write little-endian hex
write_hex:
	mov	r5, r14
	;; i/o tty out register
	mov	r2, #0x80008
	mov	r1, #7
write_hex_loop:
	mov	r3, r0
	rshift	r3, #4
	and	r3, #0xf
	jsr	write_hex_digit

	mov	r3, r0
	and	r3, #0xf
	jsr	write_hex_digit

	rshift	r0, #8
	subc	r1, #1
	bt	write_hex_loop
	jmp	[r5]

write_hex_digit:
	cmpugeq	r3, #10
	add	r3, #'0'
	mov	r4, r3
	add	r4, #7
	movt	r3, r4
	st	r3, [r2]
	jmp	[r14]
error:
.ascii	"bad input"
end_of_line:
.int8	'\n'
.int8	0

include(get-digit.s)
include(poly1305.s)
include(puts.s)
include(read-hex.s)
