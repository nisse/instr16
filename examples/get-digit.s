	;; Read a single digit. Return -1 for non-digits
	;; Clobbers no registers but r0.
get_digit:
	;; i/o tty in register
	mov	r0, #524288
	ld	r0, [r0]
	;; Entry point without i/o.
a2digit:
	cmpeq	r0, #-1
	bt	get_digit_end
	subc	r0, #'0'
	bf	get_digit_end
	cmpugeq	r0, #10
	bf	[r14]
	cmpugeq	r0, #('A' - '0')
	bf	get_digit_end
	sub	r0, #7
	cmpugeq	r0, #36
	bf	[r14]

	cmpugeq	r0, #('a'-'0'-7)
	bf	get_digit_end
	sub	r0, #32
	jmp	[r14]
get_digit_end:
	mov	r0, #-1
	jmp	[r14]
