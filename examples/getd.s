	pushdef(`COUNT', `r1')
	pushdef(`CHAR', `r2')
	pushdef(`NUMBER', `r3')
	pushdef(`BASE', `r4')
	pushdef(`TTY', `r5')

	;; Read an unsigned number, in base r0 (default 10).
	;; Returns number in r0, number of valid digits in r1, and
	;; terminating (non-digit) character in r2.
getd:
	sub	r13, #8
	st	r14, [r13]
	;; i/o tty in register
	mov	TTY, #524288

	mov	BASE, #10
	cmpeq	r0, #0
	movf	BASE, r0
	mov	NUMBER, #0
	mov	COUNT, #0

getd_loop:
	ld	CHAR, [TTY]
	mov	r0, CHAR
	;; a2digit doesn't clobber any registers but r0.
	jsr 	a2digit

	cmpugeq	r0, BASE	; True also for the r0 == -1 case.
	bt	getd_done

	add	COUNT, #1
	mullo	NUMBER, BASE
	add	NUMBER, r0
	jmp	getd_loop

getd_done:
	mov	r0, r3
	add	r13, #8
	ld	r15, [r13, #-8]

	popdef(`CHAR')
	popdef(`COUNT')
	popdef(`NUMBER')
	popdef(`BASE')
	popdef(`TTY')
