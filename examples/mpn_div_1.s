	;; div21 (u1, u0, t1, t0)
	;; Puts remainder in u1, quotient in t1
	define(`div21', `
	mov	$3, $1
	mov	$4, $1
	mullo	$4, r4
	umulhi	$3, r4
	addc	$4, $2 		; q0
	add	$3, cc, $1	; q1

	mov	$1, $3
	mullo	$1, r3
	sub	$2, $1
	mov	$1, $2
	sub	$2, r3
	cmpugeq	$2, $4 		; if (r - d >= q0)
	movf	$1, $2
	not	cc
	add	$3, cc, #0

	mov	$2, $1
	subc	$2, r3
	movt	$1, $2
	add	$3, cc, #0
')
	;; Set {r0, r2} <-- {r1, r2} / r3, and returns remainder
mpn_div_1:
	mov	r4, r3
	mov	r5, r3
	recpr	r4
	clz	r5

mpn_div_1_preinv:
	sub	r13, #40
	st	r6, [r13]
	st	r7, [r13, #8]
	st	r8, [r13, #16]
	st	r9, [r13, #24]
	st	r10, [r13, #32]
	mov	r8, r2
	lshift	r8, #3
	subc	r8, #8
	movf	r0, r2		; Return zero
	bf	mpn_div_1_ret

	ld	r6, [r1, r8]
	cmpeq	r8, #0
	bt	mpn_div_1_single

	shift	r3, r5
	mov	r7, r6
	mov	r6, #0
	shiftl	r6, r7, r5
	sub	r1, #8

mpn_div_1_loop:
	ld	r2, [r1, r8]
	shiftl	r7, r2, r5
	div21(r6, r7, r9, r10)
	st	r9, [r0, r8]
	mov	r7, r2
	sub	r8, #8
	bnz	mpn_div_1_loop

	shift	r7, r5
	;; 2/1 division (r6, r7) / r3
	div21(r6, r7, r8, r9)

	st	r8, [r0]
	neg	r5
	fabs	r5
	shift	r6, r5
	mov	r0, r6

mpn_div_1_ret:
	ld	r6, [r13]
	ld	r7, [r13, #8]
	ld	r8, [r13, #16]
	ld	r9, [r13, #24]
	ld	r10, [r13, #32]
	add	r13, #40
	jmp	[r14]

mpn_div_1_single:
	addc	r4, #1
	add	r5, cc, #-64
	;; Clear high bit, to force logic right shift
	fabs	r5

	mov	r1, r6
	umulhi	r6, r4
	addc	r6, r1
	xshift	r6, r5
	st	r6, [r0]
	mullo	r6, r3
	mov	r0, r1
	sub	r0, r6
	jmp	mpn_div_1_ret
