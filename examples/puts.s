puts:
	;; i/o tty out register
	mov	r2, #524296
puts_loop:
	ld	r1, [r0]
	add	r0, #1
	rshift	r1, #56
	cmpeq	r1, #0
	bt	[r14]
	st	r1, [r2]
	jmp	puts_loop
