	;; Set {r0, r2} <-- r3 {r1, r2} + r4
mpn_mul_1:
	mov	r4, #0		; Carry word
mpn_mul_1c:
	cmpeq	r2, #0
	movt	r0, r4
	bt	[r14]
	sub	r13, #8
	st	r8, [r13]
	mov	r8, r2
	lshift	r8, #3

	add	r0, r8
	add	r1, r8
	neg	r8

	;;     +---+---+---+---+
	;;     | v3| v2| v1| v0|
	;;     +---+---+---+---+
	;;               * |  u|
	;;                 +---+
	;;               + |  c|
	;;     ------------+---+

	;;     +---+---+---+---+
	;;     | L3| L2| L1| L0|  mullo
	;; +---+---+---+---+---+
	;; | H3| H2| H1| H0| C | umulhi
	;; +---+---+---+---+---+
	
	
mpn_mul_1_loop:
	ld	r5, [r1, r8]
	mullo	r5, r3
	addc	r5, cc, r4
	ld	r4, [r1, r8]
	st	r5, [r0, r8]
	umulhi	r4, r3
	add	r8, #8
	bnz	mpn_mul_1_loop

	mov	r0, #0
	add	r0, cc, r4
	ld	r8, [r13]
	add	r13, #8
	jmp	[r14]
