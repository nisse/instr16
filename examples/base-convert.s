start:
	mov	r0, #0
	jsr	getd		; input base
	mov	r8, r0
	mov	r0, #0
	jsr	getd		; output base
	mov	r9, r0
	mov	r0, r8
	jsr	getd
	mov	r1, r9
	jsr	putd
	mov	r0, #newline
	jsr	puts
	mov	r0, #0
	halt
newline:
.int8	'\n'
.int8	0

include(get-digit.s)
include(getd.s)
include(putd.s)
include(puts.s)
include(udiv.s)
