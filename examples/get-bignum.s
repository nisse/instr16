	;; Read an unsigned bignum, in base r2 (default 10).
	;; Store at most r1 limbs at r0. Returns size, or -1 on overflow.
get_bignum:
	sub	r13, #48
	st	r6, [r13]
	st	r7, [r13, #8]
	st	r8, [r13, #16]
	st	r9, [r13, #24]
	st	r10, [r13, #32]
	st	r14, [r13, #40]

	mov	r6, #10
	cmpeq	r2, #0
	movf	r6, r2

	mov	r7, r0
	mov	r8, r0
	mov	r9, r1
	mov	r10, #0

	jsr	get_digit
	cmpeq	r0, #-1
	bt	get_bignum_done

get_bignum_grow:
	subc	r9, #1
	bf	get_bignum_overflow
	st	r0, [r8]
	add	r8, #8
	add	r10, #1

get_bignum_next:
	jsr	get_digit
	cmpeq	r0, #-1
	bt	get_bignum_done
	mov	r4, r0
	mov	r0, r7
	mov	r1, r7
	mov	r2, r10
	mov	r3, r6
	jsr	mpn_mul_1c
	cmpeq	r0, #0
	bt	get_bignum_next
	jmp	get_bignum_grow

get_bignum_overflow:
	mov	r0, #-1
	jmp	get_bignum_ret
get_bignum_done:
	mov	r0, r10
get_bignum_ret:
	add	r13, #48
	ld	r6, [r13, #-48]
	ld	r7, [r13, #-40]
	ld	r8, [r13, #-32]
	ld	r9, [r13, #-24]
	ld	r10, [r13, #-16]
	ld	r15, [r13, #-8]
