loop:
	mov	r0, #10
	jsr	getd
	cmpeq	r1, #0
	bt 	done

	mov	r6, r0
	mov	r0, #10
	jsr	getd
	cmpeq	r1, #0
	bt 	done

	add	r0, r6
	mov	r1, #10
	jsr	putd
	mov	r0, #newline
	jsr	puts
	jmp	loop
done:
	halt

newline:
.int8	'\n'
.int8	0

include(getd.s)
include(get-digit.s)
include(putd.s)
include(puts.s)
include(udiv.s)
