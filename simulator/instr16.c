/* instr16 cpu simulator

   Copyright (C) 2013, 2014, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>

#include <errno.h>
#include <unistd.h>

#include "cpu.h"
#include "memory.h"
#include "io.h"

#include "misc.h"

static void NORETURN
usage (void)
{
  fprintf (stderr, "Usage: instr16 [OPTIONS] BINARY [R0, R1 ...]\n"
	   "Options:\n"
	   " -s SIZE   Memory size in words.\n"
	   " -i IO     Address of i/o register (in words).\n"
	   " -d        Dump registers on exit.\n"
	   " -t        Trace execution.\n");

  exit (EXIT_FAILURE);
}

int
main (int argc, char **argv)
{
  int c;
  uint64_t size = 0x1000;
  uint64_t io_offset = 0x10000;
  int dump = 0;
  int trace = 0;

  struct memory memory;
  struct cpu cpu;
  struct io_regs io;
  FILE *f;
  unsigned i;

  while ((c = getopt (argc, argv, "s:i:dt")) != -1)
    switch (c)
      {
      case 's':
	/* FIXME: Use strtoul */
	size = atol (optarg);
	break;
      case 'i':
	io_offset = atol (optarg);
	break;
      case 'd':
	dump = 1;
	break;
      case 't':
	trace = 1;
	break;
      default:
	usage ();
      }
  argv += optind;
  argc -= optind;
  if (argc < 1)
    usage ();

  if (io_offset < size)
    die ("i/o space overlapping memory.\n");

  memory_alloc (&memory, size);
  f = fopen (argv[0], "rb");
  if (!f)
    die ("Failed to open binary file %s: %s\n",
	 argv[0], strerror (errno));

  memory_load (&memory, f);
  fclose (f);

  io_init (&io);
  memory_register_io (&memory, io_offset, &io);
  
  cpu_init (&cpu);
  if (trace)
    {
      cpu_trace (&cpu);
      memory_trace (&memory);
    }

  /* Setup stack pointer */
  cpu.reg[13] = 8*size;

  argv++; argc--;
  for (i = 0; i < argc; i++)
    {
      char *endp;
      cpu.reg[i] = strtoull (argv[i], &endp, 0);
      if (!argv[i][0] || *endp)
	die ("Invalid register input '%s'\n", argv[i]);
    }
  while (cpu_instruction_cycle (&cpu, &memory))
    ;

  if (dump)
    cpu_write (&cpu, stdout);

  return !!cpu.reg[0];
}
  
