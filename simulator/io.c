/* instr16 cpu simulator

   Copyright (C) 2013  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include "io.h"

#include "misc.h"

static uint64_t
io_read (struct io_regs *io UNUSED, uint64_t addr)
{
  if (addr != 0)
    die ("io read at invalid address: %jx\n", (uintmax_t) addr);
  return (int64_t) getchar();
}

static void
io_write (struct io_regs *io UNUSED, uint64_t addr, uint64_t w)
{
  if (addr != 1)
    die ("io write at invalid address: %jx\n", (uintmax_t) addr);
  putchar (w);
}

void
io_init (struct io_regs *io)
{
  setbuf (stdout, NULL);
  io->read = io_read;
  io->write = io_write;
}
