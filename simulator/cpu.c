/* instr16 cpu simulator

   Copyright (C) 2013, 2014, 2015, 2016, 2017  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"

#include "memory.h"
#include "misc.h"

void
cpu_init (struct cpu *cpu)
{
  memset (cpu->reg, 0, sizeof(cpu->reg));
  cpu->prefix_flag = 0;
  cpu->cc_flag = 0;
  cpu->instr_counter = 0;
  cpu->trace = 0;
}

void
cpu_trace (struct cpu *cpu)
{
  cpu->trace = 1;
}

void
cpu_write (const struct cpu *cpu, FILE *f)
{
  unsigned i;
  for (i = 0; i < 16; i++)
    fprintf (f, "r%02d: %jx %ju %jd\n", i,
	     (uintmax_t) cpu->reg[i],
	     (uintmax_t) cpu->reg[i],
	     (intmax_t) (int64_t) cpu->reg[i]);
  fprintf (f, "cc: %s\n", cpu->cc_flag ? "true" : "false");
  fprintf (f, "instr count: %jd\n", (uintmax_t) cpu->instr_counter);
}

static void NORETURN
fatal_trap (const struct cpu *cpu, const char *msg)
{
  cpu_write (cpu, stderr);
  die ("trap: %s\n", msg);
}

static void
cpu_write_reg (struct cpu *cpu, unsigned reg, uint64_t value)
{
  if (cpu->trace)
    fprintf (stderr, "cpu: R[%02d] <-- %jd (0x%jx)\n",
	     reg, (uintmax_t) value, (uintmax_t) value);
  assert (reg < 16);
  cpu->reg[reg] = value;
}

static void
cpu_write_cc (struct cpu *cpu, int value)
{
  if (cpu->trace)
    fprintf (stderr, "cpu: cc <-- %d\n", value);
  cpu->cc_flag = value;
}

#define CPUW(reg, value) cpu_write_reg ((cpu), (reg), (value))
#define CPUWCC(value) cpu_write_cc((cpu), (value))

static void
i_ld_st_offset (struct cpu *cpu, struct memory *memory, uint16_t instr);
static void
i_ld_st_index (struct cpu *cpu, struct memory *memory, uint16_t instr);
static void
i_shiftl (struct cpu *cpu, uint16_t instr);
static void
i_imm_shift (struct cpu *cpu, uint16_t instr);
static void
i_imm_prefix (struct cpu *cpu, uint16_t instr);
static void
i_branch (struct cpu *cpu, uint16_t instr);
static void
i_fp_op (struct cpu *cpu, uint16_t instr);
static void
i_fmac (struct cpu *cpu, uint16_t instr);
static void
i_fldexp (struct cpu *cpu, uint16_t instr);
static void
i_binary_fp (struct cpu *cpu, uint16_t instr);
static void
i_unary_fp (struct cpu *cpu, uint16_t instr);
static void
i_imm_op (struct cpu *cpu, uint16_t instr);
static void
i_imm_add (struct cpu *cpu, uint16_t instr);
static void
i_imm_binary (struct cpu *cpu, uint16_t instr);
static void
i_imm_cmpgt (struct cpu *cpu, uint16_t instr);
static void
i_add_op (struct cpu *cpu, uint16_t instr);
static void
i_binary_op (struct cpu *cpu, struct memory *memory, uint16_t instr);
static void
i_xshift (struct cpu *cpu, uint16_t instr);
static void
i_unary_op (struct cpu *cpu, uint16_t instr);

int
cpu_instruction_cycle (struct cpu *cpu, struct memory *memory)
{
  uint16_t instr = memory_read (memory, cpu->program_counter) >> 48;
  cpu->instr_counter++;
  if (cpu->trace)
    fprintf (stderr, "cpu: %4jx %04x\n",
	     (uintmax_t) cpu->program_counter, instr);
	     
  cpu->program_counter += 2;

  int prefix_flag = 0;

  if (instr < 0x4000)
    i_ld_st_offset (cpu, memory, instr);
  else if (instr < 0x5000)
    i_ld_st_index (cpu, memory, instr);
  else if (instr < 0x6000)
    i_shiftl (cpu, instr);
  else if (instr < 0x7000)
    i_imm_shift (cpu, instr);
  else if (instr < 0x8000)
    {
      i_imm_prefix (cpu, instr);
      prefix_flag = 1;
    }
  else if (instr < 0x9800)
    i_branch (cpu, instr);
  else if (instr < 0xa000)
    i_fp_op (cpu, instr);
  else if (instr < 0xd000)
    i_imm_op (cpu, instr);
  else if (instr < 0xe000)
    i_add_op (cpu, instr);
  else if (instr < 0xf800)
    i_binary_op (cpu, memory, instr);
  else if (instr < 0xfc00)
    i_unary_op(cpu, instr);
  else if (instr == 0xfffe) /* bkpt instruction */
    cpu_write (cpu, stderr);
  else if (instr == 0xffff) /* halt instruction */
    return 0;
  else
    fatal_trap (cpu, "Reserved opcode area.\n");

  cpu->prefix_flag = prefix_flag;
  return 1;
}

static double
get_fpreg (const struct cpu *cpu, unsigned i)
{
  union {
    uint64_t i;
    double f;
  } u;
  u.i = cpu->reg[i];
  return u.f;
}

static void
set_fpreg (struct cpu *cpu, unsigned i, double f)
{
  union {
    uint64_t i;
    double f;
  } u;
  u.f = f;
  CPUW(i, u.i);
}

/* Single precision representation */
static double
get_fpreg_s (const struct cpu *cpu, unsigned i)
{
  union {
    uint32_t i;
    float f;
  } u;
  u.i = cpu->reg[i];
  return u.f;
}

static void
set_fpreg_s (struct cpu *cpu, unsigned i, float f)
{
  union {
    uint32_t i;
    float f;
  } u;
  u.f = f;
  CPUW (i, u.i);
}


static uint64_t
imm4 (const struct cpu *cpu, unsigned char imm)
{
  static const uint8_t imm4_table[16] =
    {
      32, 1,  2,  3,  4,  5,  6,  7,
      8, 10, 12, 14, 16, 20, 24, 28
    };
  return cpu->prefix_flag
    ? imm | (cpu->prefix_reg << 4)
    : imm4_table[imm];
}

static uint64_t
imm3 (unsigned char imm)
{
  static const uint8_t imm3_table[16] =
    {
      32, 10, 12, 14, 16, 20, 24, 28,
    };
  uint64_t x = imm3_table[imm & 7];
  if (imm >> 3)
    x = - x;
  return x;  
}

/* Imm-value used for branches */
static uint64_t
imm9 (const struct cpu *cpu, unsigned imm)
{
  return cpu->prefix_flag ? 
    imm | (cpu->prefix_reg << 9)
    : imm;
}

static void
umul64 (uint64_t *hp, uint64_t *lp, uint64_t a, uint64_t b)
{
  uint64_t p0, m0, m1, p1;
  uint64_t ah, al, bh, bl;

  ah = a >> 32;
  al = a & 0xffffffffUL;
  bh = b >> 32;
  bl = b & 0xffffffffUL;

  p0 = al*bl;
  m0 = al*bh;
  m1 = ah*bl;
  p1 = ah*bh;

  m0 += m1;
  p1 += (uint64_t) (m0 < m1) << 32;
  p1 += m0 >> 32;
  m0 <<= 32;
  p0 += m0;
  p1 += (p0 < m0);
  *hp = p1;
  if (lp)
    *lp = p0;
}

static uint64_t
addc (struct cpu *cpu, uint64_t a, uint64_t b, uint64_t cy, unsigned char mode)
{
  uint64_t sum;
  sum = a + cy;
  cy = sum < a;
  sum += b;
  cy += sum < b;

  if (mode & 2)
    {
      /* Signed */
      /* If we follow the ARM spec (A2-43), the "true result" is a + b
	 + cy, where a and b are interpreted as signed, and cy is
	 unsigned. We still never get overflow if a and b are of
	 opposite sign, so the overflow condition is the same: that a
	 and b have the same sign, and the result has the opposite
	 sign. */

      /* For adds (mode == 2), cc is set if a >= b (signed), i.e., if
	 the true signed result is >= 0. */

      int asign = a >> 63;
      int bsign = b >> 63;
      int ssign = sum >> 63;

      /* Overflow */
      int cc = (asign ^ bsign ^ 1) & (asign ^ ssign);
      if ( (mode & 1) == 0)
	cc ^= (ssign ^ 1);
      CPUWCC (cc);
    }
  else
    if (mode & 1)
      CPUWCC (cy);

  return sum;
}

static uint64_t
add_simd (uint64_t mask, uint64_t a, uint64_t b, uint64_t cy)
{
  return (((a & ~mask) + (b & ~mask) + cy) ^ ((a ^ b) & mask));
}

static unsigned
simd_mode (const struct cpu *cpu)
{
  if (!cpu->prefix_flag)
    return 0;

  if (cpu->prefix_reg > 3)
    fatal_trap (cpu, "Invalid simd mode\n");

  return cpu->prefix_reg;
}

static const uint64_t
simd_mask[4] = {
  UINT64_C(0x8000000000000000),
  UINT64_C(0x8000000080000000),
  UINT64_C(0x8000800080008000),
  UINT64_C(0x8080808080808080),
};

static uint64_t
clz (uint64_t x)
{
  if (x == 0)
    return 64;
  else
    {
      unsigned cnt;
      for (cnt = 0;
	   (x & ((uint64_t) 1 << 63)) == 0;
	   cnt++, x <<= 1)
	;
      return cnt;
    }
}

static uint64_t
ctz (uint64_t x)
{
  if (x == 0)
    return 64;
  else
    {
      unsigned cnt;
      for (cnt = 0; (x & 1) == 0; cnt++, x >>= 1)
	;
      return cnt;
    }
}

static unsigned
cls (uint64_t x)
{
  if (x & ((uint64_t) 1 << 63))
    x = ~x;
  return clz (x);
}

static unsigned
popc (uint64_t x)
{
  unsigned cnt, i;
  for (cnt = i = 0; i < 64; i++, x >>= 1)
    cnt += x & 1;
  return cnt;
}

static uint64_t
bswap (uint64_t x)
{
  uint64_t y;
  unsigned i;
  for (i = y = 0; i < 8; i++, x >>= 8)
    y = (y << 8) | (x & 0xff);
  return y;
}

static uint64_t
udiv_21 (uint64_t u1, uint64_t u0, uint64_t d)
{
  uint64_t dh, dl, qh, ql, p;
  assert (d & ((uint64_t) 1 << 63));
  assert (u1 < d);
  dh = d >> 32;
  dl = d - (dh << 32);
  
  qh = u1 / (dh + 1);

  u1 -=  qh * dh;
  p = qh * dl;
  u1 -= p >> 32;
  p <<= 32;
  u1 -= u0 < p;
  u0 -= p;

  while (u1 >= ((uint64_t) 1 << 32))
    {
      qh++;
      u1 -= dh + (u0 < (dl << 32));
      u0 -= (dl << 32);
    }
  ql = ((u1 << 32) + (u0 >> 32)) / (dh+1);
  
  p = ql * dl;
  u1 -= (u0 < p);
  u0 -= p;
  p = ql * dh;
  u1 -= p >> 32;
  p <<= 32;
  u1 -= (u0 < p);
  u0 -= p;
  
  while (u1 > 0)
    {
      ql++;
      u1 -= u0 < d;
      u0 -= d;
    }

  ql += (u0 >= d);

  return (qh << 32) + ql;
}

static void
i_ld_st_offset (struct cpu *cpu, struct memory *memory, uint16_t instr)
{
  /* Load and store with offset. */
  unsigned op = instr >> 12;
  unsigned s = (instr >> 8) & 15;
  unsigned d = instr & 15;
  uint64_t offset = imm4 (cpu, (instr >> 4) & 15);
  uint64_t ea;

  if (op & 1)
    ea = cpu->reg[s] - offset;
  else
    ea = cpu->reg[s] + offset;

  if (op & 2)
    memory_write (memory, ea, cpu->reg[d]);
  else
    CPUW (d, memory_read (memory, ea));
}

static void
i_ld_st_index (struct cpu *cpu, struct memory *memory, uint16_t instr)
{
  unsigned s0 = (instr >> 8) & 15;
  unsigned s1 = (instr >> 4) & 15;
  uint64_t ea = cpu->reg[s0] + cpu->reg[s1];
  unsigned d = instr & 15;
  if (s0 == s1)
    fatal_trap (cpu, "Invalid indexed memory access instruction.\n");
  if (s1 < s0)
    CPUW (d, memory_read (memory, ea));
  else
    memory_write (memory, ea, cpu->reg[d]);
}

static void
i_shiftl (struct cpu *cpu, uint16_t instr)
{
  unsigned breg = (instr >> 8) & 15;
  unsigned creg = (instr >> 4) & 15;
  unsigned dreg = instr & 15;
  uint64_t shift = cpu->reg[creg];
  uint64_t b;

  if (!shift)
    return;

  b = cpu->reg[breg];

  switch (shift >> 62)
    {
    case 0: /* Left shift */
      if (breg == 15)
	b = (uint64_t) cpu->cc_flag << 63;

      if (shift >= 128)
	CPUW (dreg, 0);
      else if (shift >= 64)
	CPUW (dreg, b << (shift - 64));
      else
	CPUW (dreg, (cpu->reg[dreg] << shift)
	      | (b >> (64 - shift)));
      break;
    case 1: /* Logical right shift */
      if (breg == 15)
	b = cpu->cc_flag;

      shift = ((uint64_t) 1 << 63) - shift;
      if (shift >= 128)
	CPUW (dreg, 0);
      else if (shift >= 64)
	CPUW (dreg, b >> (shift - 64));
      else
	CPUW (dreg, (cpu->reg[dreg] >> shift)
	      | (b << (64 - shift)));
      break;
    case 3: /* Arithmetic right shift */
      if (breg == 15)
	b = - (uint64_t) cpu->cc_flag;

      shift = -shift;
      if (shift >= 128)
      case 2:
	CPUW (dreg, - (b >> 63));
      else if (shift >= 64)
	CPUW (dreg, (b >> (shift - 64))
	      | ((- (b >> 63)) >> (128 - shift)));
      else
	CPUW (dreg, (cpu->reg[dreg] << shift)
	      | (b >> (64 - shift)));
    }
}

static void
i_imm_prefix (struct cpu *cpu, uint16_t instr)
{
  uint64_t prefix = instr & 0xfff;
  if (cpu->prefix_flag)
    prefix |= cpu->prefix_reg << 12;

  cpu->prefix_reg = prefix;
}

static void
i_imm_op (struct cpu *cpu, uint16_t instr)
{
  if (instr < 0xb000)
    i_imm_add (cpu, instr);
  else if (instr < 0xc800)
    i_imm_binary (cpu, instr);
  else if (instr < 0xca00)
    i_imm_cmpgt (cpu, instr);
  else
    fatal_trap (cpu, "Reserved imm op\n");
}

static void
i_imm_add (struct cpu *cpu, uint16_t instr)
{
  uint64_t imm = imm4 (cpu, (instr >> 4) & 15);
  uint64_t sign = (instr >> 8) & 1;
  unsigned d = instr & 15;
  CPUW (d, addc (cpu, cpu->reg[d], imm ^ -sign,
		 ((instr >> 11) & 1) ? cpu->cc_flag : sign,
		 (instr >> 9) & 3));
}

static void
i_imm_binary (struct cpu *cpu, uint16_t instr)
{
  uint64_t imm = imm4 (cpu, (instr >> 4) & 15);
  unsigned sign = (instr >> 8) & 1;
  unsigned d = instr & 15;

  if (sign)
    imm = ~imm;

  switch ( (instr >> 9) & 15)
    {
    case 8: /* mov imm */
      CPUW (d, imm + sign);
      break;
    case 9: /* and imm */
      CPUW (d, cpu->reg[d] & imm);
      break;
    case 10: /* or imm */
      CPUW (d, cpu->reg[d] | imm);
      break;
    case 11: /* xor imm */
      CPUW (d, cpu->reg[d] ^ imm);
      break;
    case 0: /* tst imm */
      CPUWCC ((cpu->reg[d] & imm) != 0);
      break;
    case 1: /* cmpeq imm */
      CPUWCC (cpu->reg[d] == imm + sign);
      break;
    case 2: /* cmpugeq imm */
      imm += sign;
      if (cpu->prefix_flag)
	goto cmpugeq;
      /* Steal some codes */
      switch (imm)
	{
	case - (uint64_t) 1:
	  CPUWCC ( (int64_t) cpu->reg[d] >= 0);
	  break;
	case 2:
	  CPUWCC (cpu->reg[d] == 0);
	  break;
	case 4:
	  CPUWCC ( (int64_t) cpu->reg[d] > 8);
	  break;
	case 8:
	  CPUWCC (cpu->reg[d] > 8U);
	  break;
	default:
	cmpugeq:
	  CPUWCC (cpu->reg[d] >= imm);
	}
      break;
    case 3: /* cmpsgeq imm */
      imm += sign;
      CPUWCC ( (int64_t) cpu->reg[d] >= (int64_t) imm);
      break;
    }
}

static void
i_imm_cmpgt (struct cpu *cpu, uint16_t instr)
{
  unsigned dreg = instr & 15;
  uint64_t imm = imm3 ( (instr >> 4) & 15);
  if ((instr >> 8) & 1)
    /* Signed cmp */
    CPUWCC ((int64_t) cpu->reg[dreg] > (int64_t) imm);
  else
    /* Unsigned cmp */
    CPUWCC (cpu->reg[dreg] > imm);
}

static void
i_imm_shift (struct cpu *cpu, uint16_t instr)
{
  unsigned shift = (instr >> 4) & 63;
  unsigned dreg = instr & 15;
  unsigned op = (instr >> 10) & 3;
  uint64_t d = cpu->reg[dreg];

  if (shift > 0)
    {
      unsigned mode = simd_mode (cpu);
      uint64_t hi_mask = simd_mask[mode];
      uint64_t mask;
      unsigned nshift;

      /* Ignore excess shift bits */
      shift &= (1u << (6 - mode)) - 1;
      nshift = (64 >> mode) - shift;
      mask = (hi_mask >> (shift - 1)) - ((hi_mask << 1) | 1);

      switch (op)
	{
	case 0: /* lshift */
	  CPUW (dreg, (d & mask) << shift);
	  break;
	case 1: /* rshift */
	  CPUW (dreg, (d >> shift) & mask);
	  break;
	case 2: /* ashift */
	  {
	    /* Arithmetic shift */
	    uint64_t sign = d & hi_mask;
	    sign = (sign << 1) - (sign >> (shift - 1));
	    CPUW (dreg, ((d >> shift) & mask) | sign);
	    break;
	  }
	case 3: /* rot left */
	  CPUW (dreg, ((d & mask) << shift) | ((d & ~mask) >> nshift));
	  break;
	}
    }
  else
    /* Zero shift count is special */
    switch (op)
      {
      case 0:
	CPUW (dreg, clz (d));
	break;
      case 1:
	CPUW (dreg, ctz (d));
	break;
      case 2:
	CPUW (dreg, cls (d));
	break;
      case 3:
	CPUW (dreg, popc (d));
      }
}

static void
i_branch (struct cpu *cpu, uint16_t instr)
{
  uint64_t imm = (imm9 (cpu, instr & 0x1ff) + 1) << 1;
  if (instr & 0x200)
    imm = -imm;

  switch ( (instr >> 10) & 7)
    {
    case 1: /* jsr */
      CPUW (14, cpu->program_counter);
      /* Fall through */
    case 0: /* jmp */
      CPUW (15, cpu->program_counter + imm);
      break;
    case 2: /* bt */
      if (cpu->cc_flag)
	CPUW (15, cpu->program_counter + imm);
      break;
    case 3: /* bf */
      if (!cpu->cc_flag)
	CPUW (15, cpu->program_counter + imm);
      break;
    case 4: /* bnz */
      if (cpu->reg[8])
	CPUW (15, cpu->program_counter + imm);
      break;
    default:
      fatal_trap (cpu, "Reserved branch opcode.\n");    
    }
}

static void
i_add_op (struct cpu *cpu, uint16_t instr)
{
  unsigned dreg = instr & 15;
  uint64_t d = cpu->reg[dreg];
  uint64_t s = cpu->reg[(instr >> 4) & 15];
  uint64_t sub_flag = (instr >> 11) & 1;
  unsigned cin_flag = ((instr >> 10) & 1);
  unsigned cout_mode = (instr >> 8) & 3;
  /* Ignore mask (and prefix) for operations involving carry. */
  unsigned mode = (cin_flag > 0 || cout_mode > 0) ? 0 : simd_mode(cpu);

  s ^= -sub_flag;

  if (mode == 0)
    CPUW (dreg, addc (cpu, d, s, cin_flag ? cpu->cc_flag : sub_flag, cout_mode));
  else
    {
      uint64_t mask = simd_mask[mode];
      CPUW (dreg, add_simd (mask, d, s, -sub_flag & ((mask << 1) | 1)));
    }
}

static void
i_binary_op (struct cpu *cpu, struct memory *memory, uint16_t instr)
{
  unsigned dreg = instr & 15;
  uint64_t s = cpu->reg[(instr >> 4) & 15];
  uint64_t d = cpu->reg[dreg];
  switch ( (instr >> 8) & 31)
    {
    case 0: /* mov */
      CPUW (dreg, s);
      break;
    case 1: /* and */
      CPUW (dreg, d & s);
      break;
    case 2: /* or */
      CPUW (dreg, d | s);
      break;
    case 3: /* xor */
      CPUW (dreg, d ^ s);
      break;
    case 4: /* movt */
      if (cpu->cc_flag)
	CPUW (dreg, s);
      break;
    case 5: /* movf */
      if (!cpu->cc_flag)
	CPUW (dreg, s);
      break;
    case 6: /* mullo */
      CPUW (dreg, d * s);
      break;
    case 7: /* umulhi */
      {
	uint64_t hi;
	umul64 (&hi, NULL, d, s);
	CPUW (dreg, hi);
	break;
      }
    case 8: /* shift */
      switch (s >> 62)
	{
	case 0: /* Left shift */
	  if (s >= 64)
	    CPUW (dreg, 0);
	  else
	    CPUW (dreg, d << s);
	  break;
	case 1: /* Logical right shift */
	  s = ((uint64_t) 1 << 63) - s;
	  if (s >= 64)
	    CPUW (dreg, 0);
	  else
	    CPUW (dreg, d >> s);
	  break;
	case 3: /* Arithmetic right shift by huge */
	  if ((int64_t) s <= -64)
	  case 2:
	    CPUW (dreg, - (d >> 63));
	  else
	    {
	      uint64_t sign = - (d >> 63);
	      CPUW (dreg, ((d >> - s)
			   | (sign << (64 + s))));
	    }
	  break;
	}
      break;
    case 9: /* injt8 */
      CPUW (dreg, ((d & (((uint64_t) 1 << 56)-1))
		   | s << 56));
      break;
    case 10: /* injt16 */
      CPUW (dreg, ((d & (((uint64_t) 1 << 48)-1))
		   | s << 48));
      break;
    case 11: /* injt32 */
      CPUW (dreg, ((d & (((uint64_t) 1 << 32)-1))
		   | (s << 32)));
      break;
    case 12: /* tst */
      CPUWCC ((d & s) != 0);
      break;
    case 13: /* cmpeq */
      CPUWCC (d == s);
      break;
    case 14: /* cmpugeq */
      CPUWCC (d >=  s);
      break;
    case 15: /* cmpuleq */
      CPUWCC ((int64_t) d >= (int64_t) s);
      break;
    case 16: /* ld */
      CPUW (dreg, memory_read (memory, s));
      break;
    case 17: /* st */
      memory_write (memory, s, d);
      break;
    default:
      fatal_trap (cpu, "Unassigned instr.\n");
    }
}

static void
i_xshift (struct cpu *cpu, uint16_t instr)
{
  unsigned dreg = instr & 15;
  switch ((instr >> 4) & 3)
    {
    case 0:
      CPUW (dreg, ((cpu->reg[dreg] >> 1)
		   | ( (uint64_t) cpu->cc_flag << 63)));
      break;
    case 1:
      {
	uint64_t cin = cpu->cc_flag;
	CPUWCC (cpu->reg[dreg] & 1);
	CPUW (dreg, ((cpu->reg[dreg] >> 1)
		     | (cin << 63)));
	break;
      }
    case 2:
      CPUWCC (cpu->reg[dreg] & 1);
      CPUW (dreg, cpu->reg[dreg] >> 1);
      break;
    case 3:
      /* Arithmetic shift */
      CPUWCC (cpu->reg[dreg] & 1);
      CPUW (dreg, ((cpu->reg[dreg] >> 1)
		   | (cpu->reg[dreg] & ((uint64_t) 1 << 63))));
      break;
    }
}

static void
i_unary_op (struct cpu *cpu, uint16_t instr)
{
  unsigned dreg = instr & 15;
  instr &= 0x7ff;
  if (instr < 0x80)
    /* add cc, #0, sub, cc, #0 */
    CPUW (dreg, addc (cpu, cpu->reg[dreg], 
		      ((instr >> 6) & 1) ? -1 : 0,
		      cpu->cc_flag, (instr >> 4) & 3));
  else if (instr < 0x100)
    i_xshift (cpu, instr);
  else switch ((instr >> 4) & 7)
    {
    case 0: /* neg */
      CPUW (dreg, - cpu->reg[dreg]);
      break;
    case 1: /* not */
      if (dreg == 15)
	CPUWCC (!cpu->cc_flag);
      else
	CPUW (dreg, ~cpu->reg[dreg]);
      break;
    case 2: /* bswap */
      CPUW (dreg, bswap (cpu->reg[dreg]));
      break;
    case 3: /* recpr */
      {
	uint64_t d = cpu->reg[dreg];
	if (d == 0)
	  fatal_trap (cpu, "Divide by zero.\n");
	d <<= clz (d);
	/* Compute floor [(2^128 - 1) / d] - 2^64
	   = floor [<2^64 - 1 - d, 2^64 - 1> / d] */
	CPUW (dreg, udiv_21 (~d, ~(uint64_t) 0, d));
	break;
      }
    case 4: /* call, indirect */
      CPUW (14, cpu->program_counter);
      CPUW (15, cpu->reg[dreg]);
      break;
    case 5: /* fneg */
      CPUW (dreg, cpu->reg[dreg] ^ ((uint64_t) 1 << 63));
      break;
    case 6: /* fabs */
      CPUW (dreg, cpu->reg[dreg] & ~((uint64_t) 1 << 63));
      break;
    default:
      fatal_trap (cpu, "Unassigned opcode area.\n");
    }
}

static void
i_fp_op (struct cpu *cpu, uint16_t instr)
{
  instr &= 0x7ff;
  if (instr < 0x200)
    i_fmac (cpu, instr);
  else if (instr < 0x280)
    i_fldexp (cpu, instr);
  else if (instr < 0x500)
    i_binary_fp (cpu, instr);
  else 
    i_unary_fp (cpu, instr);

}

static void
i_fmac (struct cpu *cpu, uint16_t instr)
{
  unsigned dreg = instr & 7;
  double d = get_fpreg (cpu, dreg);

  d += get_fpreg (cpu, (instr >> 6) & 7)
    * get_fpreg (cpu, (instr >> 3) & 7);
  set_fpreg (cpu, dreg, d);
}

static void
i_fldexp (struct cpu *cpu, uint16_t instr)
{
  unsigned dreg = instr & 7;
  double d = get_fpreg (cpu, dreg);
  set_fpreg (cpu, dreg, ldexp (d, cpu->reg[(instr >> 3) & 15]));
}

static void
i_binary_fp (struct cpu *cpu, uint16_t instr)
{
  unsigned dreg = instr & 7;
  unsigned sreg = (instr >> 3) & 7;
  double d = get_fpreg (cpu, dreg);
  double s = get_fpreg (cpu, sreg);

  switch ((instr >> 6) & 31)
    {
    case 10: /* fadd */
      set_fpreg (cpu, dreg, d + s);
      break;
    case 11: /* fsub */
      set_fpreg (cpu, dreg, d - s);
      break;
    case 12: /* fmul */
      set_fpreg (cpu, dreg, d * s);
      break;
    case 13: /* fdiv */
      set_fpreg (cpu, dreg, d / s);
      break;
    case 14: /* fcmpeq */
      CPUWCC (d == s);
      break;
    case 15: /* fcmpgeq */
      CPUWCC (d >= s);
      break;
    case 16: /* fcmpgt */
      CPUWCC (d > s);
      break;
    default: 
      fatal_trap (cpu, "Reserved binary fp op.\n");
    }
}

static void
i_unary_fp (struct cpu *cpu, uint16_t instr)
{
  unsigned dreg = instr & 7;
  double d = get_fpreg (cpu, dreg);
  switch ((instr >> 3) & 31)
    {
    case 0: /* fs2d */
      set_fpreg (cpu, dreg, get_fpreg_s (cpu, dreg));
      break;
    case 1: /* fd2s */
      set_fpreg_s (cpu, dreg, d);
      break;
    case 2: /* fui2d */
      set_fpreg (cpu, dreg, (double) cpu->reg[dreg]);
      break;
    case 3: /* fd2ui */
      CPUW (dreg, d);
      break;
    case 4: /* fsi2d */
      set_fpreg (cpu, dreg, (double) (int64_t) cpu->reg[dreg]);
      break;
    case 5: /* fd2si */
      CPUW (dreg, (int64_t) d);
      break;
    case 6: /* fui2s */
      set_fpreg_s (cpu, dreg, (float) cpu->reg[dreg]);
      break;
    case 7: /* fsi2s */
      set_fpreg_s (cpu, dreg, (float) (int64_t) cpu->reg[dreg]);
      break;
    case 8: /* feqz */
      CPUWCC (d == 0.0);
      break;
    case 9: /* fgeqz */
      CPUWCC (d >= 0.0);
      break;
    case 10: /* fgtz */
      CPUWCC (d < 0.0);
      break;
    case 11: /* fleqz */
      CPUWCC (d <= 0.0);
      break;
    case 12: /* fltz */
      CPUWCC (d < 0.0);
      break;
    default:
      fatal_trap (cpu, "Reserved unary fp op.\n");
    }
}
