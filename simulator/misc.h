/* instr16 cpu simulator

   Copyright (C) 2013  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_MISC_H_INCLUDED
#define INSTR16_MISC_H_INCLUDED

#if __GNUC__
# define NORETURN __attribute__ ((__noreturn__))
# define PRINTF_STYLE(f, a) __attribute__ ((__format__ (__printf__, f, a)))
# define UNUSED __attribute__ ((__unused__))
#else
# define NORETURN
# define PRINTF_STYLE(f, a)
# define UNUSED
#endif

void NORETURN PRINTF_STYLE (1, 2)
die (const char *format, ...);

void PRINTF_STYLE (1, 2)
werror (const char *format, ...);

#endif /* INSTR16_MISC_H_INCLUDED */
