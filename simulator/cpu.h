/* instr16 cpu simulator

   Copyright (C) 2013  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_CPU_H_INCLUDED
#define INSTR16_CPU_H_INCLUDED

#include <stdint.h>
#include <stdio.h>

struct memory;

struct cpu
{
  uint64_t reg[16];
  uint64_t prefix_reg;
  int prefix_flag;
  int cc_flag;
  uint64_t instr_counter;
  int trace;
};

#define program_counter reg[15]

void
cpu_init (struct cpu *cpu);

void
cpu_trace (struct cpu *cpu);

void
cpu_write (const struct cpu *cpu, FILE *f);

int
cpu_instruction_cycle (struct cpu *cpu, struct memory *memory);


#endif /* INSTR16_CPU_H_INCLUDED */
