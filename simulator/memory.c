/* instr16 cpu simulator

   Copyright (C) 2013, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>

#include "memory.h"
#include "io.h"
#include "misc.h"

#define ROTL64(n, x) (((x) << (n)) | ((x) >> ((-n) & 63)))

uint64_t
memory_read (const struct memory *memory, uint64_t addr)
{
  uint64_t waddr = addr >> 3;
  unsigned rot;
  uint64_t w;

  if (waddr < memory->storage_size)
    w = memory->storage[waddr];
  else if (memory->io && memory->io->read && waddr >= memory->io_offset)
    w = memory->io->read (memory->io, waddr - memory->io_offset);
  else
    die ("Out of range memory read, address %jx\n", (uintmax_t) addr);

  rot = 8*(addr & 7);

  return ROTL64 (rot, w);
}

void
memory_write (struct memory *memory, uint64_t addr, uint64_t w)
{
  uint64_t waddr = addr >> 3;
  unsigned rot;

  if (memory->trace)
    fprintf (stderr, "memory: store, address %jx <-- %jx\n",
	     (uintmax_t) addr, (uintmax_t) w);

  rot = 8*((-addr) & 7);
  w = ROTL64 (rot, w);
  
  if (waddr < memory->storage_size)
    memory->storage[waddr] = w;
  else if (memory->io && memory->io->write && waddr >= memory->io_offset)
    memory->io->write (memory->io, waddr - memory->io_offset, w);
  else
    die ("Out of range memory write, address %jx\n", (uintmax_t) addr);
}

void
memory_alloc (struct memory *memory, uint64_t size)
{
  memory->storage_size = size;
  memory->storage = malloc (size * sizeof(*memory->storage));
  if (!memory->storage)
    die ("Memory exhausted.\n");
  memory->io_offset = 0;
  memory->io = NULL;
  memory->trace = 0;
}

void
memory_trace (struct memory *memory)
{
  memory->trace = 1;
}

void
memory_load (struct memory *memory, FILE *f)
{
  uint64_t i = 0;
  for (;;)
    {
      unsigned char buf[sizeof(uint64_t)];
      size_t res = fread (buf, 1, sizeof(buf), f);
      if (res < sizeof(buf))
	memset (buf + res, 0xff, sizeof(buf) - res);
      memory->storage[i++]
	= ((uint64_t)buf[0] << 56)
	| ((uint64_t)buf[1] << 48)
	| ((uint64_t)buf[2] << 40)
	| ((uint64_t)buf[3] << 32)
	| ((uint64_t)buf[4] << 24)
	| ((uint64_t)buf[5] << 16)
	| ((uint64_t)buf[6] << 8)
	| buf[7];
      if (res < sizeof (buf))
	break;
      if (i >= memory->storage_size)
	die ("Simulated memory too small.\n");
    }
}

void
memory_register_io (struct memory *memory, uint64_t addr,
		    struct io_regs *io)
{
  memory->io_offset = addr;
  memory->io = io;
}
