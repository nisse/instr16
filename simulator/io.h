/* instr16 cpu simulator

   Copyright (C) 2013  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_IO_H_INCLUDED
#define INSTR16_IO_H_INCLUDED

#include <stdint.h>

struct io_regs
{
  uint64_t (*read) (struct io_regs *io, uint64_t addr);
  void (*write) (struct io_regs *io, uint64_t addr, uint64_t value);
};

void
io_init (struct io_regs *io);

#endif /* INSTR16_IO_H_INCLUDED */
