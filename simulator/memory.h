/* instr16 cpu simulator

   Copyright (C) 2013, 2015  Niels Möller

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INSTR16_MEMORY_H_INCLUDED
#define INSTR16_MEMORY_H_INCLUDED

#include <stdint.h>
#include <stdio.h>

struct io_regs;

struct memory
{
  /* Size in 64-bit words */
  uint64_t storage_size;
  uint64_t *storage;

  uint64_t io_offset;
  struct io_regs *io;
  int trace;
};

uint64_t
memory_read (const struct memory *memory, uint64_t addr);

void
memory_write (struct memory *memory, uint64_t addr, uint64_t w);

void
memory_alloc (struct memory *memory, uint64_t size);

void
memory_trace (struct memory *memory);

void
memory_load (struct memory *memory, FILE *f);

void
memory_register_io (struct memory *memory, uint64_t addr,
		    struct io_regs *io);

#endif /* INSTR16_MEMORY_H_INCLUDED */
